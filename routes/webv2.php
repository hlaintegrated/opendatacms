<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'LandingController@index');

// switch language

Route::get('language/{id}', 'TranslationController@switchLanguage');

Route::get('/login', 'PublicController@index');
Route::post('/login', 'AuthController@login')->name('login');

// Access to auth user only
Route::middleware(['administrator'])->group(function () {

  // Prefix url with /administrator/
  Route::prefix('administrator')->group(function () {

    Route::get('dashboard', 'DashboardController@index');
    Route::get('user','UserController@index');
    Route::get('user/create','UserController@create');

    Route::get('menu/section/{section}','MenuController@sectionindex');
    Route::resource('menu','MenuController');

    Route::get('settings/','SystemSettingController@index');
    Route::get('translation','TranslationController@index');
    Route::get('settings/banner','SystemSettingController@banner');
    Route::resource('mailtemplate','MailTemplateController');

    Route::get('clusters','ClusterController@index');
    Route::post('clusters','ClusterController@save');
    Route::get('clusters/position','ClusterController@position');
    Route::post('clusters/position','ClusterController@savePosition');

  });

});





