<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/', function () {
//     return view('portal/welcome');
// });

// Public Portal Pages
Route::get('/', 'PortalController@index');

Route::Resource('/admin/ticket','TicketController');

Route::Resource('/portal/request','RequestController');

// Login process
Route::get('/admin', function () {
    return view('admin/login');
});
Route::get('/admin/login', function () {
    return view('admin/login');
});
// After Login Pages
Route::get('/admin/dashboard', function () {
    return view('admin/dashboard');
});

Route::get('/admin/report', function () {
    return view('admin/report');
});