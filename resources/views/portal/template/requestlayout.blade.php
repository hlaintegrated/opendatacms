<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Permohonan Set Data</title>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.14/moment-timezone.min.js"></script>

        <style>
            .scrollable-menu {
                height: auto;
                max-height: 200px;
                overflow-x: hidden;
            }
            .title {
                font-weight: bold;
            }
            .mandatory {
                color: #ff0000;
            }
            .eg {
                font-size: 0.9rem;
            }
            .btn-text {
                font-weight:bold; 
                width:90px;
            }
            .mb0 {
                font-weight: bold; 
                color: #6c757d;
            }
        </style>
    </head>
    <body>
        <div class="container contact">
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header "><b>Permohonan Set Data</b></div>
                        <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            <br>
                            @endif
                                <form action="{{route('request.store')}}" method="POST">
                                    @csrf
                                    <!-- Textarea Nama -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class ="col-form-label title">Name<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <input type="text" name="name" class ="form-control eg" placeholder="Cth: Syed Imran Hafizzudin " value="{{old('name')}}"></input> 
                                                <span class="text-danger">{{ $errors->first('name') }}</span>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <!-- Textarea No. telefon -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class ="col-form-label title">No. Telefon<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <input type="text" name="phone" class ="form-control eg" placeholder="Cth: 01123374868" value="{{old('phone')}}"></input> 
                                                <span class="text-danger">{{ $errors->first('phone') }}</span>
                                            </div>
                                        </div>
                                    <!-- Textarea E-mel -->
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class ="col-form-label title">Emel<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <input type="text" name="emel" class="form-control eg" placeholder="Cth: syedimran@domain.com" value="{{old('emel')}}"></input> 
                                                <span class="text-danger">{{ $errors->first('emel') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Textarea Tajuk Set Data -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class="col-form-label title">Tajuk Set Data<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <input type="text" name="subjek" class="form-control eg" placeholder="Cth: Lokaliti Hotspot Denggi di Malaysia mengikut Negeri" value="{{old('subjek')}}"></input>
                                                <span class="text-danger">{{ $errors->first('subjek') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Textarea Keterangan Set Data -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class ="col-form-label title">Keterangan Set Data<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <textarea rows="3" name="description" class="form-control eg" placeholder="Cth: Senarai denggi di Malaysia dari tahun 2000 hingga 2016 oleh negeri">{{Request::old('description')}}</textarea> 
                                                <span class="text-danger">{{ $errors->first('description') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Textarea Catatan Lain -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class ="col-form-label title">Catatan Lain</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <textarea rows="3" name="notes" class ="form-control eg" placeholder="Cth: Mempunyai granulariti dataset mengikut statistik mingguan">{{Request::old('notes')}}</textarea> 
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Dropdown Penyumbang Set Data-->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class ="col-form-label title">Penyumbang Set Data<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <select id="report" name="pembekal" class="form-control  eg" value="{{old('pembekal')}}">
                                                <option selected disabled>Sila Pilih</option>
                                                @foreach($sources ['data'] as $src)
                                                <option value='{{ $src->data_source_name }}'>{{ $src->data_source_name }}</option>
                                                @endforeach
                                                </select>
                                                <span class="text-danger">{{ $errors->first('pembekal') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                    <div class="col-lg-4"></div>  
                                    <!-- Butang Batal Permohonan -->                     
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary btn-md eg btn-text">Batal</button>
                                            </div>
                                        </div>
                                    <!-- Butang Hantar Permohonan -->
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-md eg btn-text">Hantar</button> 
                                            </div>
                                        </div>
                                        <div class="col-lg-4"></div> 
                                    </div>
                                    
                                </form>
                            </div>
                    </div>
                </div>
                <!-- Ruang Panduan -->
                <div class="col-lg-3 mb0">
                    <div><img src="{{ URL::to('/images/info-icon.png') }}"></div><br>
                        <div>
                        <span>Sila isikan borang di sebelah untuk memohon set data</span>
                        </div>  
                        <br>
                        <div>
                        <span>NOTA: Medan bertanda <span class="mandatory">*</span> adalah mandatori</span>
                        </div>
                </div>
            </div>
        </div>
    </body>
    <!-- Script -->
    <script type='text/javascript'>

    </script>
</html>