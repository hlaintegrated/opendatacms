<div class="container-fluid">
    <div class="row">
        <div class="col pr-top-line1">&nbsp;</div>
        <div class="col pr-top-line2">&nbsp;</div>
    </div>
</div>

<!-- Menu -->
<nav class="navbar navbar-light navbar-expand-lg py-2">
    <div class="container">
        <div class="navbar-brand mb-0 d-flex align-items-center">
            <a href="{{ url('/') }}"><img src="/images/icon_only.png" style="width: 55px;" /></a>
            <div class="pl-3" style="display: inline-block;">
                <a href="{{ url('/') }}"><span class="pr-top-title">data.gov.my</span></a><br/>
                <!-- <span class="pr-top-subtitle">data untuk kesejahteraan rakyat</span> -->
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="#">Pilihan Bahasa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="{{ url('login') }}">Log Masuk</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<nav class="navbar navbar-expand-lg pr-nav-bar">
    <div class="container">
        <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item px-4">
                    <a class="nav-link" href="http://150.242.183.95/data/dataset">Data</a>
                </li>
                <li class="nav-item dropdown px-4">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Perkhidmatan
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="http://150.242.183.95/portal/request">Permohonan Set Data</a>
                        <a class="dropdown-item" href="#">Semakan Set Data</a>
                        <a class="dropdown-item" href="#">Muat Turun Manual</a>
                    </div>
                </li>
                <li class="nav-item dropdown px-4">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pengumuman
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Hebahan</a>
                        <a class="dropdown-item" href="#">Aktiviti</a>
                    </div>
                </li>
                <li class="nav-item dropdown px-4">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Informasi
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Terma Pengguna</a>
                        <a class="dropdown-item" href="#">Galeri Aplikasi</a>
                        <a class="dropdown-item" href="#">Peneraju Data Terbuka</a>
                        <a class="dropdown-item" href="#">Pekeliling Data Terbuka</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>