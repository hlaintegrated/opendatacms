<!doctype html>
<html lang="en">
  <head>
    @include('portal.template.partials.head')
  </head>
  <body>

    @include('portal.template.partials.topbar')

    <!-- Begin Page Content -->
    @yield('content')

    @include('portal.template.partials.footer')
    @include('portal.template.partials.footer-scripts')

    @yield('script-after-load')
  </body>
</html>