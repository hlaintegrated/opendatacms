<!DOCTYPE html>
<html lang="en">

<head>

@include('admin.template.partials.head')

</head>

<body class="bg-gradient-primary">


    <!-- Begin Page Content -->
    @yield('content')
    <!-- /.container-fluid -->


  @include('admin.template.partials.footer-scripts')

</body>

</html>
