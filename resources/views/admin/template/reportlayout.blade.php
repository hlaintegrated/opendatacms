<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Reporting</title>
    <script type="text/javascript">
      function select(){

        var a = document.getElementById('report').value;
        if(a === "ringkasan"){
          document.getElementById('in1').style.display = "block";
          document.getElementById('in2').style.display = "none";
          document.getElementById('in3').style.display = "none";
          document.getElementById('in4').style.display = "none";
          document.getElementById('in5').style.display = "none";
          document.getElementById('in6').style.display = "none";
          document.getElementById('frame').style.display = "block";
        }
        else if(a === "set data"){
          document.getElementById('in1').style.display = "none";
          document.getElementById('in2').style.display = "block";
          document.getElementById('in3').style.display = "none";
          document.getElementById('in4').style.display = "none";
          document.getElementById('in5').style.display = "none";
          document.getElementById('in6').style.display = "none";
          document.getElementById('frame').style.display = "block";
        }
        else if(a === "metadata"){
          document.getElementById('in1').style.display = "none";
          document.getElementById('in2').style.display = "none";
          document.getElementById('in3').style.display = "block";
          document.getElementById('in4').style.display = "none";
          document.getElementById('in5').style.display = "none";
          document.getElementById('in6').style.display = "none";
          document.getElementById('frame').style.display = "block";
        }
        else if(a === "muat turun"){
          document.getElementById('in1').style.display = "none";
          document.getElementById('in2').style.display = "none";
          document.getElementById('in3').style.display = "none";
          document.getElementById('in4').style.display = "block";
          document.getElementById('in5').style.display = "none";
          document.getElementById('in6').style.display = "none";
          document.getElementById('frame').style.display = "block";
        }
        else if(a === "pembekal set data"){
          document.getElementById('in1').style.display = "none";
          document.getElementById('in2').style.display = "none";
          document.getElementById('in3').style.display = "none";
          document.getElementById('in4').style.display = "none";
          document.getElementById('in5').style.display = "block";
          document.getElementById('in6').style.display = "none";
          document.getElementById('frame').style.display = "block";
        }
        else if(a === "tiket"){
          document.getElementById('in1').style.display = "none";
          document.getElementById('in2').style.display = "none";
          document.getElementById('in3').style.display = "none";
          document.getElementById('in4').style.display = "none";
          document.getElementById('in5').style.display = "none";
          document.getElementById('in6').style.display = "block";
          document.getElementById('frame').style.display = "block";
        }
        else{
          document.getElementById('in1').style.display = "none";
          document.getElementById('in2').style.display = "none";
          document.getElementById('in3').style.display = "none";
          document.getElementById('in4').style.display = "none";
          document.getElementById('in5').style.display = "none";
          document.getElementById('in6').style.display = "none";
          document.getElementById('frame').style.display = "none";
        }
      }
      function myFunction() {
          window.print();
      }
    </script>
    <style>
      .in{
        padding:0;
      }

      #in1, #in2, #in3, #in4, #in5, #in6, #frame{
        display: none;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="card border-info mb-3">
        <div class="card-header"><b>Maklumat</b></div>
        <div class="card-body">
          <div class="form-group row">
            <label for="report" class="col-sm-2 col-form-label">Senarai Laporan :</label>
            <select id="report" name="report" class="form-control col-sm-5" onchange="select()">
              <option value="pilih">-Sila Pilih-</option>
              <option value="ringkasan">Ringkasan</option>
              <option value="set data">Set Data</option>
              <option value="metadata">Metadata</option>
              <option value="muat turun">Muat Turun</option>
              <option value="pembekal set data">Pembekal Set Data</option>
              <option value="tiket">Tiket</option>
            </select>
          </div>
          <hr/>
          <form action="" method="">
            <div class="in" id="in1">
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Kementerian  :</label>
                <select id="report" name="kementerian" class="form-control col-sm-5">
                  <option value="ringkasan">Kementerian Pendidikan</option>
                  <option value="set data">Kementerian Kewangan</option>
                  <option value="metadata">Kementerian Kesihatan</option>
                  <option value="muat turun">Kementerian Sumber Manusia</option>
                </select>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">&nbsp;</div>
                <div class="col-sm-1">
                  <button type="submit" class="btn btn-primary btn-md">Jana</button>
                </div>
              </div>
            </div>
            <div class="in" id="in2">
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Tarikh Mula   :</label>
                  <input type="date" id="start" name="trip-start" value="2018-07-22"
                    min="2003-01-01" max="2100-12-31" class="form-control col-sm-5">
              </div>
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Tarikh Akhir  :</label>
                  <input type="date" id="start" name="trip-end" value="2018-07-22"
                    min="2003-01-01" max="2100-12-31" class="form-control col-sm-5">
              </div>
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Status    :</label>
                    <select id="report" name="status" class="form-control col-sm-5">
                      <option value="ringkasan">Diterbitkan</option>
                      <option value="set data">Diluluskan</option>
                      <option value="metadata">Ditolak</option>
                      <option value="muat turun">Dalam Proses</option>
                    </select>
              </div>
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Kekerapan :</label>
                    <select id="report" name="kekerapan" class="form-control col-sm-5">
                      <option value="ringkasan">Harian</option>
                      <option value="set data">Mingguan</option>
                      <option value="metadata">Bulanan</option>
                      <option value="muat turun">Suku Tahunan</option>
                      <option value="muat turun">Tahunan</option>
                    </select>
              </div>
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Kluster :</label>
                      <select id="report" name="kluster" class="form-control col-sm-5">
                        <option value="ringkasan">Pendidikan</option>
                        <option value="set data">Pertanian</option>
                        <option value="metadata">Jenayah</option>
                        <option value="muat turun">Alam Sekitar</option>
                        <option value="muat turun">Pengangkutan</option>
                      </select>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">&nbsp;</div>
                <div class="col-sm-1">
                  <button type="submit" class="btn btn-primary btn-md">Jana</button>
                </div>
              </div>
            </div>
            <div class="in" id="in3">
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Tarikh Mula   :</label>
                  <input type="date" id="start" name="trip-start" value="2018-07-22"
                    min="2003-01-01" max="2100-12-31" class="form-control col-sm-5">
              </div>
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Tarikh Akhir  :</label>
                  <input type="date" id="start" name="trip-end" value="2018-07-22"
                    min="2003-01-01" max="2100-12-31" class="form-control col-sm-5">
              </div>
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Kementerian :</label>
                      <select id="report" name="kluster" class="form-control col-sm-5">
                        <option value="ringkasan">Kementerian Pendidikan</option>
                        <option value="set data">Kementerian Kewangan</option>
                         <option value="metadata">Kementerian Kesihatan</option>
                         <option value="muat turun">Kementerian Sumber Manusia</option>
                      </select>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">&nbsp;</div>
                <div class="col-sm-1">
                  <button type="submit" class="btn btn-primary btn-md">Jana</button>
                </div>
              </div>
            </div>
            <div class="in" id="in4">
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Tarikh Mula   :</label>
                  <input type="date" id="start" name="trip-start" value="2018-07-22"
                    min="2003-01-01" max="2100-12-31" class="form-control col-sm-5">
              </div>
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Tarikh Akhir  :</label>
                  <input type="date" id="start" name="trip-end" value="2018-07-22"
                    min="2003-01-01" max="2100-12-31" class="form-control col-sm-5">
              </div>
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Status    :</label>
                    <select id="report" name="status" class="form-control col-sm-5">
                      <option value="ringkasan">Diterbitkan</option>
                      <option value="set data">Diluluskan</option>
                      <option value="metadata">Ditolak</option>
                      <option value="muat turun">Dalam Proses</option>
                    </select>
              </div>
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Kementerian :</label>
                      <select id="report" name="kluster" class="form-control col-sm-5">
                        <option value="ringkasan">Kementerian Pendidikan</option>
                        <option value="set data">Kementerian Kewangan</option>
                         <option value="metadata">Kementerian Kesihatan</option>
                         <option value="muat turun">Kementerian Sumber Manusia</option>
                      </select>
              </div>
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Kluster :</label>
                      <select id="report" name="kluster" class="form-control col-sm-5">
                        <option value="ringkasan">Pendidikan</option>
                        <option value="set data">Pertanian</option>
                        <option value="metadata">Jenayah</option>
                        <option value="muat turun">Alam Sekitar</option>
                        <option value="muat turun">Pengangkutan</option>
                      </select>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">&nbsp;</div>
                <div class="col-sm-1">
                  <button type="submit" class="btn btn-primary btn-md">Jana</button>
                </div>
              </div>
            </div>
            <div class="in" id="in5">
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Kementerian  :</label>
                <select id="report" name="kementerian" class="form-control col-sm-5">
                  <option value="ringkasan">Kementerian Pendidikan</option>
                  <option value="set data">Kementerian Kewangan</option>
                  <option value="metadata">Kementerian Kesihatan</option>
                  <option value="muat turun">Kementerian Sumber Manusia</option>
                </select>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">&nbsp;</div>
                <div class="col-sm-1">
                  <button type="submit" class="btn btn-primary btn-md">Jana</button>
                </div>
              </div>
            </div>
            <div class="in" id="in6">
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Tarikh Mula   :</label>
                  <input type="date" id="start" name="trip-start" value="2018-07-22"
                    min="2003-01-01" max="2100-12-31" class="form-control col-sm-5">
              </div>
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Tarikh Akhir  :</label>
                  <input type="date" id="start" name="trip-end" value="2018-07-22"
                    min="2003-01-01" max="2100-12-31" class="form-control col-sm-5">
              </div>
              <div class="form-group row">
                <label for="report" class="col-sm-2 col-form-label">Status    :</label>
                    <select id="report" name="status" class="form-control col-sm-5">
                      <option value="ringkasan">Diterbitkan</option>
                      <option value="set data">Diluluskan</option>
                      <option value="metadata">Ditolak</option>
                      <option value="muat turun">Dalam Proses</option>
                    </select>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">&nbsp;</div>
                <div class="col-sm-1">
                  <button type="submit" class="btn btn-primary btn-md">Jana</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
