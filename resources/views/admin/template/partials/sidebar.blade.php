<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="http://localhost:8000/admin/dashboard">
    <!-- <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
    </div> -->
    <div class="sidebar-brand-text mx-3">Open Data</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
    <a class="nav-link" href="index.html">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
    Pengurusan Portal
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m1" aria-expanded="true" aria-controls="m1">
        <i class="fas fa-fw fa-cog"></i>
        <span>Pengurusan Pengguna</span>
    </a>
    <div id="m1" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <!-- <h6 class="collapse-header">Custom Components:</h6> -->
        <a class="collapse-item" href="buttons.html">Senarai Pengguna</a>
        <a class="collapse-item" href="cards.html">Cipta Pengguna Baru</a>
        </div>
    </div>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="http://150.242.183.95/admin/ticket">
        <i class="fas fa-fw fa-cog"></i>
        <span>Pengurusan Tiket</span>
    </a>
     
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link" href="http://150.242.183.95/admin/report">
        <i class="fas fa-fw fa-file"></i>
        <span>Laporan</span>
    </a>
    <div id="m3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <!-- <h6 class="collapse-header">Custom Components:</h6> -->
        <!-- <a class="collapse-item" href="">Laporan</a>
        <a class="collapse-item" href="cards.html">Dataset Approval Workflow</a>
        <a class="collapse-item" href="cards.html">Update Frequency</a> -->
        </div>
    </div>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m4" aria-expanded="true" aria-controls="m4">
        <i class="fas fa-fw fa-cog"></i>
        <span>Tetapan Menu</span>
    </a>
    <div id="m4" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <!-- <h6 class="collapse-header">Custom Components:</h6> -->
        <a class="collapse-item" href="buttons.html">Senarai Menu</a>
        <a class="collapse-item" href="cards.html">Cipta Menu Baru</a>
        <a class="collapse-item" href="cards.html">Menu Header</a>
        <a class="collapse-item" href="cards.html">Menu Footer</a>
        </div>
    </div>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m5" aria-expanded="true" aria-controls="m5">
        <i class="fas fa-fw fa-cog"></i>
        <span>CMS Setting</span>
    </a>
    <div id="m5" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <!-- <h6 class="collapse-header">Custom Components:</h6> -->
        <a class="collapse-item" href="buttons.html">Settings</a>
        <a class="collapse-item" href="cards.html">Translation</a>
        <a class="collapse-item" href="cards.html">Mail Template</a>
        </div>
    </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m6" aria-expanded="true" aria-controls="m6">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Pengurusan Kluster</span>
    </a>
    <div id="m6" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <!-- <h6 class="collapse-header">Custom Utilities:</h6> -->
        <a class="collapse-item" href="">Icon Management</a>
        <a class="collapse-item" href="">Position Management</a>
        </div>
    </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m7" aria-expanded="true" aria-controls="m7">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Pengurusan Hebahan</span>
    </a>
    <div id="m7" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <!-- <h6 class="collapse-header">Custom Utilities:</h6> -->
        <a class="collapse-item" href="">Senarai Hebahan</a>
        <a class="collapse-item" href="">Cipta Hebahan Baru</a>
        </div>
    </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m8" aria-expanded="true" aria-controls="m8">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Pengurusan Laman</span>
    </a>
    <div id="m8" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <!-- <h6 class="collapse-header">Custom Utilities:</h6> -->
        <a class="collapse-item" href="">Senarai Laman</a>
        <a class="collapse-item" href="">Cipta Laman Baru</a>
        </div>
    </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m8" aria-expanded="true" aria-controls="m8">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Pengurusan Imej</span>
    </a>
    <!-- <div id="collapseUtilities3" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="">Senarai Laman</a>
        <a class="collapse-item" href="">Cipta Laman Baru</a>
        </div>
    </div> -->
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m9" aria-expanded="true" aria-controls="m9">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Pengurusan Soalan Lazim</span>
    </a>
    <div id="m9" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <!-- <h6 class="collapse-header">Custom Utilities:</h6> -->
        <a class="collapse-item" href="">Senarai Soalan Lazim</a>
        <a class="collapse-item" href="">Cipta Soalan Lazim Baru</a>
        </div>
    </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m10" aria-expanded="true" aria-controls="m10">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Article Management</span>
    </a>
    <div id="m10" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <!-- <h6 class="collapse-header">Custom Utilities:</h6> -->
        <a class="collapse-item" href="">Article List</a>
        <a class="collapse-item" href="">Create New Article</a>
        </div>
    </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m11" aria-expanded="true" aria-controls="m11">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Pengurusan Penyumbang Set Data</span>
    </a>
    <div id="m11" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="">Senarai Penyumbang Set Data</a>
        <a class="collapse-item" href="">Cipta Penyumbang Set Data Kustom</a>
        <a class="collapse-item" href="">Reload Semula Penyumbang Set Data</a>
        </div>
    </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m12" aria-expanded="true" aria-controls="m12">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Pengurusan Jenis Data</span>
    </a>
    <div id="m12" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="">Senarai Jenis Data</a>
        <a class="collapse-item" href="">Cipta Jenis Data Baharu</a>
        </div>
    </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m13" aria-expanded="true" aria-controls="m13">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Pengurusan Set Data Pilihan</span>
    </a>
    <!-- <div id="collapseUtilities4" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="">Senarai Jenis Data</a>
        <a class="collapse-item" href="">Cipta Jenis Data Baharu</a>
        </div>
    </div> -->
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m14" aria-expanded="true" aria-controls="m14">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Audit Trail</span>
    </a>
    <!-- <div id="collapseUtilities4" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="">Senarai Jenis Data</a>
        <a class="collapse-item" href="">Cipta Jenis Data Baharu</a>
        </div>
    </div> -->
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
    Other
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#m15" aria-expanded="true" aria-controls="m15">
        <i class="fas fa-fw fa-folder"></i>
        <span>Dataset Ticket</span>
    </a>
    <div id="m15" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <!-- <h6 class="collapse-header">Login Screens:</h6> -->
        <a class="collapse-item" href="">New Ticket List</a>
        <a class="collapse-item" href="">Manage Ticket</a>
        <!-- <a class="collapse-item" href="register.html">Register</a>
        <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
        <div class="collapse-divider"></div>
        <h6 class="collapse-header">Other Pages:</h6>
        <a class="collapse-item" href="404.html">404 Page</a>
        <a class="collapse-item" href="blank.html">Blank Page</a> -->
        </div>
    </div>
    </li>

    <!-- Nav Item - Charts -->
    <!-- <li class="nav-item">
    <a class="nav-link" href="charts.html">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Charts</span></a>
    </li> -->

    <!-- Nav Item - Tables -->
    <!-- <li class="nav-item">
    <a class="nav-link" href="tables.html">
        <i class="fas fa-fw fa-table"></i>
        <span>Tables</span></a>
    </li> -->

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
