<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Pengurusan Tiket</title>
    
    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <style>
            .status{
                align:left;
            }
            .filterable {
    margin-top: 15px;
}
.filterable .panel-heading .pull-right {
    margin-top: -20px;
}
.filterable .filters input[disabled] {
    background-color: transparent;
    border: none;
    cursor: auto;
    box-shadow: none;
    padding: 0;
    height: auto;
}
.filterable .filters input[disabled]::-webkit-input-placeholder {
    color: #333;
}
.filterable .filters input[disabled]::-moz-placeholder {
    color: #333;
}
.filterable .filters input[disabled]:-ms-input-placeholder {
    color: #333;
}

.heighttable{
    height: 450px; 
    overflow-y: scroll;
}

.heighttable::-webkit-scrollbar {
    display: none;
}

.th {
    font-size: 12px;
}
    </style>
</head>
<body>
<div class="container">
    <div class="panel panel-default">
    </div>
    @if($message = Session::get('success'))
    <div class="alert-success">
    <p>{{$message}}</p>
    </div>
    @endif
    <div class="card">
        <div class="card-body">
            <div class="panel panel-primary filterable heighttable">
                <table class="table table-bordered" >
                    <thead>
                        <tr>
                            <th>Tiket</th>
                            <th>Terakhir Kemaskini</th>
                            <th>Subjek</th>
                            <th>Daripada</th>
                            <th>Status</th>
                            <th>Pembekal Set Data</th>
                            <th>Action</td>
                        </tr>
                        <tr class="filters">
                            <th><input type="text" class="form-control" placeholder="Tiket" ></th>
                            <th><input type="text" class="form-control" placeholder="Terakhir Kemaskini" ></th>
                            <th><input type="text" class="form-control" placeholder="Subjek" ></th>
                            <th><input type="text" class="form-control" placeholder="Daripada" ></th>
                            <th><input type="text" class="form-control" placeholder="Status" ></th>
                            <th><input type="text" class="form-control" placeholder="Pembekal Set Data" ></th>
                            <th></th>
                        </tr>
                    </thead>
                        <tbody>
                            @foreach($ticket as $ticket)
                            <tr >
                                <td>{{$ticket->tick}}</td>
                                <td >{{$ticket->updated_at}}</td>
                                <td>{{$ticket->subjek}}</a></td>
                                <td>{{$ticket->name}}</td>
                                <td>{{$ticket->status}}</td>
                                <td>{{$ticket->pembekal}}</td>
                                <td>
                                        <button data-t_id="{{$ticket->id}}" data-subjek="{{ $ticket->subjek }}" data-name="{{ $ticket->name }}"
                                        data-status="{{ $ticket->status }}" data-pembekal="{{ $ticket->pembekal }}" data-description="{{ $ticket->description }}" 
                                        class="btn btn-info btn-sm" data-toggle="modal" data-target="#ticket-edit">Urus</button>

                                    <form action="{{ route('ticket.destroy',$ticket->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger btn-sm">Buang</button>
                                    </form>    
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
                  
        </div>
    </div>
</div>

<div class="modal" id="ticket-edit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title"><b>Kemas kini Permohonan Tiket</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('ticket.update','t_id') }}" method="POST">
                    @csrf
                    @method('PUT')
            
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Subjek:</strong>
                            <input type="text" name="subjek" id="subjek" class="form-control" placeholder="Sila tukar subjek anda" readonly>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                                <input type="ext" name="name" id="name" class="form-control" placeholder="Sila tukar nama pemohon">
                                <input type="hidden" id="t_id" name="t_id">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Status:</strong>
                                <select id="status" name="status" value="{{old('status')}}" class="form-control">
                                    @foreach($statuses ['data'] as $row)
                                    <option value='{{ $row->status_name }}'>{{ $row->status_name }}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Pembekal Set Data:</strong>
                                <select name="pembekal" id="pembekal" value="{{old('pembekal')}}" class="form-control">
                                    <option selected disabled>Sila Pilih</option>
                                    @foreach($sources ['data'] as $src)
                                    <option value='{{ $src->data_source_name }}'>{{ $src->data_source_name }}</option>
                                    @endforeach
                                </select>
                        </div> 
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Catatan:</strong>
                                <textarea rows="3" name="description" id="description" class="form-control" placeholder="Sila tukar nama pemohon" readonly>
                                </textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Ulasan Penolakan:</strong>
                                <select name="ulasan" id="ulasan" class="form-control">
                                    <option selected disabled>-Sila Pilih-</option>
                                    <option value="Tidak berkaitan">Tidak berkaitan</option>
                                    <option value="Set Data Tersedia">Set Data Tersedia</option>
                                    <option value="Data Maklumat Sulit / Terhad">Data Maklumat Sulit / Terhad</option>
                                    <option value="Set Data tidak boleh didapati">Set Data tidak boleh didapati</option>
                                </select>
                        </div> 
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a class="btn btn-secondary" href="http://localhost:8000/admin/ticket">Cancel</a>
        </div>
    </form>
    </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){

    $('.filterable .filters input').keyup(function(e){
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
        $inputContent = $input.val().toLowerCase(),
        $panel = $input.parents('.filterable'),
        $column = $panel.find('.filters th').index($input.parents('th')),
        $table = $panel.find('.table'),
        $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('td').eq($column).text().toLowerCase();
            return value.indexOf($inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
    });
});

$('#ticket-edit').on('show.bs.modal', function(event){

var button = $(event.relatedTarget)
var name = button.data('name')
var subjek = button.data('subjek')
var status = button.data('status')
var pembekal = button.data('pembekal')
var description = button.data('description')
var t_id = button.data('t_id')

var modal = $(this)

modal.find('.modal-body #name').val(name);
modal.find('.modal-body #subjek').val(subjek);
modal.find('.modal-body #status').val(status);
modal.find('.modal-body #pembekal').val(pembekal);
modal.find('.modal-body #description').val(description);
modal.find('.modal-body #t_id').val(t_id);
});
</script>
</body>
</html>
