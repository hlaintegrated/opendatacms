@extends('v2.admin.template.adminlayout')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content">
	<!-- START JUMBOTRON -->
	<div class="jumbotron" data-pages="parallax">
		<div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
			<div class="inner">
				<!-- START BREADCRUMB -->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Pengurusan Kluster</a></li>
					<li class="breadcrumb-item active">Icon Management</li>
				</ol>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- END JUMBOTRON -->
	<!-- START CONTAINER FLUID -->
	<div class="container-fluid container-fixed-lg">
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		{!! Form::open(['action'=>['ClusterController@save'],'files' => 'true'])!!}
		<div class="panel panel-default">
			<?php $counter = 0; ?>
			<div class="panel-heading">
				{{_('Cluster Management')}}
			</div>
			<div class="panel-body">
				<div style="overflow:auto">
					<table class="table table-bordered cluster-view-cell" align="center">
						<thead>
							<td>{{_('No')}}.</td>
							<td>{{_('Link')}}</td>
							<td>{{_('Title')}}</td>
							<td>{{_('Home Show')}}</td>
							<td>{{_('Image')}}</td>
							<td>{{_('Hover Image')}}</td>
						</thead>
						@foreach($clusters as $cluster)

						<tr>
							<input type="hidden" name="link[]" value="{{$cluster->link}}">
							<td>{{++$counter}}</td>
							<td>
								<p>{{$cluster->link}}</p>
							</td>
							<td>
								<p>{{T::_($cluster->title)}}</p>
							</td>
							<td>{!! Form::checkbox("show$cluster->link",null,$cluster->home_show) !!}</td>
							<td>
								<img src='{{asset("images/cluster/$cluster->image")}}' style='max-height:50px; max-width:50px'>
								{!! Form::file('image[]',null,[ 'id' => 'image','accept' =>'.png,.jpg,.bmp'])!!}
							</td>
							<td>
								<img src='{{asset("images/cluster/$cluster->hover_image")}}' style='max-height:50px; max-width:50px'>
								{!! Form::file('hover_image[]',null,['id' => 'image','accept' =>'.png,.jpg,.bmp'])!!}
							</td>

						</tr>

						@endforeach
					</table>
				</div>
			</div>
			<div class="panel-footer">
				<div class="row">
					<button class="btn btn-success col-md-4 col-md-offset-4">{{_('Save')}}</button>
				</div>
			</div>
		</div>
		{!! Form::close()!!}
		<!-- END PLACE PAGE CONTENT HERE -->
	</div>
	<!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
@endsection