@extends('v2.admin.template.adminlayout')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Pengurusan Kluster</a></li>
                    <li class="breadcrumb-item active">Position Management</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        {!! Form::open(['method' => 'POST','action'=>['ClusterController@savePosition'],'class'=>'form-horizontal'])!!}

        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-10">
                        {{_('Cluster Position Management')}}
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="drag-drop-row" id="portlet-row" style="padding:0px">
                    @foreach($clusters as $cluster)
                    <div class="portlet panel panel-default" id="{{$cluster->link}}">
                        <input type="hidden" name="link[]" value="{{$cluster->link}}">
                        <div class="portlet-header panel-heading">{{$cluster->link}}</div>
                        <div class="portlet-body panel-body">
                            {{$cluster->title}}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="panel-footer">
                <div class='form-group'>
                    <div class='col-sm-3'></div>
                    <div class='col-sm-8'>
                        <button type='submit' class='form-control btn btn-primary'>{{_('Save')}}</button>
                    </div>
                    <div class='col-sm-1'></div>
                </div>
            </div>
        </div>

        {!! Form::close() !!}
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
@endsection