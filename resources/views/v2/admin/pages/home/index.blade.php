@extends('administrator.layout.default')

@section('script')
<script src="{{asset('assets/jquery/jquery-ui.min.js')}}"></script>
<script src="{{ asset('assets/app/js/dragdropobject.js')}}"></script>
<script>
function addHome(name)
{
    var target = '{{url("administrator/card/home/")}}/'+name;
    $.get(target,function(data){
        $("#portlet-row").append(data);
    });
    $("#portlet-row").sortable('refresh');
}
</script>
@stop

@section('content')
@foreach($homes as $home)
{!!Form::model($home,['method'=>'DELETE','action'=>['HomeController@destroy',$home->id],'id'=>'home'.$home->id])!!}{!!Form::close()!!} 
@endforeach

{!! Form::open(['method' => 'POST','action'=>['HomeController@save'],'class'=>'form-horizontal'])!!}
   
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-10">
            {{_('Home Widget Management')}}
            </div>
            <div class="col-sm-2">
                <a href="{{action('HomeController@create')}}" class="btn btn-primary">{{_('Add Widget')}}</a>                    
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="drag-drop-row" id="portlet-row" style="padding:0px">
            @foreach($homes as $home)
            <div class="portlet panel panel-default" id="{{$home->id}}">
                <input type="hidden" name="id[]" value="{{$home->id}}">
                <div class="portlet-header panel-heading">{{$home->title}}</div>
                <div class="portlet-content panel-body">Description: {{$home->description}}
                    <button title="{{_('Remove Home')}}"  type="button" onClick="deleteConfirmation('home{{$home->id}}')" class="btn btn-danger glyphicon glyphicon-minus" style="float:right"></button>
                    <a href="{{action('HomeController@edit',$home->id)}}" class="btn btn-success glyphicon glyphicon-edit" style="float:right; margin-right: 1%"></a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="panel-footer">
        <div class='form-group'>
            <div class='col-sm-3'></div>
            <div class='col-sm-8'>
                <button type='submit' class='form-control btn btn-primary'>{{_('Save')}}</button>
            </div>
            <div class='col-sm-1'></div>
        </div>
    </div>
</div>

{!! Form::close() !!}
@stop