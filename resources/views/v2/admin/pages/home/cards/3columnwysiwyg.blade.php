<div class="row">
    @for($i = 0; $i < 3; $i++)
    <div class="col-md-4 ">
        <div class="panel panel-3column home-widget">
            <div class="panel-heading">
                <input type="text" name="-title[]" class="form-control" placeholder="Title" value="{{isset($content)?$content[$i]['title']:''}}">
            </div>
            <div class="panel-body">
                {!! Form::textarea('-wysiwyg[]',isset($content)?$content[$i]['wysiwyg']:null,['class'=>'form-control', 'id' => 'wysiwyg'.$i,'rows'=>'10','cols'=>'80'])!!}
            </div>
            <div class="panel-footer">
                <div class="row form-group">
                    <div class="col-md-6">
                        <input type='text' name='-linktitle[]' class='form-control' placeholder='Link Title' value='{{isset($content)?$content[$i]['linktitle']:''}}'>
                    </div>
                    <div class="col-md-6">
                        <input type='text' name='-linkref[]' class='form-control' placeholder='Link Ref' value='{{isset($content)?$content[$i]['linkref']:''}}'>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endfor
</div>
