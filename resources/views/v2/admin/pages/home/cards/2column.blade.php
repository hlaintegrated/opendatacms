
<div class="row">
    @for($i = 0; $i < 2; $i++)
    <div class="col-md-6 ">
        <div class="panel panel-2column home-widget">
            <div class="panel-heading">
                <input type="text" name="-title[]" class="form-control" placeholder="Title" value="{{isset($content)?$content[$i]['title']:''}}">
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">                
                        <textarea name="-content[]" rows="14" class="form-control" >{!!isset($content)?$content[$i]['content']:''!!}</textarea>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row form-group">
                    <div class="col-md-6">
                        <input type='text' name='-linktitle[]' class='form-control' placeholder='Link Title' value='{{isset($content)?$content[$i]['linktitle']:''}}'>
                    </div>
                    <div class="col-md-6">
                        <input type='text' name='-linkref[]' class='form-control' placeholder='Link Ref' value='{{isset($content)?$content[$i]['linkref']:''}}'>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endfor
</div>