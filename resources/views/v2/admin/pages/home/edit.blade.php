@extends('administrator.layout.default')
@section('script')
<script>
	homeedit({{$home->id}});
</script>
@stop
@section('content')
<div class="panel panel-default ">
	<div class="panel-heading ">
		Edit home {{$home->title}}
	</div>
	<div class="panel-body ">
		{!! Form::model($home,['id'=>'homeform','method' => 'PATCH','action'=>['HomeController@update',$home->id],'class'=>'form-horizontal','files'=>true])!!}

			@include('administrator.pages.home.form')

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-success">{{_("Save")}}</button>
			</div>
		</div>
		{!! Form::close()!!}
	</div>
</div>
@stop