@section('additionalscript')
<script>
var chartgeneratorurl = "{{url('administrator/home/chartgenerator')}}";
$(document).ready()
{
	var types=[
		<?php $first = true; ?>
		@foreach(Home::types() as $index=>$type)<?php
				if ($first)
				{
					$first = false;
				}
				else
				{
					echo ',';
				}
				?>'{{$index}}'@endforeach
	];
	var select=document.getElementById('types');
	
    function check_ckeditor(value)
    {
        var num = 0;
        if (value == '2columnwysiwyg')
            num = 2;
        if (value == '3columnwysiwyg')
            num = 3;
        for (var i = 0; i < num; i++)
        {
            CKEDITOR.replace('wysiwyg'+i);
        }
    }
    
	function hometypechange()
	{
		var value = select.value;
		var target = "{{url('administrator/home/card')}}/"+value;
		$.get(target,function(data)
		{
			$("#ajaxform").html(data);
            check_ckeditor(value);
		});		
	}
	
	function homeedit($id)
	{
		var target = "{{url('administrator/home/card')}}/"+$id+"/edit";
		$.get(target,function(data)
		{
			$("#ajaxform").html(data);
			onPageLoad();
            check_ckeditor(id);
		});
	}
}
</script>
@stop
<!-- Text Box for Title -->
<div class="form-group">
	<div class="col-md-1"></div>
	{!! Form::label('title',_("Title").'*',['class' => 'control-label col-md-2']) !!}
	<div class="col-md-8">
		{!! Form::text('title',null,['class'=>'form-control', 'id' => 'title','required'=>'required'])!!}
		<input type="checkbox" name="show_title" @if(isset($home) && $home->title_show)checked @endif > <label class="form-label">Show</label>
	</div>
	<div class="col-md-1"></div>
</div>

<!-- Text Box for Description -->
<div class="form-group">
	<div class="col-md-1"></div>
	{!! Form::label('description',_("Description").'*',['class' => 'control-label col-md-2']) !!}
	<div class="col-md-8">
		{!! Form::text('description',null,['class'=>'form-control', 'id' => 'description','required'=>'required'])!!}
	</div>
	<div class="col-md-1"></div>
</div>

<div class="form-group">
	<div class="col-md-1"></div>
	{!! Form::label('type',_("Type"),['class' => 'control-label col-md-2']) !!}
	<div class="col-md-8">
		{!! Form::select('type', Home::types(),null,['class'=>'form-control','id'=>'types','onchange'=>'hometypechange()']) !!}
	</div>
	<div class="col-md-1"></div>
</div>
<div class="form-group" id="ajaxform">
</div>

@include('administrator.layout.partials.chartprocessor')

@section('loader')
	@include('partials.loader')
@stop