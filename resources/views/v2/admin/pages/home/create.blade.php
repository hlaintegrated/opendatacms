@extends('administrator.layout.default')
@section('script')
<script>
	hometypechange();
</script>
@stop
@section('content')
<div class="panel panel-default ">
		<div class="panel-heading ">
			{{_("Add Home")}}
		</div>
		<div class="panel-body ">
			{!! Form::open(['id'=>'homeform','method' => 'POST','action'=>['HomeController@store'],'class'=>'form-horizontal','files'=>true])!!}

				@include('administrator.pages.home.form')

			<div class="form-group">
				<div class="col-sm-4 col-sm-offset-4">
					<button type="submit" class="form-control btn btn-primary">{{_("Create")}}</button>
				</div>
			</div>
			
			
			{!! Form::close()!!}
		</div>
</div>
@stop