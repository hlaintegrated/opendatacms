@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		{{_('Edit user')}} {{$user->fullname}}
	</div>
	<div class="panel-body">
		{!! Form::model($user,['method' => 'PATCH','action'=>['UserController@update',$user->id],'class'=>'form-horizontal'])!!}
<input type="hidden" name="id" value="{{$user->id}}">
			@include('administrator.pages.user.form')

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-success">{{_('Save')}}</button>
			</div>
		</div>
		{!! Form::close() !!}

	</div>
</div>
@stop