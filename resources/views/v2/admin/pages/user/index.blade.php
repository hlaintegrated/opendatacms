@extends('v2.admin.template.adminlayout')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
                    <li class="breadcrumb-item active">Pengurusan Pengguna</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="panel panel-default panel-admin">
            <?php $counter = 0; ?>
            <div class="panel-heading">
                Pengurusan Pengguna
            </div>
            <div class="panel-body">

                <div class="row" style="margin-bottom: 10px">
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label class="control-label">Search Name</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-block btn-primary">{{_("Search")}}</button>
                        </div>
                    </div>
                </div>
               
                <div style="overflow:auto" class="row">
                    <table class="table table-bordered" align="center" style="white-space: nowrap">
                        <thead>
                            <td>No.</td>
                            <td>{{_("Username")}}</td>
                            <td>{{_("Created")}}</td>
                            <td>{{_("Full Name")}}</td>
                            <td>{{_("Email")}}</td>
                            <td>{{_("Sysadmin")}}</td>
                            @if(env("TICKETING","default") == "default")
                            <td>{{_("Data Source")}}</td>@endif
                            <td>{{_("State")}}</td>
                            <td class="text-center" colspan="4">{{_("Action")}}</td>
                        </thead>
                        @foreach($users as $user)
                        <tr>
                            <td>{{++$counter}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->created}}</td>
                            <td>{{$user->fullname}}</td>
                            <td>{{$user->email}}</td>
                            <td align="center">
                                @if ($user->sysadmin)
                                <i class="fa fa-check-circle fa-2x green" title="{{_('Sysadmin')}}"></i>
                                @else
                                <i class="fa fa-times-circle fa-2x red" title="{{_('Not Sysadmin')}}"></i>
                                @endif
                            </td>
                            @if(env("TICKETING","default") == "default")
                            <td align="center">
                                @if ($user->data_source)
                                <i class="fa fa-check-circle fa-2x green" title="{{_('Data Source')}}"></i>
                                @else
                                <i class="fa fa-times-circle fa-2x red" title="{{_('Not Data Source')}}"></i>
                                @endif
                            </td>@endif
                            <td align="center">
                                @if ($user->state == 'active')
                                <i class="fa fa-check-circle fa-2x green" title="{{_('Active')}}"></i>
                                @elseif ($user->state =='suspended')
                                <i class="fa fa-times-circle fa-2x red" title="{{_('Suspended')}}"></i>
                                @else
                                <a href="{{action("UserController@resend",$user->id)}}"><i class="fa fa-envelope fa-2x yellow" title="{{_('Waiting For Activation. Click here for resending email')}}"></i></a>
                                @endif
                            </td>
                            <td>
                                @if (!$user->sysadmin)
                                <button class="btn btn-success glyphicon glyphicon-edit" title="{{_('Edit User')}}" onClick="window.location ='{{action('UserController@edit',$user->id)}}'"></button>
                                @endif
                            </td>
                            <td>
                                @if (!$user->sysadmin)
                                <button class="btn btn-danger glyphicon glyphicon-remove-circle" title="{{_('Delete User')}}" onClick="alerts('{{$user->id}}','{{$user->name}}')"></button>
                                @endif
                            </td>
                            <td>
                                @if (!$user->sysadmin)
                                @if ($user->state == 'active')
                                <button class="btn btn-danger glyphicon glyphicon-minus" title="{{_('Suspend User')}}" onClick="window.location = '{{action('UserController@suspend',$user->id)}}'"></button>
                                @else
                                <button class="btn btn-primary glyphicon glyphicon-plus" title="{{_('Unsuspend User')}}" onClick="window.location = '{{action('UserController@unsuspend',$user->id)}}'"></button>
                                @endif
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-primary glyphicon glyphicon-envelope" title="{{_('Reset Activation And Password Reset Email')}}" href="{{action("UserController@resend",$user->id)}}"></a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div align="center">
            <ul class="pagination pagination-lg">
            </ul>
        </div>
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
@endsection

@section('script')
<script>
    function alerts(user, name) {
        console.log(user, name);
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete ' + name + '?',
            buttons: {
                confirm: function() {
                    window.location = "/administrator/user/" + user + "/destroy";
                },
                cancel: function() {
                    $.alert('Canceled!');
                },

            }
        });
    }
</script>

@stop