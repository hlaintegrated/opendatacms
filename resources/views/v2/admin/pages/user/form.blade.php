Mandatory fields are marked with (*)<br/>


<!-- Text Box for Username -->
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
	<div class="col-md-1"></div>
	<?php /* {!! Form::label('name',_('Username').'*',['class' => 'control-label col-md-2']) !!} */ ?>
	<div class="col-md-8">
		<?php /* {!! Form::text('name',null,['class'=>'form-control', 'id' => 'name', 'value' => old('value'),'style'=>'text-transform: lowercase;','required'=>'required'])!!} */ ?>
	</div>
	<div class="col-md-1"></div>
</div>
<!-- Text Box for Full Name -->
<div class="form-group">
	<div class="col-md-1"></div>
	<?php /* {!! Form::label('fullname',_('Full Name').'*',['class' => 'control-label col-md-2']) !!} */ ?>
	<div class="col-md-8">
		<?php /* {!! Form::text('fullname',null,['class'=>'form-control', 'id' => 'full_name','required'=>'required'])!!} */ ?>
	</div>
	<div class="col-md-1"></div>
</div>

<!-- Text Box for Email -->
<div class="form-group">
	<div class="col-md-1"></div>
	<?php /* {!! Form::label('email',_('Email').'*',['class' => 'control-label col-md-2']) !!} -->
	<div class="col-md-8">
		<?php /* {!! Form::text('email',null,['class'=>'form-control', 'id' => 'email','required'=>'required'])!!} */ ?>
	</div>
	<div class="col-md-1"></div>
</div>

@if(env("TICKETING","default") == "default")
<div class="form-group ">
	<div class="col-sm-1"></div>
	<label for ='data_source' class ='control-label col-sm-2'>{{_("Data Source")}}</label>
	<div class="col-sm-8">
		<?php /* {!! Form::select('data_source',['true'=>'True','false'=>'False'],null,['class'=>'form-control'])!!} */ ?>
	</div>
	<div class="col-sm-1"></div>
</div>
@endif

@if (!isset($user))
<div class="form-group">
	<div class="col-sm-1"></div>
	<label for ='data_source' class ='control-label col-sm-2'>{{_("System Administrator")}}</label>
	<div class="col-sm-8">
		<?php /* {!! Form::select('system_administrator',['true'=>'True','false'=>'False'],null,['class'=>'form-control'])!!} */ ?>
	</div>
	<div class="col-sm-1"></div>
</div>
@endif