@extends('v2.admin.template.adminlayout')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content">
	<!-- START JUMBOTRON -->
	<div class="jumbotron" data-pages="parallax">
		<div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
			<div class="inner">
				<!-- START BREADCRUMB -->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Pengguna</a></li>
					<li class="breadcrumb-item active">Cipta Pengguna Baru</li>
				</ol>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- END JUMBOTRON -->
	<!-- START CONTAINER FLUID -->
	<div class="container-fluid container-fixed-lg">
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		<div class="panel panel-default">
			<div class="panel-heading">
				{{_('Create New User')}}
			</div>
			<div class="panel-body">

				@include('v2.admin.template.pages.user.form')

				<div class="form-group">
					<div class="col-sm-4 col-sm-offset-4">
						<button type="submit" class="form-control btn btn-primary">{{_('Register')}}</button>
					</div>
				</div>
			</div>
		</div>
		<!-- END PLACE PAGE CONTENT HERE -->
	</div>
	<!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
@endsection