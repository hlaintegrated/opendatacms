@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		{{_('Edit Mail Template')}} {{$template->name}}
	</div>
	<div class="panel-body">
		{!! Form::model($template,['method' => 'PATCH','action'=>['MailTemplateController@update',$template->id],'class'=>'form-horizontal'])!!}

		<div class="form-group">
			<div class="col-sm-2">
				<label class="control-label">Name</label>
			</div>
			<div class="col-sm-10">
				<label class="control-label">{{$template->name}}</label>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-2">
				<label class="control-label">Description</label>
			</div>
			<div class="col-sm-10">
				{!! Form::textarea("description",null,['class'=>'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-2">
				<label class="control-label">Template</label>
			</div>
			<div class="col-sm-10">
				{!! Form::textarea("template",null,['class'=>'form-control','id'=>'template-editor']) !!}
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-2">
				<label class="control-label">Available Variables: </label>
			</div>
			<div class="col-sm-10">
				<ul class="list-group">
				@foreach($template->template_fields_array() as $field)
					<li class="list-group-item list-group-item-info">${{$field}}</li>
				@endforeach
				</ul>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-success">{{_('Save')}}</button>
			</div>
		</div>
		{!! Form::close() !!}

	</div>
</div>
@stop

@section('additionalscript')
<script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
<script>
    CKEDITOR.replace( 'template-editor' );
</script>
@stop