@extends('v2.admin.template.adminlayout')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content">
	<!-- START JUMBOTRON -->
	<div class="jumbotron" data-pages="parallax">
		<div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
			<div class="inner">
				<!-- START BREADCRUMB -->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Tetapan CMS</a></li>
					<li class="breadcrumb-item active">Mail Template</li>
				</ol>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- END JUMBOTRON -->
	<!-- START CONTAINER FLUID -->
	<div class="container-fluid container-fixed-lg">
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		<div class="panel panel-default panel-admin">

			<?php $counter = 0; ?>
			<div class="panel-heading">
				{{_('Mail Template Management')}}
			</div>
			<div class="panel-body">
				<div style="overflow:auto">
					<table class="table table-bordered" align="center" style="white-space: nowrap">
						<thead>
							<td>No.</td>
							<td>{{_("Name")}}</td>
							<td>{{_("Description")}}</td>
							<td colspan="1">{{_("Edit")}}</td>
						</thead>
						@foreach($templates as $template)
						<tr>
							<td>{{++$counter}}</td>
							<td>{{$template->name}}</td>
							<td>{{$template->description}}</td>
							<td>
								<a class="btn btn-primary glyphicon glyphicon-edit" href="{{action("MailTemplateController@edit",$template->id)}}"></a>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
		<!-- END PLACE PAGE CONTENT HERE -->
	</div>
	<!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
@endsection