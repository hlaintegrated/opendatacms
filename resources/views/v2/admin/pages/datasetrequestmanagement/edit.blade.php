@extends('administrator.layout.request')
@section('content')
{!! Form::model($dataset_request,['method'=>'POST','action'=>['DatasetRequestManagementController@update',$dataset_request->request_id]])!!}
<div class="panel panel-default" style="margin-top:20px">
	<div class="panel-heading">
		{{_('Dataset Request Detail')}}
	</div>
	<div class="panel-body">
		<table class="table-striped table-hover" width="100%">
			<tr style="height: 30px">
				<td style="width: 20%">{{_('Sender')}}</td>
				<td>{{$dataset_request->name}}</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Email')}}</td>
				<td>{{$dataset_request->email}}</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Phone')}}</td>
				<td>{{$dataset_request->phone}}</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Title')}}</td>
				<td>{{$dataset_request->title}}</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Description')}}</td>
				<td>{{$dataset_request->description}}</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Remarks')}}</td>
				<td>{{$dataset_request->notes}}</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Sources')}}</td>
				<td>
					@if ($dataset_request->status == 'pending')
					@foreach($sources as $source)
					<div class="col-sm-4">
						<input type="checkbox" name="source[]" id="{{$source->data_source_id}}" value="{{$source->data_source_id}}" <?= in_array($source->data_source_id,$dataset_request->sources)?'checked':''?> >
						<label for="{{$source->data_source_id}}" class="form-label">{{_("$source->data_source_name")}}</label><br/>
					</div>
					@endforeach-->
					@else
					<ul>
						@foreach($dataset_request->sources as $source)
						<li>{{T::_($source->data_source_name)}}</li>
						@endforeach
					</ul>
					@endif
				</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Type')}}</td>
				<td>
					@if ($dataset_request->status == 'pending')
					@foreach($types as $type)
					<div class="col-sm-4">
						<input type="checkbox" name="type[]" id="{{$type->data_type_id}}" value="{{$type->data_type_id}}" <?= in_array($type->data_type_id,$dataset_request->types)?'checked':''?>>
						<label for="{{$type->data_type_id}}" class="form-label">{{$type->data_type_name}}</label><br/>
					</div>
					@endforeach
					@else
					<ul>
						@foreach($dataset_request->types as $type)
						<li>{{$type->data_type_name}}</li>
						@endforeach
					</ul>
					@endif
				</td>
			</tr>
			<tr style="height: 30px">
				<td style="width: 20%">{{_('Status')}}</td>
				<td>
					@include('administrator.pages.datasetrequest.status')
				</td>
			</tr>
			<tr style="height: 30px">
				<td style="width: 20%"></td>
				<td>
					@if ($dataset_request->status == 'pending')
					<input type="hidden" name="status" value="in_progress">
					<button class="btn btn-primary">Accept</button>
					<a href="{{action('DatasetRequestManagementController@reject',$dataset_request->request_id)}}" class="btn btn-danger">Reject</a>
					@elseif ($dataset_request->status == 'in_progress')
					<input type="hidden" name="status" value="completed">
					<button class="btn btn-primary">Mark Finished</button>
					<a href="{{action('DatasetRequestManagementController@reject',$dataset_request->request_id)}}" class="btn btn-danger">Reject</a>
					@endif
				</td>
			<tr>
		</table>
		{!! Form::close() !!}
	</div>
</div>
@stop