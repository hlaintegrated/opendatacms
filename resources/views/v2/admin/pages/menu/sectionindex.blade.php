@extends('v2.admin.template.adminlayout')

@section('additionalstyle')
<link rel="stylesheet" href="{{asset('assets/app/css/dragdropobject.css')}}" type="text/css" />
@stop
@section('additionalscript')
<script src="{{ asset('assets/app/js/dragdropobject.js')}}"></script>
<script src="{{asset('assets/jquery/jquery-ui.min.js')}}"></script>
@stop


@section('content')
<!-- START PAGE CONTENT -->
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Tetapan Menu</a></li>
                    <li class="breadcrumb-item active">Menu Header</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		<div class="row">
			<div class="form-group col-md-10">
				<div class="form-group">
					<div class="col-md-2">
						<label for="menu" class="control-label col-md-2">{{_('Menu')}}</label>
					</div>
					<div class="col-md-10">
						<select class="form-control" required name="menu" id="portlet-add">
							<option value="noselect" disabled selected>{{_("Select Available Menu")}}</option>
							@foreach($selects as $select)
							<option value="{{$select->link}}">{{$select->link}} - {{$select->title}}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<button class="btn btn-primary" onclick="addMenu()">{{_("Add")}}</button>
			</div>
		</div>



		<div class="row panel panel-default">

			<div class="panel-heading">
				{{_($page." Menu Position Management")}}
			</div>
			<div class="panel-body" >
				<div class="drag-drop-row" id="portlet-row" style="padding:0px">
				@foreach($menus as $menu)
					<div class="portlet panel panel-default" id="{{$menu->link}}">
		                <input type="hidden" name="links[]" value="{{$menu->link}}">
						<div class="portlet-header panel-heading">{{$menu->title}}</div>
						<div class="portlet-content panel-body">Link: {{$menu->link}}
							<button type="button" title="{{_('Remove Menu')}}"  onClick="deleteConfirmation('menu{{$menu->link}}')" class="btn btn-danger glyphicon glyphicon-minus" style="float:right"></button>
						</div>
					</div>
				@endforeach
				</div>
			</div>
		</div>
        <div class="col-sm-4 col-sm-offset-4">
		    <button id="save-footer" class="btn btn-success btn-block">{{_("Save")}}</button>
        </div>
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
@endsection