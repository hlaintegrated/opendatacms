<div class="row">
	<div class="form-group col-md-10">
		<div class="form-group">
			<div class="col-md-2">
				<label for="menu" class="control-label col-md-2">{{_('Menu')}}</label>
			</div>
			<div class="col-md-10">
				<select class="form-control" name="menu" id="portlet-add">
					<option id="noselect" value="noselect">{{_("Select Available Menu")}}</option>
					@foreach($selects as $select)
					<option id="{{$select->link}}" value="{{$select->link}},{{$select->title}}">{{$select->link}} - {{$select->title}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<button class="btn btn-primary" type="button" onclick="addMenu()">{{_("Add")}}</button>
	</div>
</div>

<div class="row panel panel-default">

	<div class="panel-heading">
		{{_($page." Menu Position Management")}}
	</div>
	<div class="panel-body" >
		<div class="drag-drop-row" id="portlet-row" style="padding:0px">
		@foreach($menus as $menu)
			<div class="portlet panel panel-default" id="{{$menu->link}}">
                <input type="hidden" name="links[]" value="{{$menu->link}}">
				<div class="portlet-header panel-heading">{{$menu->title}}</div>
				<div class="portlet-content panel-body">Link: {{$menu->link}}
					<button type="button" title="{{_('Remove Menu')}}"  onClick="removeMenu('{{$menu->link}},{{$menu->link}},{{$menu->title}}')" class="btn btn-danger glyphicon glyphicon-minus" style="float:right"></button>
				</div>
			</div>
		@endforeach
		</div>
	</div>
</div>