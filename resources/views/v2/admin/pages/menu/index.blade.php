@extends('v2.admin.template.adminlayout')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content">
	<!-- START JUMBOTRON -->
	<div class="jumbotron" data-pages="parallax">
		<div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
			<div class="inner">
				<!-- START BREADCRUMB -->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Tetapan Menu</a></li>
					<li class="breadcrumb-item active">Senarai Menu</li>
				</ol>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- END JUMBOTRON -->
	<!-- START CONTAINER FLUID -->
	<div class="container-fluid container-fixed-lg">
		<!-- BEGIN PlACE PAGE CONTENT HERE -->

		<div class="container-fluid container-fixed-lg bg-white">
			<!-- START card -->
			<div class="card card-transparent">
				<div class="card-header  d-flex justify-content-between">
					<div class="card-title">Senarai Menu
					</div>
					<div class="export-options-container"></div>
					<!-- <div class="clearfix"></div> -->
				</div>
				<div class="card-body">
					<table class="table table-striped" id="tableWithExportOptions">
						<thead>
							<tr>
								<td>{{_('No')}}.</td>
								<td>{{_("Link")}}</td>
								<td>{{_("Title")}}</td>
								<td>{{_("Target")}}</td>
								<td>{{_("Created At")}}</td>
								<td>{{_("Updated At")}}</td>
								<td colspan="2">{{_("Action")}}</td>
							</tr>
						</thead>
						<tbody>
							<tr class="odd gradeX">
								<td>Trident</td>
								<td>Internet Explorer 4.0</td>
								<td>Win 95+</td>
								<td class="center"> 4</td>
								<td class="center">X</td>
								<td class="center">X</td>
								<td class="center">X</td>
								<td class="center">X</td>
							</tr>
							<tr class="even gradeC">
								<td>Trident</td>
								<td>Internet Explorer 5.0</td>
								<td>Win 95+</td>
								<td class="center">5</td>
								<td class="center">C</td>
								<td class="center">X</td>
								<td class="center">X</td>
								<td class="center">X</td>
							</tr>
							<tr class="odd gradeA">
								<td>Trident</td>
								<td>Internet Explorer 5.5</td>
								<td>Win 95+</td>
								<td class="center">5.5</td>
								<td class="center">A</td>
								<td class="center">X</td>
								<td class="center">X</td>
								<td class="center">X</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END card -->
		</div>
		<!-- END PLACE PAGE CONTENT HERE -->
	</div>
	<!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
@endsection