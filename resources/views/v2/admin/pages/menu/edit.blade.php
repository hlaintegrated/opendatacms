@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		Edit menu {{$menu->link}}
	</div>
	<div class="panel-body">
		{!! Form::model($menu,['method' => 'PATCH','action'=>['MenuController@update',$menu->link],'class'=>'form-horizontal multi-language-form','data-language-input'=>'title','files'=>'true'])!!}

			@include('administrator.pages.menu.form')

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-success">{{_("Save")}}</button>
			</div>
		</div>
		{!! Form::close()!!}
	</div>
</div>
@stop