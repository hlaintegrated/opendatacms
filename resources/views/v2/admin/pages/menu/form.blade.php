<div class="row">
    <div class="col-md-8 col-xlg-8">
        <!-- START card -->
        <div class="card">
            <div class="card-header ">
                <div class="card-title">Separated form layouts
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="mw-80">Simple but not simpler, create a separate form layout. Best for settings pages.</h3>
                        <p class="mw-80">Want it to be more Descriptive and User-Friendly, We Made it possible, Use Separated Form Layouts Structure to Presentation your Form Fields.
                        </p>
                        <br>
                        <form id="form-work" class="form-horizontal" role="form" autocomplete="off">
                            <div class="form-group row">
                                <label for="fname" class="required col-md-5 control-label">Pautan</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control" id="pautan" placeholder="" name="pautan" required>
                                    <span class="help">Pautan mestilah unik dan belum pernah didaftarkan</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-5 control-label required">Target</label>
                                <div class="col-md-7">
                                    <select class="cs-select cs-skin-slide full-width" data-init-plugin="cs-select">
                                        <optgroup label="Alaskan/Hawaiian Time Zone">
                                            <option value="AK">Alaska</option>
                                            <option value="HI">Hawaii</option>
                                        </optgroup>
                                        <optgroup label="Pacific Time Zone">
                                            <option value="CA">California</option>
                                            <option value="NV">Nevada</option>
                                            <option value="OR">Oregon</option>
                                            <option value="WA">Washington</option>
                                        </optgroup>
                                        <optgroup label="Mountain Time Zone">
                                            <option value="AZ">Arizona</option>
                                            <option value="CO">Colorado</option>
                                            <option value="ID">Idaho</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="UT">Utah</option>
                                            <option value="WY">Wyoming</option>
                                        </optgroup>
                                        <optgroup label="Central Time Zone">
                                            <option value="AL">Alabama</option>
                                            <option value="AR">Arkansas</option>
                                            <option value="IL">Illinois</option>
                                            <option value="IA">Iowa</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                            <option value="LA">Louisiana</option>
                                            <option value="MN">Minnesota</option>
                                            <option value="MS">Mississippi</option>
                                            <option value="MO">Missouri</option>
                                            <option value="OK">Oklahoma</option>
                                            <option value="SD">South Dakota</option>
                                            <option value="TX">Texas</option>
                                            <option value="TN">Tennessee</option>
                                            <option value="WI">Wisconsin</option>
                                        </optgroup>
                                        <optgroup label="Eastern Time Zone">
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="FL">Florida</option>
                                            <option value="GA">Georgia</option>
                                            <option value="IN">Indiana</option>
                                            <option value="ME">Maine</option>
                                            <option value="MD">Maryland</option>
                                            <option value="MA">Massachusetts</option>
                                            <option value="MI">Michigan</option>
                                            <option value="NH">New Hampshire</option>
                                            <option value="NJ">New Jersey</option>
                                            <option value="NY">New York</option>
                                            <option value="NC">North Carolina</option>
                                            <option value="OH">Ohio</option>
                                            <option value="PA">Pennsylvania</option>
                                            <option value="RI">Rhode Island</option>
                                            <option value="SC">South Carolina</option>
                                            <option value="VT">Vermont</option>
                                            <option value="VA">Virginia</option>
                                            <option value="WV">West Virginia</option>
                                        </optgroup>
                                    </select>
                                    <span class="help">Look for the referral person name in your account xm</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="position" class="col-md-5 control-label">Test Configuration</label>
                                <div class="col-md-7">
                                    <button aria-label="" class="btn btn-default m-b-10 m-t-5">Test with my project</button>
                                    <span class="help">Test if your pages UI framework works with your backend project correctly and
                                        adaptively</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-5 control-label">Project SSL Scope</label>
                                <div class="col-md-7">
                                    <textarea class="form-control" rows="4" id="name" placeholder="Briefly Describe your reports"></textarea>
                                    <span class="help">Add report descriptions and analysis data report to provide a SSL certificate
                                        function clearer</span>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="small-text hint-text">Note: changes may take some time to apply. Wait a day and then try to verify again </p>
                                </div>
                                <div class="col-md-6">
                                    <button aria-label="" class="btn btn-success pull-right" type="submit">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END card -->
    </div>
</div>

@section('additionalcssscript')
<link href="/v2/cms/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
@endsection

@section('additionaljsscript')
<script type="text/javascript" src="/v2/cms/assets/plugins/classie/classie.js"></script>
<script type="text/javascript" src="/v2/cms/assets/plugins/select2/js/select2.full.min.js"></script>
@endsection