@extends('administrator.layout.default')

@section('additionalstyle')
<link rel="stylesheet" href="{{asset('assets/app/css/dragdropobject.css')}}" type="text/css" />
@stop
@section('additionalscript')
<script src="{{ asset('assets/app/js/dragdropobject.js')}}"></script>
<script src="{{asset('assets/jquery/jquery-ui.min.js')}}"></script>
@stop


@section('content')
    {!! Form::open(['method'=>'POST','action'=>['MenuController@footersave']])!!}
    @include('administrator.pages.menu.positionform')

        <div class="col-sm-4 col-sm-offset-4">
		    <button id="save-footer" class="btn btn-success btn-block">{{_("Save")}}</button>
        </div>

    {!! Form::close() !!}
@stop