@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		{{_("Add Article")}}
	</div>
	<div class="panel-body">
		{!! Form::open(['method' => 'POST','action'=>['ArticleController@store'],'class'=>'form-horizontal multi-language-form','data-language-input'=>'name','files'=>'true','id'=>'article-multi-language-form'])!!}

			@include('administrator.pages.article.form')

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-primary">{{_("Create")}}</button>
			</div>
		</div>
		{!! Form::close()!!}
	</div>
</div>
@stop