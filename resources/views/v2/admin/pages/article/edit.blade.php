@extends('administrator.layout.default')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{_('Edit Article')}} {{$article->link}}
        </div>
        <div class="panel-body">
            {!! Form::model($article,['method' => 'PATCH','action'=>['ArticleController@update',$article->link],'class'=>'form-horizontal multi-language-form','data-language-input'=>'name','id'=>'article-multi-language-form'])!!}

            @include('administrator.pages.article.form')

            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-4">
                    <button type="submit" class="form-control btn btn-success">{{_("Save")}}</button>
                </div>
            </div>
            {!! Form::close()!!}
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading ">
            <div class="row">
                <div class="col-sm-11">
                    {{_('Article Item Management')}}
                </div>
                <div class="col-sm-1">
                    <a class="btn btn-primary btn-block" href='{{action("ArticleItemController@create",$id)}}'><i
                                class="glyphicon glyphicon-plus"></i></a>
                </div>
            </div>
        </div>
        <style>
            .card {
                position: relative;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: vertical;
                -webkit-box-direction: normal;
                -ms-flex-direction: column;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 1px solid rgba(0, 0, 0, .125);
                border-radius: .25rem;
            }

            .card-header {
                padding: .75rem 1.25rem;
                margin-bottom: 0;
                background-color: rgba(0, 0, 0, .03);
                border-bottom: 1px solid rgba(0, 0, 0, .125);
            }

            .btn:not(:disabled):not(.disabled) {
                cursor: pointer;
            }

            .btn-link:hover {
                color: #0056b3;
                text-decoration: underline;
                background-color: transparent;
                border-color: transparent;
            }

            .mb-0, .my-0 {
                margin-bottom: 0 !important;
            }

            .card-header .mb-0 .collapsed .f {
                transform: rotate(-90deg);
            }

            .card-header .mb-0 .f {
                transition: .3s transform ease-in-out;
            }
        </style>
        {!! Form::open(['method' => 'POST','action'=>['ArticleController@saveposition',$id],'class'=>'form-horizontal'])!!}


        <div class="panel-body">
            <div class="drag-drop-row" id="portlet-row" style="padding:0px">
                @foreach($articleitems as $articleitem)
                    <div class="card">
                        <div class="card-header panel-heading pointer">

                            <input type="hidden" name="ids[]" value="{{$articleitem->id}}">
                            <div class="mb-0">

                                <div class="col-sm-1"><a class="btn btn-success btn-block"
                                                         href="{{action('ArticleItemController@edit',$articleitem->id)}}"><span
                                                class="fa fa-edit"></span></a></div>
                                <div class="col-sm-1">
                                    <button type="button" class="btn btn-danger btn-block "
                                            onclick="$(this).closest('.portlet').remove()"><span
                                                class="fa fa-trash"></span></button>
                                </div>
                                <a class="btn btn-link collapsed" style=" display: block" data-toggle="collapse"
                                   data-target="{{'#'.$articleitem->id}}" aria-expanded="true"
                                   aria-controls="collapseOne">
                                    {{$articleitem->title}}
                                    <i class="fa faz f fa-chevron-down pull-right"></i>
                                </a>


                            </div>
                        </div>
                        <div class="card-body collapse" id="{{$articleitem->id}}">
                            <div class="card card-body">

                                @if(str_word_count($articleitem->content)>6)
                                    <div style="padding: 10px;overflow: auto">
                                    {!! $articleitem->content !!}
                                    </div>
                                @else
                                    <?php
                                    $articless = \App\Model\ArticleItem::where('parent_id', $articleitem->id)->orderBy('position','asc')->get();

                                    ?>
                                    <div style="margin-top: 10px">
                                        @foreach($articless as $articless)
                                            <?php $articless = $articless->local_content(); ?>

                                            <div class="card" style="margin: 2px 20px 2px 20px">
                                                <div class="card-header panel-heading pointer">

                                                    <input type="hidden" name="ids[]" value="{{$articless->id}}">
                                                    <div class="mb-0">

                                                        <div class="col-sm-1"><a class="btn btn-success btn-block"
                                                                                 href="{{action('ArticleItemController@edit',$articless->id)}}"><span
                                                                        class="fa fa-edit"></span></a></div>
                                                        <div class="col-sm-1">
                                                            <button type="button" class="btn btn-danger btn-block "
                                                                    onclick="$(this).closest('.portlet').remove()"><span
                                                                        class="fa fa-trash"></span></button>
                                                        </div>
                                                        <a class="btn btn-link collapsed" style=" display: block"
                                                           data-toggle="collapse"
                                                           data-target="{{'#'.$articless->id}}" aria-expanded="true"
                                                           aria-controls="collapseOne">
                                                            {{$articless->title}}
                                                            <i class="fa faz f fa-chevron-down pull-right"></i>
                                                        </a>


                                                    </div>
                                                </div>
                                                <div class="card-body collapse" id="{{$articless->id}}">
                                                    <div class="card card-body" >
                                                        <div style="padding: 10px;overflow: auto">
                                                            {!! $articless->content !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="col-sm-4 col-sm-offset-4">
                    <button type="submit" id="save-position" class="form-control btn btn-success">{{_("Save")}}</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('script')
    <script src="{{ asset('assets/app/js/dragdropobject.js')}}"></script>
    <script src="{{asset('assets/jquery/jquery-ui.min.js')}}"></script>

@stop