@extends ('administrator.layout.default')

@section('content')

<div class="panel panel-default">
	
	<?php $counter = 0; ?>
	<div class="panel-heading">
		{{_("Article Management")}}
	</div>
	<div class="panel-body" >
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td>{{_('No')}}.</td>
					<td>{{_("Name")}}</td>
					<td>{{_("Link")}}</td>
					<td>{{_("Status")}}</td>
					<td>{{_("Created At")}}</td>
					<td>{{_("Updated At")}}</td>
					<td colspan="2">{{_("Action")}}</td>
				</thead>
				@foreach($articles as $article)
				<tr>
					<td>{{++$counter}}</td>
					<td>{{$article->name}}</td>
					<td>{{$article->link}}</td>
					<td>{{$article->status}}</td>
					<td>{{$article->created_at}}</td>
					<td>{{$article->updated_at}}</td>
					<td>
						<button title="{{_('Edit Page')}}" class="btn btn-success btn-block glyphicon glyphicon-edit" onClick="window.location ='{{action('ArticleController@edit',$article->link)}}'"></button>
					</td>
					<td>
						{!!Form::model($article,['method'=>'DELETE','action'=>['ArticleController@destroy',$article->link],'id'=>$article->link])!!}{!!Form::close()!!}
						<button title="{{_('Delete Page')}}" type="submit" class="btn btn-danger btn-block glyphicon glyphicon-minus" onclick="deleteConfirmation('{{$article->link}}')"></button>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>

@stop