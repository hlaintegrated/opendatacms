<!-- Text Box for Link -->
<div class="form-group">
	<div class="col-md-1"></div>
	{!! Form::label('lnk',_('Link').'*',['class' => 'control-label col-md-2']) !!}
	<div class="col-md-8">
		{!! Form::text('link',null,['class'=>'form-control', 'id' => 'lnk','required'=>'required'])!!}
	</div>
	<div class="col-md-1"></div>
</div>

<!-- Text Area for Status -->
<div class='form-group'>
    <div class='col-md-1'></div>
    {!! Form::label('status','Status',['class' => 'control-label col-md-2']) !!}
    <div class='col-md-8'>
        {!! Form::select('status',\App\Model\Article::statuses(),null,['class'=>'form-control', 'id' => 'status'])!!}
    </div>
    <div class='col-md-1'></div>
</div>


<ul class="nav nav-tabs" id="language-selector">
    @foreach(Settings::languages() as $code => $name)
    <li class="tab-container" id="{{$code}}-tab"><a class="language-tab" data-language="{{$code}}">{{$name}}</a></li>
    {!! Form::hidden($code.'-name',null,['id'=>"$code".'-name']) !!}
    @endforeach
</ul>

<!-- Text Area for Name -->
<div class='form-group'>
   <div class='col-md-1'></div>
   {!! Form::label('name',_('Name').'*',['class' => 'control-label col-md-2']) !!}
   <div class='col-md-8'>
       {!! Form::text('name',null,['class'=>'form-control', 'id' => 'name','required'=>'required'])!!}
   </div>
   <div class='col-md-1'></div>
</div>
