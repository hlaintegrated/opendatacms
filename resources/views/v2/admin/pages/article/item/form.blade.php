<ul class="nav nav-tabs" id="language-selector">
    @foreach(Settings::languages() as $code => $name)
    <li class="tab-container" id="{{$code}}-tab"><a class="language-tab" data-language="{{$code}}">{{$name}}</a></li>
    {!! Form::hidden($code.'-title',null,['id'=>"$code".'-title']) !!}
    {!! Form::hidden($code.'-content',null,['id'=>"$code".'-content']) !!}
    @endforeach
</ul>

<!-- Text Box for title -->
<div class="form-group">
	<div class="col-md-1"></div>
	{!! Form::label('title',_('Title').'*',['class' => 'control-label col-md-2']) !!}
	<div class="col-md-8">
		{!! Form::text('title',null,['class'=>'form-control', 'id' => 'title','required'=>'required'])!!}
	</div>
	<div class="col-md-1"></div>
</div>
<!-- Text Area for content -->
<div class='form-group'>
   <div class='col-md-1'></div>
   {!! Form::label('content',_('Content').'*',['class' => 'control-label col-md-2']) !!}
   <div class='col-md-8'>
       {!! Form::textarea('content',null,['class'=>'form-control', 'id' => 'content','required'=>'required'])!!}
   </div>
   <div class='col-md-1'></div>
</div>
<div class='form-group'>
    <div class='col-md-1'></div>
    {!! Form::label('parent','Parent',['class' => 'control-label col-md-2']) !!}
    <div class='col-md-8'>
        @if(isset($articleitem))
          {!! Form::select('parent_id',\App\Model\ArticleItem::parent($articleitem->id),null,['class'=>'form-control', 'id' => 'status'])!!}
        @else
          {!! Form::select('parent_id',\App\Model\ArticleItem::parent(),null,['class'=>'form-control', 'id' => 'status'])!!}
        @endif  
    </div>
    <div class='col-md-1'></div>
</div>

@section('additionalscript')
    <script>



    </script>
    <script src="{{asset('js/ckeditor4/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'content', {
            filebrowserUploadUrl: '{{ url('/administrator/send_uploads/?_token='. csrf_token() ) }}'
        } );

        {{--CKEDITOR.on( 'fileUploadRequest', function( evt ) {--}}
            {{--var xhr = evt.data.fileLoader.xhr;--}}

            {{--xhr.setRequestHeader( 'Cache-Control', 'no-cache' );--}}
            {{--xhr.setRequestHeader( 'X-File-Name', this.fileName );--}}
            {{--xhr.setRequestHeader( 'X-File-Size', this.total );--}}
            {{--xhr.setRequestHeader('X-CSRF-TOKEN', '{{ csrf_token() }}' );--}}
            {{--xhr.send( this.file );--}}

            {{--// Prevented the default behavior.--}}
            {{--evt.stop();--}}
        {{--} );--}}
    </script>
@stop


@section('loader')
    @include('partials.loader')
@stop