@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		{{_("Add Article Item")}}
	</div>
	<div class="panel-body">
		{!! Form::open(['method' => 'POST','action'=>['ArticleItemController@store',$article],'class'=>'form-horizontal multi-language-form','data-language-input'=>'title,-content','files'=>'true','id'=>'article-item-multi-language-form'])!!}

			@include('administrator.pages.article.item.form')

		<div class="form-group">
			<div class="col-sm-3"></div>
			<div class="col-sm-8">
				<button type="submit" class="form-control btn btn-primary">{{_("Create")}}</button>
			</div>
			<div class="col-sm-1"></div>
		</div>
		{!! Form::close()!!}
	</div>
</div>
@stop