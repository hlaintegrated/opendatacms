@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default panel-admin">
	<?php $counter = 0; ?>
	<div class="panel-heading">
		{{_('Dataset Publisher Management')}}
	</div>
	<div class="panel-body" >
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td>{{_('No')}}.</td>
					<td>{{_("Name")}}</td>
					<td>{{_("Custom")}}</td>
{{--					<td>{{_("Contact")}}</td>--}}
					<td colspan="2">{{_("Action")}}</td>
				</thead>
				@foreach($sources as $source)
				<tr>
					<td>{{++$counter}}</td>
					<td>{{T::_($source['name'])}}</td>
					<td align="center">
						@if ($source['custom'])
						<i title="{{_('Not a CKAN Default Dataset publisher')}}" class="fa fa-check-circle fa-2x green"></i>
						@else
						<i title="{{_('CKAN Default Dataset publisher')}}" class="fa fa-times-circle fa-2x red"></i>
						@endif
					</td>
{{--					<td>{{_($source['contact'])}}</td>--}}
					<td>
						<button title="{{_('Edit Dataset publisher')}}" class="btn btn-success glyphicon glyphicon-edit btn-block" onClick="window.location='{{action('DataSourceController@edit',$source['id'])}}'"></button>
					</td>
					<td>
						<div>
							{!!Form::open(['method'=>'DELETE','action'=>['DataSourceController@destroy',$source['id']],'id'=>$source['id']])!!}
							{!!Form::close()!!}
							<button type="submit" title="{{_('Delete Dataset publisher')}}" class="btn btn-danger glyphicon glyphicon-minus btn-block glyph-color" onclick="deleteConfirmation('{{$source['id']}}')"></button>
						</div>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
@stop