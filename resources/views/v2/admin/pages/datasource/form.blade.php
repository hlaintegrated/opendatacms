<!-- Text Box for Setting -->
<div class="form-group">
	<div class="col-md-1"></div>
	{!! Form::label('data_source_name',_('Name').'*',['class' => 'control-label col-md-2']) !!}
	<div class="col-md-8">
		{!! Form::text('data_source_name',null,['class'=>'form-control', 'id' => 'data_source_name','required'])!!}
	</div>
	<div class="col-md-1"></div>
</div>

<div class="form-group">
	<div class="col-md-1"></div>
	{!! Form::label('description',_('Description'),['class' => 'control-label col-md-2']) !!}
	<div class="col-md-8">
		{!! Form::textarea('description',null,['class'=>'form-control', 'id' => 'description','required'])!!}
	</div>
	<div class="col-md-1"></div>
</div>

{{--<div class="form-group" >--}}
	{{--<div class="col-md-1"></div>--}}
	{{--{!! form::label('source',_('Source'),['class'=>'control-label col-md-2']) !!}	--}}
	{{--<div class="col-md-8 form-group" style="padding-left: 30px">--}}
		{{--{!! Form::select('id',$sources,null,['class'=>'form-control'])!!}--}}
	{{--</div>--}}
	{{--<div class="col-md-1"></div>--}}
{{--</div>--}}