@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		{{_("Edit Dataset Publisher")}}
	</div>
	<div class="panel-body">
<!-- Create Form for Source -->
{!! Form::model($source,['method'=>'PATCH','action'=>['DataSourceController@update',$source->data_source_id],'class'=>'form-horizontal','files' => 'true'])!!}
@include('administrator.pages.datasource.form')

<div class="form-group">
	<div class="col-sm-4 col-sm-offset-4">
		<button type="submit" class="form-control btn btn-success btn-block">{{_("Save")}}</button>
	</div>
</div>

{!! Form::close() !!}
	</div>
</div>
@stop