@extends ('administrator.layout.default')

@section('content')

<div class="panel panel-default">
	
	<?php $counter = 0; ?>
	<div class="panel-heading">
		{{_("FAQ Management")}}
	</div>
	<div class="panel-body" >
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td>{{_('No')}}.</td>
					<td>{{_("Name")}}</td>
					<td>{{_("Link")}}</td>
					<td>{{_("Status")}}</td>
					<td>{{_("Created At")}}</td>
					<td>{{_("Updated At")}}</td>
					<td colspan="2">{{_("Action")}}</td>
				</thead>
				@foreach($faqs as $faq)
				<tr>
					<td>{{++$counter}}</td>
					<td>{{$faq->name}}</td>
					<td>{{$faq->link}}</td>
					<td>{{$faq->status}}</td>
					<td>{{$faq->created_at}}</td>
					<td>{{$faq->updated_at}}</td>
					<td>
						<button title="{{_('Edit Page')}}" class="btn btn-success btn-block glyphicon glyphicon-edit" onClick="window.location ='{{action('FAQController@edit',$faq->link)}}'"></button>
					</td>
					<td>
						{!!Form::model($faq,['method'=>'DELETE','action'=>['FAQController@destroy',$faq->link],'id'=>$faq->link])!!}{!!Form::close()!!}
						<button title="{{_('Delete Page')}}" type="submit" class="btn btn-danger btn-block glyphicon glyphicon-minus" onclick="deleteConfirmation('{{$faq->link}}')"></button>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>

@stop