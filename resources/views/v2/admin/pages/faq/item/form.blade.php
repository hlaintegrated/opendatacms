<ul class="nav nav-tabs" id="language-selector">
    @foreach(Settings::languages() as $code => $name)
    <li class="tab-container" id="{{$code}}-tab"><a class="language-tab" data-language="{{$code}}">{{$name}}</a></li>
    {!! Form::hidden($code.'-question',null,['id'=>"$code".'-question']) !!}
    {!! Form::hidden($code.'-answer',null,['id'=>"$code".'-answer']) !!}
    @endforeach
</ul>

<!-- Text Box for Question -->
<div class="form-group">
	<div class="col-md-1"></div>
	{!! Form::label('question',_('Question').'*',['class' => 'control-label col-md-2']) !!}
	<div class="col-md-8">
		{!! Form::text('question',null,['class'=>'form-control', 'id' => 'question','required'=>'required'])!!}
	</div>
	<div class="col-md-1"></div>
</div>
<!-- Text Area for Answer -->
<div class='form-group'>
   <div class='col-md-1'></div>
   {!! Form::label('answer',_('Answer').'*',['class' => 'control-label col-md-2']) !!}
   <div class='col-md-8'>
       {!! Form::textarea('answer',null,['class'=>'form-control', 'id' => 'answer','required'=>'required'])!!}
   </div>
   <div class='col-md-1'></div>
</div>


@section('additionalscript')
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'answer' )
    </script>
@stop


@section('loader')
    @include('partials.loader')
@stop