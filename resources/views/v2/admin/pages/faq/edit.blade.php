@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		{{_('Edit FAQ')}} {{$faq->link}}
	</div>
	<div class="panel-body">
		{!! Form::model($faq,['method' => 'PATCH','action'=>['FAQController@update',$faq->link],'class'=>'form-horizontal multi-language-form','data-language-input'=>'name','id'=>'faq-multi-language-form'])!!}

			@include('administrator.pages.faq.form')

		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-success">{{_("Save")}}</button>
			</div>
		</div>
		{!! Form::close()!!}
	</div>
</div>
<div class="panel panel-default">
    <div class="panel-heading ">
        <div class="row">
            <div class="col-sm-11">
            {{_('FAQ Item Management')}}
            </div>
            <div class="col-sm-1">
                <a class="btn btn-primary btn-block" href='{{action("FAQItemController@create",$id)}}'><i class="glyphicon glyphicon-plus"></i></a>
            </div>
        </div>
    </div>
    
    {!! Form::open(['method' => 'POST','action'=>['FAQController@saveposition',$id],'class'=>'form-horizontal'])!!}
    <div class="panel-body">
        <div class="drag-drop-row" id="portlet-row" style="padding:0px">
        @foreach($faqitems as $faqitem)
            <div class="portlet panel panel-default pointer">
                <div class="portlet-header panel-heading pointer">
                    <input type="hidden" name="ids[]" value="{{$faqitem->id}}">
                    <div class="row">
                        <div class="col-sm-10">{{$faqitem->question}}</div>
                        <div class="col-sm-1"><a class="btn btn-success btn-block" href="{{action('FAQItemController@edit',$faqitem->id)}}"><span class="fa fa-edit"></span></a></div>
                        <div class="col-sm-1"><button type="button" class="btn btn-danger btn-block " onclick="$(this).closest('.portlet').remove()"><span class="fa fa-trash"></span></button></div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
    <div class="panel-footer">
		<div class="form-group row">
            <div class="col-sm-4 col-sm-offset-4">
				<button type="submit" id="save-position" class="form-control btn btn-success">{{_("Save")}}</button>
			</div>
		</div>
    </div>
    {!! Form::close() !!}
</div>
@stop

@section('script')
<script src="{{ asset('assets/app/js/dragdropobject.js')}}"></script>
<script src="{{asset('assets/jquery/jquery-ui.min.js')}}"></script>

@stop