@if ($dataset_request->status == 'pending')
	<i class="fa fa-exclamation-circle yellow fa-2x" title="{{_('Pending')}}">
@elseif($dataset_request->status == 'completed')
	<i class="fa fa-check-circle green fa-2x" title="{{_('Completed')}}">
@elseif($dataset_request->status == 'in_progress')
	<i class="fa fa-hourglass-2 fa-2x green" title="{{_('In Progress')}}">
@else
	<i class="fa fa-times-circle fa-2x red" title="{{_('Rejected')}}">	
@endif