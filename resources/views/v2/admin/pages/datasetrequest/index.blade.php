@extends('administrator.layout.default')

@section('content')

<div class="panel panel-default" style="margin-top:20px">
	<?php $counter = 0; ?>
	<div class="panel-heading">
		{{_('Dataset Request Management')}}
	</div>
	<div class="panel-body" >
        <div class="dropdown" style="padding: 10px">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select Filter</button>
            <ul class="dropdown-menu">
                <li><a href="{{action('DatasetRequestController@index')}}?status=all">All</a></li>
                <li><a href="{{action('DatasetRequestController@index')}}?status=pending">Pending</a></li>
                <li><a href="{{action('DatasetRequestController@index')}}?status=in_progress">In Progress</a></li>
                <li><a href="{{action('DatasetRequestController@index')}}?status=completed">Completed</a></li>
                <li><a href="{{action('DatasetRequestController@index')}}?status=rejected">Rejected</a></li>
            </ul>
        </div>
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td>{{_('No')}}.</td>
					<td>{{_("Sender")}}</td>
					<td>{{_("Title")}}</td>
					<td>{{_("Description")}}</td>
					<td>{{_("Type")}}</td>
					<td>{{_("Source")}}</td>
					<td align="center">{{_("Status")}}</td>
					<td align="center">{{_("Detail")}}</td>
				</thead>
				@foreach($dataset_requests as $dataset_request)
					<tr>	
						<td>{{$dataset_requests->total()-$dataset_requests->perPage()*($dataset_requests->currentPage()-1)-$counter}}</td>
						<td>{{$dataset_request['name']}}</td>
						<td>{{strlen($dataset_request['title']) > 15? substr($dataset_request['title'],0,12).'...':$dataset_request['title']}}</td>
						<td>{{strlen($dataset_request['description']) > 30? substr($dataset_request['description'],0,27).'...':$dataset_request['description']}}</td>
<td align="center"><p title="@foreach($dataset_request->types as $type){{$type->data_type_name}}
@endforeach">{{count($dataset_request->types)}}</p></td>
<td align="center"><p title="@foreach($dataset_request->sources as $source){{$source->data_source_name}}
@endforeach">{{count($dataset_request->sources)}}</p></td>
						<td  align="center">
							@include('administrator.pages.datasetrequest.status')
						</td>
						<td  align="center">
							<button class="btn btn-success fa fa-file" onclick="window.location='{{action('DatasetRequestController@edit',$dataset_request->request_id)}}'" title="{{_('Show Detail')}}"></button>
						</td>
					</tr>
					<?php $counter++; ?>
				@endforeach
			</table>
		</div>
	</div>
</div>

<div align="center">
	<ul class="pagination pagination-lg">
		@for ($i = 1; $i <= ($dataset_requests->total()+$dataset_requests->perPage())/$dataset_requests->perPage(); $i++)
		<li><a href="datasetrequest?page={{$i}}">{{$i}}</a></li>
		@endfor
	</ul>
</div>
@stop