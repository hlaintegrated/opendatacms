@extends('administrator.layout.default')
@section('content')
<button onclick="window.location='{{action('DatasetRequestController@edit',$dataset_request->request_id)}}'" class="btn btn-success">Edit</button>
<div class="panel panel-default" style="margin-top:20px">
	<div class="panel-heading">
		{{_('Dataset Request Detail')}}
	</div>
	<div class="panel-body">
		<table class="table-striped table-hover" width="100%">
			<tr style="height: 30px">
				<td style="width: 20%">{{_('Sender')}}</td>
				<td>{{$dataset_request->name}}</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Email')}}</td>
				<td>{{$dataset_request->email}}</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Phone')}}</td>
				<td>{{$dataset_request->phone}}</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Title')}}</td>
				<td>{{$dataset_request->title}}</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Description')}}</td>
				<td>{{$dataset_request->description}}</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Remarks')}}</td>
				<td>{{$dataset_request->notes}}</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Sources')}}</td>
				<td>
					<ul>
						@foreach($dataset_request->sources as $source)
						<li>{{T::_($source->data_source_name)}}</li>
						@endforeach
					</ul>
				</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%">{{_('Types')}}</td>
				<td>
					<ul>
						@foreach($dataset_request->types as $type)
						<li>{{$type->data_type_name}}</li>
						@endforeach
					</ul>
				</td>
			</tr>



			<tr style="height: 30px">
				<td style="width: 20%">{{_('Status')}}</td>
				<td>
					@include('administrator.pages.datasetrequest.status')
				</td>
			</tr>
		</table>
	</div>
</div>
@stop