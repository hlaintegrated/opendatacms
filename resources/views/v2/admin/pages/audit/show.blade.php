    <div class="panel panel-default panel-admin">
        <div class="panel-body" >

                Changes Made By : <strong>{{ $user->name }}</strong>
            <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th width="50%" class="text-center">Before</th>
                    <th width="50%" class="text-center">After</th>
                </tr>
                <tr>
                    <td>
                        @if(is_array($before))
                        <table class="table table-striped">
                            @foreach($before as $field => $value)
                                <tr>
                                    <td>{{ $field }}</td>
                                    <td>{{ $value }}</td>
                                </tr>
                            @endforeach
                        </table>
                        @endif
                    </td>
                    <td>
                        @if(is_array($after))
                        <table class="table table-striped">
                            @foreach($after as $field => $value)
                                <tr>
                                    <td>{{ $field }}</td>
                                    <td>{{ $value }}</td>
                                </tr>
                            @endforeach
                        </table>
                        @endif
                    </td>
                </tr>
            </table>
            </div>
        </div>
    </div>