@extends('administrator.layout.default')

@section('content')

    <div class="panel panel-default panel-admin">
        <div class="panel-heading">
            {{_("Audit Trail")}}
        </div>
        <div class="panel-body" >
            <div style="overflow:auto">
                {{--<div class="col-sm-5">--}}
                    {{--<div class="form-group mx-sm-3 mb-2">--}}
                        {{--<input type="text" class="form-control" value="{{ $search }}" id="search">--}}
                    {{--</div>--}}
                    {{--<button class="btn btn-primary mb-2" onclick="searchme()" style="float:right;">Search</button>--}}
                {{--</div>--}}
                <div class="row" style="padding-bottom:10px;">
                    <div class="col-sm-5">
                        <div class="form-inline">
                            <div class="form-group mb-3">
                                <input type="text" class="form-control" value="{{ $search }}" id="search">
                            </div>
                            <button class="btn btn-primary mb-2" onclick="searchme()" >Search</button>
                        </div>
                    </div>
                </div>

                <div>

                    <table class="table table-bordered" align="center" style="white-space: nowrap">
                        <thead>
                        <td>No.</td>
                        <td>{{ _("User")}}</td>
                        <td>{{ _("Ref")  }}</td>
                        <td>{{ _("Time")}}</td>
                        <td>{{ _("Action")}}</td>
                        <td></td>
                        </thead>
                        @foreach($audits as $audit)
                            <tr>
                                <td>{{$audit->id}}</td>
                                <td>{{$audit->user->name}}</td>
                                <td>{{ substr($audit->ref, 0, 30) }}... <span title="{{$audit->ref}}" style="point"  data-toggle="tooltip" ><i class="fa fa-eye"></i></span></td>
                                <td>{{$audit->at}}</td>
                                <td>{{ substr($audit->message,0,30) }}... <span title="{{$audit->message}}" style="point"  data-toggle="tooltip" ><i class="fa fa-eye"></i></span></td>
                                <td><button class="btn btn-primary" onclick="view({{ $audit->id }})">View</button></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div align="center">
        <ul class="pagination pagination-lg">
            {!! $audits->render() !!}
            {{--@for ($i = 1; $i <= ($audits->total()+$audits->perPage())/$audits->perPage(); $i++)--}}
                {{--<li><a href="audit?page={{$i}}">{{$i}}</a></li>--}}
            {{--@endfor--}}
        </ul>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="view_diff_modal" tabindex="-1" role="dialog" aria-labelledby="view_diff_modal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">View Diff</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="diff_item">
                    loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function view(id) {
            $('#view_diff_modal').modal('toggle');
            $( "#diff_item" ).load( "{{ url('administrator/audit') }}/" + id + "/view");
        }

        function searchme() {
            document.location.href= "{{ url('administrator/audit') }}/" + $('#search').val();
        }
    </script>
@stop