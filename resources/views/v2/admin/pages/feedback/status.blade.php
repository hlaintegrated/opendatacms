@if ($feedback->status == 'unread')
	<i class="fa fa-exclamation-circle yellow fa-2x" title="{{_('Unread')}}">
@else
	<a href="{{action('FeedbackController@reportShow',$feedback->status)}}"><span class="fa fa-bookmark green fa-2x" title="{{_('Read')}}"></span></a>				
@endif