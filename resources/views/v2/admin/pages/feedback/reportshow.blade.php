@extends('administrator.layout.default')

@section('content')

	@foreach($feedbackgroups as $type => $feedbacks)
	<div class="panel panel-default" style="margin-top:20px">
		<div class="panel-heading">
			{{_($type)}}
		</div>
		<?php $counter = 0; ?>
		<div class="panel-body" >
			<div style="overflow:auto">
				<table class="table table-bordered" align="center" style="white-space: nowrap">
					<thead>
						<td>{{_('No')}}.</td>
						<td>{{_("Sender")}}</td>
						<td>{{_("Title")}}</td>
						<td>{{_("Description")}}</td>
						<td align="center">{{_("Detail")}}</td>
					</thead>
					@foreach($feedbacks as $feedback)
						<tr>	
							<td>{{++$counter}}</td>
							<td>{{$feedback['name']}}</td>
							<td>{{strlen($feedback['title']) > 15? substr($feedback['title'],0,12).'...':$feedback['title']}}</td>
							<td>{{strlen($feedback['description']) > 30? substr($feedback['description'],0,27).'...':$feedback['description']}}
							</td>
							
							<td  align="center">
								<button class="btn btn-success fa fa-file" onclick="window.location='{{action('FeedbackController@show',$feedback->feedback_id)}}'" title="{{_('Show Detail')}}"></button>
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
	@endforeach
@stop