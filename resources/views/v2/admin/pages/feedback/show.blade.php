@extends('administrator.layout.default')
@section('content')
<div class="panel panel-default" style="margin-top:20px">
	<div class="panel-heading">
		{{_('Feedback Detail')}}
	</div>
	<div class="panel-body">
		<table class="table-striped table-hover" width="100%">
		<tr style="height: 30px">
			<td style="width: 20%">{{_('Sender')}}</td>
			<td>{{$feedback->name}}</td>
		</tr>
		
		<tr style="height: 30px">
			<td style="width: 20%">{{_('Email')}}</td>
			<td>{{$feedback->email}}</td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%">{{_('Phone')}}</td>
			<td>{{$feedback->phone}}</td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%">{{_('Type')}}</td>
			<td>{{$feedback->type}}</td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%">{{_('Title')}}</td>
			<td>{{$feedback->title}}</td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%">{{_('Description')}}</td>
			<td>{{$feedback->description}}</td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%">{{_('Remarks')}}</td>
			<td>{{$feedback->notes}}</td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%">{{_('Status')}}</td>
			<td>
                @if ($feedback->status == 'unread')
	               <span class="fa fa-exclamation-circle yellow" title="{{_('Unread')}}"></span>{{_('Unread')}} <a class="btn btn-success" href="{{action('FeedbackController@read',$feedback->feedback_id)}}">{{_('Mark Read')}}</a>
                @else
                    <span class="fa fa-bookmark green"></span> {{_('Read')}}
                @endif
			</td>
		</tr>
		</table>
	</div>
</div>
@stop