@extends('administrator.layout.default')

@section('content')

<div class="panel panel-default panel-admin">
	<?php $counter = 0; ?>
	<div class="panel-heading">
		{{_('Feedback Management')}}
	</div>
	<div class="panel-body" >
        <div class="dropdown" style="padding: 10px">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">{{_('Select Filter')}}</button>
            <ul class="dropdown-menu">
                <li><a href="{{action('FeedbackController@index')}}?status=all">{{_('All')}}</a></li>
                <li><a href="{{action('FeedbackController@index')}}?status=unread">{{_('Unread')}}</a></li>
                <li><a href="{{action('FeedbackController@index')}}?status=read">{{_('Read')}}</a></li>
            </ul>
        </div>
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td>{{_('No')}}.</td>
					<td>{{_("Sender's Name")}}</td>
					<td>{{_("Email Address")}}</td>
					<td>{{_("Feedback Type")}}</td>
					<td>{{_("Title")}}</td>
					<td>{{_("Description")}}</td>
					<td>{{_("Reference")}}</td>
					<td align="center">{{_("Feedback Status")}}</td>
					<td>{{_("Created Date")}}</td>
					<td align="center">{{_("View Detail")}}</td>
				</thead>
				@foreach($feedbacks as $feedback)
					<tr>	
						<td>{{$feedbacks->total()-$feedbacks->perPage()*($feedbacks->currentPage()-1)-$counter}}</td>
						<td>{{$feedback['name']}}</td>
						<td>{{$feedback['email']}}</td>
						<td>{{$feedback['type']}}</td>
						<td>{{strlen($feedback['title']) > 15? substr($feedback['title'],0,12).'...':$feedback['title']}}</td>
						<td>{{strlen($feedback['description']) > 30? substr($feedback['description'],0,27).'...':$feedback['description']}}</td>
						<td>{{$feedback['reference']}}</td>
						<td  align="center">
						@include('administrator.pages.feedback.status')
						</td>
                        <td>
                            {{$feedback['created_at']}}
                        </td>
						<td  align="center">
							<button class="btn btn-success fa fa-file" onclick="window.location='{{action('FeedbackController@show',$feedback->feedback_id)}}'" title="{{_('Show Detail')}}"></button>
						</td>
					</tr>
					<?php $counter++?>
				@endforeach
			</table>
		</div>
	</div>
</div>

<div align="center">
	<ul class="pagination pagination-lg">
		@for ($i = 1; $i <= ($feedbacks->total()+$feedbacks->perPage())/$feedbacks->perPage(); $i++)
		<li><a href="feedback?page={{$i}}">{{$i}}</a></li>
		@endfor
	</ul>
</div>
@stop