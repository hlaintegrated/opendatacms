@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default" style="margin-top:20px">
	<div class="panel-heading">
		{{_('Generated Reports')}}
	</div>
	<div class="panel-body" >
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td>{{_('No.')}}.</td>
					<td>{{_("Created At")}}</td>
					@foreach($types as $type)
						<td>{{_($type)}}</td>
					@endforeach
					<td align="center">{{_("Detail")}}</td>
				</thead>
				@foreach($reports as $number=>$report)
					<tr>	
						<td>{{$number}}</td>
						@foreach($report as $field)
							<td>{{$field}}</td>
						@endforeach
						<td  align="center">
							<button class="btn btn-success fa fa-file" onclick="window.location='{{action('FeedbackController@reportShow',$number)}}'" title="{{_('Show Detail')}}"></button>
						</td>
					</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>

<div align="center">
	<ul class="pagination pagination-lg">
		@for ($i = 1; $i <= $pages; $i++)
		<li><a href="reports?page={{$i}}">{{$i}}</a></li>
		@endfor
	</ul>
</div>
@stop