@extends('administrator.layout.default')

@section('content')

{!! Form::model($image,['method'=>'PATCH','action'=>['ImageController@update',$image->image_id],'class'=>'form-horizontal','files' => 'true'])!!}
@include('administrator.pages.image.form')

<div class="form-group">
	<div class="col-sm-4 col-sm-offset-4">
		<button type="submit" class="form-control btn btn-success btn-block">{{_("Save")}}</button>
	</div>
</div>

{!! Form::close() !!}

@stop