@extends('administrator.layout.default')

@section('content')

<!-- Create Form for Image -->
{!! Form::open(['action'=>['ImageController@store'],'class'=>'form-horizontal','files' => 'true'])!!}
@include('administrator.pages.image.form')

<div class="form-group">
	<div class="col-sm-4 col-sm-offset-4">
		<button type="submit" class="form-control btn btn-primary btn-block">{{_("Add")}}</button>
	</div>
</div>

{!! Form::close() !!}
@stop