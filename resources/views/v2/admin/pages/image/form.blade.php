		<!-- Text Box for Name -->
		<div class="form-group">
			<div class="col-md-1"></div>
			{!! Form::label('name',_('Name').'*',['class' => 'control-label col-md-2']) !!}
			<div class="col-md-8">
				{!! Form::text('name',null,['class'=>'form-control', 'id' => 'name','required'=>'required'])!!}
			</div>
			<div class="col-md-1"></div>
		</div>

		<!-- Text Area for Description -->
		<div class="form-group">
			<div class="col-md-1"></div>
			{!! Form::label('description',_('Description'),['class' => 'control-label col-md-2']) !!}
			<div class="col-md-8">
				{!! Form::textarea('description',null,['class'=>'form-control', 'id' => 'description'])!!}
			</div>
			<div class="col-md-1"></div>
		</div>
		
		<!-- Date for Public -->
		<div class="form-group">
			<div class="col-md-1"></div>
			{!! Form::label('public',_('Access'),['class' => 'control-label col-md-2']) !!}
			<div class="col-md-8">
				{!! Form::select('public',['false' =>'Private','true'=>'Public'],null,['class'=>'form-control'])!!}
			</div>
			<div class="col-md-1"></div>
		</div>

		<!-- Date for Carousel -->
		<div class="form-group">
			<div class="col-md-1"></div>
			{!! Form::label('carousel',_('For Carousel'),['class' => 'control-label col-md-2']) !!}
			<div class="col-md-8">
				{!! Form::select('carousel',['false' =>'False','true'=>'True'],null,['class'=>'form-control'])!!}
			</div>
			<div class="col-md-1"></div>
		</div>

		<!-- File for image -->
		<div class="form-group">
			<div class="col-md-1"></div>
			{!! Form::label('image',_('Image').'*',['class' => 'control-label col-md-2']) !!}
			<div class="col-md-8">
			{!! Form::file('image',['class'=>'form-control-file', 'id' => 'image','accept' =>'.png,.jpg,.bmp'])!!}
			</div>
			<div class="col-md-1"></div>
		</div>

		