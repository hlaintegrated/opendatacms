
		<!-- Date for Publish Date -->
		<div class="form-group">
			<div class="col-md-1"></div>
			{!! Form::label('publish_date',_('Publish Date'),['class' => 'control-label col-md-2']) !!}
			<div class="col-md-8">
				{!! Form::input('date','publish_date',null,['class'=>'form-control', 'id' => 'publish_date'])!!}
			</div>
			<div class="col-md-1"></div>
		</div>

<ul class="nav nav-tabs" id="language-selector">
    @foreach(Settings::languages() as $code => $name)
    <li class="tab-container" id="{{$code}}-tab"><a class="language-tab" data-language="{{$code}}">{{$name}}</a></li>
    {!! Form::hidden($code.'-title',null,['id'=>"$code".'-title']) !!}
    {!! Form::hidden($code.'-content',null,['id'=>"$code".'-content']) !!}
    @endforeach
</ul>
		<!-- Text Box for Title -->
		<div class="form-group">
			<div class="col-md-1"></div>
			{!! Form::label('title',_('Title').'*',['class' => 'control-label col-md-2']) !!}
			<div class="col-md-8">
				{!! Form::text('title',null,['class'=>'form-control', 'id' => 'title'])!!}
			</div>
			<div class="col-md-1"></div>
		</div>

		<!-- Text Area for Description -->
		<div class="form-group">
			<div class="col-md-1"></div>
			{!! Form::label('content',_('Description').'*',['class' => 'control-label col-md-2']) !!}
			<div class="col-md-8">
				{!! Form::textarea('content',null,['class'=>'form-control', 'id' => 'content'])!!}

			</div>
			<div class="col-md-1"></div>
		</div>


		@section('loader')
			@include('partials.loader')
		@stop

@section('additionalscript')
	<script src="{{asset('js/ckeditor4/ckeditor.js')}}"></script>
	<script>
        CKEDITOR.replace( 'content', {
            filebrowserUploadUrl: '{{ url('/administrator/send_uploads/?_token='. csrf_token() ) }}'
        } );
	</script>
@stop