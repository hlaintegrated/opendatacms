@extends('administrator.layout.default')

@section('content')

<div class="panel panel-default panel-admin">
	<?php $counter = 0; ?>
	<div class="panel-heading">
	{{_('News')}} / {{_('Event')}} / {{_('Activity')}}
	</div>
	<div class="panel-body" >
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td>No.</td>
					<td>{{_("Title")}}</td>
					<td>{{_("Publish Date")}}</td>
					<td colspan="2">{{_("Action")}}</td>
				</thead>
				@foreach($events as $event)
				<tr>
					<td>{{++$counter}}</td>
					<td>{{$event->title}}</td>
					<td>{{$event->publish_date}}</td>
					<td>
						<button title="{{_('Edit Event')}}" class="btn btn-success glyphicon glyphicon-edit btn-block" onClick="window.location ='{{action('EventController@edit',$event->id)}}'"></button>
					</td>
					<td>
						{!!Form::model($event,['method'=>'DELETE','action'=>['EventController@destroy',$event->id],'id'=>$event->id])!!}{!!Form::close()!!}
						<button title="{{_('Delete Event')}}" class="btn btn-danger glyphicon glyphicon-minus btn-block" onclick="deleteConfirmation('{{$event->id}}')"></button>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
<div align="center">
	{!!$events->render()!!}
</div>
@stop