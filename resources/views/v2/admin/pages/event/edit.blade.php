@extends('administrator.layout.default')

@section('content')

<!-- Create Form for Event -->
{!! Form::model($event,['method'=>'PATCH','action'=>['EventController@update',$event->id],'class'=>'form-horizontal multi-language-form','data-language-input'=>'title,-content','files' => 'true'])!!}
@include('administrator.pages.event.form')

<div class="form-group">
	<div class="col-sm-4 col-sm-offset-4">
		<button type="submit" class="form-control btn btn-success btn-block">{{_("Save")}}</button>
	</div>
</div>

{!! Form::close() !!}

@stop