<!-- Text Box for Link -->
<div class="form-group">
	<div class="col-md-1"></div>
	{!! Form::label('lnk',_('Link').'*',['class' => 'control-label col-md-2']) !!}
	<div class="col-md-8">
		{!! Form::text('link',null,['class'=>'form-control', 'id' => 'lnk','required'=>'required'])!!}
	</div>
	<div class="col-md-1"></div>
</div>

<!-- Text Area for Background -->
<div class='form-group' id="background-container">
    <div class='col-md-1'></div>
        {!! Form::label('background',_('Background'),['class' => 'control-label col-md-2']) !!}
        <div class='col-md-4'>
        {!! Form::file('background',null,['class'=>'form-control', 'id' => 'background','accept'=>'.png,.bpm,.jpg'])!!}
    </div>
    <div class="col-md-4">
        {{_('Remove Background')}} <input type="checkbox" name="clear">
    </div>
    <div class='col-md-1'></div>
</div>

<ul class="nav nav-tabs" id="language-selector">
    @foreach(Settings::languages() as $code => $name)
    <li class="tab-container" id="{{$code}}-tab"><a class="language-tab" data-language="{{$code}}">{{$name}}</a></li>
    {!! Form::hidden($code.'-title',null,['id'=>"$code".'-title']) !!}
    {!! Form::hidden($code.'-content',null,['id'=>"$code".'-content']) !!}
    @endforeach
</ul>
<!-- Text Box for Title -->
<div class="form-group" id="target">
	<div class="col-md-1"></div>
	{!! Form::label('title',_('Title').'*',['class' => 'control-label col-md-2']) !!}
	<div class="col-md-8">
		{!! Form::text('title',null,['class'=>'form-control', 'id' => 'title','required'=>'required'])!!}
	</div>
	<div class="col-md-1"></div>
</div>

<!-- Text Area for Content -->
<div>
    <div class="form-group">
        <div class="col-md-1"></div>
        {!! Form::label('content',_('Content'),['class' => 'control-label col-md-2']) !!}
        <div class="col-md-8">
            {!! Form::textarea('content',null,['class'=>'form-control', 'id' => 'content','rows'=>'10','cols'=>'80'])!!}

        </div>
        <div class="col-md-1"></div>
    </div>
</div>

@section('additionalscript')
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'content' )
    </script>
@stop

@section('loader')
    @include('partials.loader')
@stop