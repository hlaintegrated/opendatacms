@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		{{_('Edit Page')}} {{$page->link}}
	</div>
	<div class="panel-body">
		{!! Form::model($page,['method' => 'PATCH','action'=>['PageController@update',$page->link],'class'=>'form-horizontal multi-language-form','data-language-input'=>'title,-content','files'=>'true'])!!}

			@include('administrator.pages.page.form')

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-success">{{_("Save")}}</button>
			</div>
		</div>
		{!! Form::close()!!}
	</div>
</div>
@stop