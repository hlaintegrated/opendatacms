@extends ('administrator.layout.default')

@section('content')

<div class="panel panel-default">
	
	<?php $counter = 0; ?>
	<div class="panel-heading">
		{{_("Page Management")}}
	</div>
	<div class="panel-body" >
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td>{{_('No')}}.</td>
					<td>{{_("Link")}}</td>
					<td>{{_("Title")}}</td>
					<td>{{_("Created At")}}</td>
					<td>{{_("Updated At")}}</td>
					<td colspan="2">{{_("Action")}}</td>
				</thead>
				@foreach($pages as $page)
				<tr>
					<td>{{++$counter}}</td>
					<td>{{$page->link}}</td>
					<td>{{$page->title}}</td>
					<td>{{$page->created_at}}</td>
					<td>{{$page->updated_at}}</td>
					<td>
						<button title="{{_('Edit Page')}}" class="btn btn-success btn-block glyphicon glyphicon-edit" onClick="window.location ='{{action('PageController@edit',$page->link)}}'"></button>
					</td>
					<td>
						{!!Form::model($page,['method'=>'DELETE','action'=>['PageController@destroy',$page->link],'id'=>$page->link])!!}{!!Form::close()!!}
						<button title="{{_('Delete Page')}}" type="submit" class="btn btn-danger btn-block glyphicon glyphicon-minus" onclick="deleteConfirmation('{{$page->link}}')"></button>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>

@stop