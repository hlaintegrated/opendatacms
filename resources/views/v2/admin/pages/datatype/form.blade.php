<div class="form-group">
	<div class="col-md-1"></div>
	{!! Form::label('data_type_name',_('Name').'*',['class' => 'control-label col-md-2']) !!}
	<div class="col-md-8">
		{!! Form::text('data_type_name',null,['class'=>'form-control', 'id' => 'data_type_name','required'=>'required'])!!}
	</div>
	<div class="col-md-1"></div>
</div>

<div class="form-group">
	<div class="col-md-1"></div>
	{!! Form::label('data_type_detail',_('Detail'),['class' => 'control-label col-md-2']) !!}
	<div class="col-md-8">
		{!! Form::textarea('data_type_detail',null,['class'=>'form-control', 'id' => 'data_type_detail'])!!}
	</div>
	<div class="col-md-1"></div>
</div>


<div class="form-group">
	<div class="col-sm-1"></div>
	<label for ='available' class ='control-label col-sm-2'>{{_("Availability")}}</label>
	<div class="col-sm-8">
		{!! Form::select('available',['true'=>'True','false'=>'False'],'true',['class'=>'form-control'])!!}
	</div>
	<div class="col-sm-1"></div>
</div>
		