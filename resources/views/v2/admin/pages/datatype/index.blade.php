@extends('administrator.layout.default')

@section('content')

<div class="panel panel-default panel-admin" >
	<?php $counter = 0; ?>
	<div class="panel-heading">
		{{_('Data Type Management')}}
	</div>
	<div class="panel-body" >
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td>No.</td>
					<td>{{_("Name")}}</td>
					<td>{{_("Detail")}}</td>
					<td>{{_("Availability")}}</td>
					<td colspan="3">{{_("Action")}}</td>
				</thead>
				@foreach($types as $type)
				<tr>
					<td>{{++$counter}}</td>
					<td>{{$type->data_type_name}}</td>
					<td><a href="{{action('DataTypeController@show',$type->data_type_id)}}">{{strlen($type->data_type_detail) > 30 ? substr($type->data_type_detail,0,27).'...':$type->data_type_detail}}</a></td>
					<td align="center">
						@include('administrator.pages.datatype.status')
					</td>
					<td>
						<button class="btn btn-success btn-block glyphicon glyphicon-edit" title="{{_('Edit Data Type')}}" onClick="window.location ='{{action('DataTypeController@edit',$type->data_type_id)}}'"></button>
					</td>
					<td>
						@if ($type->available)
							<button class="btn btn-danger btn-block glyphicon glyphicon-minus" title="{{_('Deactive Data Type')}}" onClick="window.location = '{{action('DataTypeController@deactivate',$type->data_type_id)}}'"></button>
						@else

							<button class="btn btn-primary btn-block glyphicon glyphicon-plus" title="{{_('Activate Data Type')}}" onClick="window.location = '{{action('DataTypeController@activate',$type->data_type_id)}}'"></button>
						@endif
					</td>
					<td>
						{!!Form::model($type,['method'=>'DELETE','action'=>['DataTypeController@destroy',$type->data_type_id],'id'=>$type->data_type_id])!!}{!!Form::close()!!}
						<button title="{{_('Delete Data Type')}}" type="submit" class="btn btn-danger btn-block glyphicon glyphicon-trash" onclick="deleteConfirmation('{{$type->data_type_id}}')"></button>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
@stop