@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		{{_('Create Data Type')}}
	</div>
	<div class="panel-body">
		{!! Form::open(['method' => 'POST','action'=>['DataTypeController@store'],'class'=>'form-horizontal'])!!}

			@include('administrator.pages.datatype.form')

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-primary">{{_('Create')}}</button>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@stop