@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		{{_('Edit Data Type')}} {{$type->data_type_name}}
	</div>
	<div class="panel-body">
		{!! Form::model($type,['method' => 'PATCH','action'=>['DataTypeController@update',$type->data_type_id],'class'=>'form-horizontal'])!!}

			@include('administrator.pages.datatype.form')

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-success">{{_('Save')}}</button>
			</div>
		</div>
		{!! Form::close() !!}

	</div>
</div>
@stop