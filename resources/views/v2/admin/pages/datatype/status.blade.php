@if($type->available)
	<i class="fa fa-check-circle fa-2x green" title="{{_('Available')}}">
@else
	<i class="fa fa-times-circle fa-2x red" title="{{_('Not Available')}}" >
@endif