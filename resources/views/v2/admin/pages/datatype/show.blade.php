@extends('administrator.layout.default')
@section('content')
<div class="panel panel-default" style="margin-top:20px">
	<div class="panel-heading">
		{{_('Data Type Detail')}}
	</div>
	<div class="panel-body">
		<table class="table-striped table-hover" width="100%">
		<tr style="height: 30px">
			<td style="width: 20%">{{_('Name')}}</td>
			<td>{{$type->data_type_name}}</td>
		</tr>
		
		<tr style="height: 30px">
			<td style="width: 20%">{{_('Detail')}}</td>
			<td>{{$type->data_type_detail}}</td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%">{{_('Availability')}}</td>
			<td>@include('administrator.pages.datatype.status')</td>
		</tr>
		</table>
	</div>
</div>
@stop