@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	
	<div class="panel-heading">
		{{_("Setting Management")}}
	</div>
	<div class="panel-body" >
            {!! Form::open(['method'=>'POST',"action"=>['SystemSettingController@saveOther',$field]])!!}
		<div>
			<table class="table table-bordered" align="center" style="white-space: nowrap" id="language">
				<thead>
					<td>{{_("Key")}}</td>
					<td>{{_("Value")}}</td>
				</thead>
                <tr class="setting-row">
                    <td><label class="form-label">{{$field}}</label></td><td>
				@if ($setting->name == 'Default Color Theme')
						<select name="value" class="form-control">
							@foreach(Settings::theme_colors() as $color)
							<option value="{{$color}}" <?= $color == $setting->value?"selected":"" ?>>{{$color}}</option>
							@endforeach
						</select>
				@else
                    	<textarea rows="1" class="form-control" name="value" style="resize: vertical">{{$setting->value}}</textarea>
                @endif
                	</td>
                </tr>
			</table>
            <div class="row">
                <div class="col-md-offset-4 col-md-4">
                    <button class="btn btn-success btn-block" type='submit' id="save-localized">{{_("Save")}}</button>
                </div>
            </div>
		</div>
            {!! Form::close() !!}   
	</div>
</div>
@stop