@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	
	<?php $counter = 0; ?>
	<div class="panel-heading">
		{{_("Language Management")}}
	</div>
	<div class="panel-body" >
            {!! Form::open(['method'=>'POST',"action"=>['SystemSettingController@saveLanguage',"Languages"]])!!}
		<div>
			<table class="table table-bordered" align="center" style="white-space: nowrap" id="language">
				<thead>
					<td>No.</td>
					<td>{{_("Code")}}</td>
					<td>{{_("Name")}}</td>
                    <td>{{_('Remove')}}</td>
				</thead>
				@foreach($languages as $key => $value)
                <tr class="setting-row">
                    <td>{{++$counter}}</td>
                    <td><input type="text" class="form-control" name="codes[]" placeholder="Code, ex: en_US, ms_MY" value="{{$key}}"></td>
                    <td><input type="text" class="form-control" name="values[]" placeholder="Value, ex: English, Bahasa Malaysia" value="{{$value}}"></td>
                    <td><button type="button" class="btn btn-danger btn-block fa fa-trash" onclick="$(this).closest('tr').remove()"></button>
                </tr>
				@endforeach
			</table>
            <div class="row">
                <div class="col-md-3 col-md-offset-2">
                    <button type="button" class="btn btn-primary btn-block" onclick="addLanguageRow()">{{_('Add')}}</button>
                </div>
                <div class="col-md-offset-2 col-md-3">
                    <button class="btn btn-success btn-block" type='submit' id="save-language">{{_("Save")}}</button>
                </div>
            </div>
		</div>
            {!! Form::close() !!}   
	</div>
</div>
@stop