@extends('v2.admin.template.adminlayout')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content">
	<!-- START JUMBOTRON -->
	<div class="jumbotron" data-pages="parallax">
		<div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
			<div class="inner">
				<!-- START BREADCRUMB -->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Tetapan CMS</a></li>
					<li class="breadcrumb-item active">Tetapan Sistem</li>
				</ol>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- END JUMBOTRON -->
	<!-- START CONTAINER FLUID -->
	<div class="container-fluid container-fixed-lg">
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		<div class="panel panel-default">

			<?php $counter = 0; ?>
			<div class="panel-heading">
				{{_("System Management")}}
			</div>
			<div class="panel-body">
				<div style="overflow:auto">
					<?php
					$index = 0; ?>
					@foreach($grouping as $title=>$group)
					<div class="panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" href="#section{{$index}}">{{$title}}</a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="section{{$index}}">
								<ul class="list-group">
									@foreach($group as $element)
									@foreach($settings as $setting)
									@if ($setting->name == $element)
									<li class="list-group-item">
										<div class="row">
											<div class="col-xs-4">{{$element}}</div>
											<div class="col-xs-8">
												@if ($setting->type== 'localized')
												<a class="btn btn-success" href="{{action('SystemSettingController@localized',$setting->name)}}">{{_('Change Values')}} </a>
												{!! Settings::getlocalized($setting->name,null,true) !!}
												@else
												<a class="btn btn-success" href="{{action('SystemSettingController@other',$setting->name)}}">{{_('Change Values')}} </a>
												{!! $setting->value !!}
												@endif
											</div>
										</div>
									</li>
									@endif
									@endforeach
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					<?php $index++; ?>
					@endforeach
				</div>
			</div>
		</div>
		<!-- END PLACE PAGE CONTENT HERE -->
	</div>
	<!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
@endsection