@extends('administrator.layout.default')

@section('content')
<div class="panel panel-default">
	
	<div class="panel-heading">
		{{_("Language Management")}}
	</div>
	<div class="panel-body" >
            {!! Form::open(['method'=>'POST',"action"=>['SystemSettingController@saveLanguage',$field]])!!}
		<div>
			<table class="table table-bordered" align="center" style="white-space: nowrap" id="language">
				<thead>
					<td>{{_("Language")}}</td>
					<td>{{_("Message")}}</td>
				</thead>
				@foreach(Settings::languages() as $key => $value)
                <tr class="setting-row">
					<input type="hidden" name="codes[]" value="{{$key}}">
                    <td><label class="form-label">{{$value}}</label></td>
                    <td><textarea rows="1" class="form-control" name="values[]" placeholder="Value, ex: English, Bahasa Malaysia" style="resize: vertical">{{array_key_exists($key,$settings)?$settings[$key]:''}}</textarea></td>
                </tr>
				@endforeach
			</table>
            <div class="row">
                <div class="col-md-offset-4 col-md-4">
                    <button class="btn btn-success btn-block" type='submit' id="save-localized">{{_("Save")}}</button>
                </div>
            </div>
		</div>
            {!! Form::close() !!}   
	</div>
</div>
@stop