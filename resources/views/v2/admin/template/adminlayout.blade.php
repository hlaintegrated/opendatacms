<!DOCTYPE html>
<html lang="en">

<head>

@include('v2.admin.template.partials.head')
@yield('additionalcssscript')
</head>

<body class="fixed-header menu-pin">

    <!-- Sidebar -->
    @include('v2.admin.template.partials.sidebar')
    <!-- End of Sidebar -->

    <!-- START PAGE-CONTAINER -->
    <div class="page-container">
    
      <!-- Topbar -->
      @include('v2.admin.template.partials.topbar')
      <!-- End of Topbar -->

      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper">
        <!-- Begin Page Content -->
        @yield('content')
        <!-- /.container-fluid -->

        <!-- Footer -->
        @include('v2.admin.template.partials.footer')
        <!-- End of Footer -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->

    </div>
    <!-- END PAGE CONTAINER -->

    @include('v2.admin.template.partials.footer-scripts')
    @yield('additionaljsscript')
</body>

</html>
