<!-- BEGIN VENDOR JS -->
<script src="/v2/cms/assets/plugins/feather-icons/feather.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS -->
<script src="/v2/cms/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="/v2/cms/assets/plugins/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="/v2/cms/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="/v2/cms/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="/v2/cms/assets/plugins/popper/umd/popper.min.js" type="text/javascript"></script>
<script src="/v2/cms/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/v2/cms/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="/v2/cms/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="/v2/cms/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="/v2/cms/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="/v2/cms/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="/v2/cms/pages/js/pages.min.js" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="/v2/cms/assets/js/scripts_alt.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->

<style>
/* body.menu-pin .page-sidebar {
    width: 300px;
}

body.menu-pin .page-container .page-content-wrapper .content {
    padding-left: 300px;
}

body.menu-pin .page-container .page-content-wrapper .footer {
    left: 300px;
} */

.page-sidebar .sidebar-menu .menu-items > li > a > .title {
    width: 86% !important;
}

.page-sidebar .sidebar-menu .menu-items > li > a > .arrow {
    padding-right: 0px !important;
}

.icon-thumbnail i {
    font-size: 17px;
}
</style>