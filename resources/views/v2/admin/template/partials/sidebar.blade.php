<!-- BEGIN SIDEBAR -->
<!-- BEGIN SIDEBPANEL-->
<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
    <div class="sidebar-overlay-slide from-top" id="appMenu">

    </div>
    <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
        <img src="{{url('/images/dtsa.png')}}" alt="logo" class="brand" data-src="{{url('/images/dtsa.png')}}" data-src-retina="{{url('/images/dtsa.png')}}" height="26">
    </div>
    <!-- END SIDEBAR MENU HEADER-->
    <!-- START SIDEBAR MENU -->
    <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
            <li class="m-t-30">
                <a href="/administrator/dashboard" class="detailed">
                    <span class="title">Dashboard</span>
                    <span class="details">12 notifications</span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengguna</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="/administrator/user">Senarai Pengguna</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="/administrator/user/create">Cipta Pengguna Baru</a>
                        <span class="icon-thumbnail">cpb</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="https://osticket.data.gov.my/scp/">
                    <span class="title">Permohonan Data</span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-envelope"></i></span>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Laporan</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-bar-chart-o"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="http://www.data.gov.my/data/ms_MY/report">Broken Links & Openness</a>
                        <span class="icon-thumbnail">blo</span>
                    </li>
                    <li class="">
                        <a href="http://www.data.gov.my/data/ms_MY/dsw_reports">Dataset Approval Workflow</a>
                        <span class="icon-thumbnail">da</span>
                    </li>
                    <li class="">
                        <a href="http://www.data.gov.my/data/ms_MY/fq_reports">Update Frequency</a>
                        <span class="icon-thumbnail">uf</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Tetapan Menu</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-cogs"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="/administrator/menu">Senarai Menu</a>
                        <span class="icon-thumbnail">sm</span>
                    </li>
                    <li class="">
                        <a href="/administrator/menu/create">Cipta Menu Baru</a>
                        <span class="icon-thumbnail">cmb</span>
                    </li>
                    <li class="">
                        <a href="/administrator/menu/section/header">Menu Header</a>
                        <span class="icon-thumbnail">mh</span>
                    </li>
                    <li class="">
                        <a href="/administrator/menu/section/footer">Menu Footer</a>
                        <span class="icon-thumbnail">mf</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Tetapan CMS</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="/administrator/settings">Tetapan Sistem</a>
                        <span class="icon-thumbnail">ts</span>
                    </li>
                    <li class="">
                        <a href="/administrator/translation">Translation</a>
                        <span class="icon-thumbnail">ts</span>
                    </li>
                    <li class="">
                        <a href="/administrator/settings/banner">Banner</a>
                        <span class="icon-thumbnail">bn</span>
                    </li>
                    <li class="">
                        <a href="/administrator/mailtemplate">Mail Template</a>
                        <span class="icon-thumbnail">mt</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengurusan Kluster</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-th"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="/administrator/clusters">Icon Management</a>
                        <span class="icon-thumbnail">im</span>
                    </li>
                    <li class="">
                        <a href="/administrator/clusters/position">Position Management</a>
                        <span class="icon-thumbnail">pm</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengurusan Hebahan</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-bullhorn"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">Senarai Hebahan</a>
                        <span class="icon-thumbnail">sh</span>
                    </li>
                    <li class="">
                        <a href="#">Cipta Hebahan Baru</a>
                        <span class="icon-thumbnail">chb</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengurusan Laman</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">Senarai Laman</a>
                        <span class="icon-thumbnail">sl</span>
                    </li>
                    <li class="">
                        <a href="#">Cipta Laman Baru</a>
                        <span class="icon-thumbnail">clb</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="#">
                    <span class="title">Pengurusan Imej</span>
                </a>
                <span class="icon-thumbnail "><i class="fa fa-image"></i></span>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengurusan Soalan Lazim</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-comments"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">Senarai Soalan Lazim</a>
                        <span class="icon-thumbnail">ssl</span>
                    </li>
                    <li class="">
                        <a href="#">Cipta Soalan Lazim Baru</a>
                        <span class="icon-thumbnail">csl</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Article Management</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-paper-plane"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">Article List</a>
                        <span class="icon-thumbnail">al</span>
                    </li>
                    <li class="">
                        <a href="#">Create New Article</a>
                        <span class="icon-thumbnail">cna</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengurusan Penyumbang Data</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-share-alt"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">Senarai Penyumbang Set Data</a>
                        <span class="icon-thumbnail">spd</span>
                    </li>
                    <li class="">
                        <a href="#">Cipta Penyumbang Set Data</a>
                        <span class="icon-thumbnail">cpd</span>
                    </li>
                    <li class="">
                        <a href="#">Reload Semula Penyumbang Set Data</a>
                        <span class="icon-thumbnail">rsp</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengurusan Jenis Data</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-tags"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">Senarai Jenis Data</a>
                        <span class="icon-thumbnail">sjd</span>
                    </li>
                    <li class="">
                        <a href="#">Cipta Jenis Data Baru</a>
                        <span class="icon-thumbnail">cjd</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="#">
                    <span class="title">Pengurusan Set Data Pilihan</span>
                </a>
                <span class="icon-thumbnail "><i class="fa fa-crosshairs"></i></span>
            </li>
            <li class="">
                <a href="#">
                    <span class="title">Audit Trail</span>
                </a>
                <span class="icon-thumbnail "><i class="fa fa-list"></i></span>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->
</nav>
<!-- END SIDEBAR -->
<!-- END SIDEBAR -->