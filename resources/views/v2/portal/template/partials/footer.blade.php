<footer class="pr-footer">
    <div class="container my-auto">
        <div class="row py-5">
            <div class="col-3">
                <a href="#" class="text-light">Data</a><br/>
                <a href="#" class="text-light">Mengenai data.gov.my</a><br/>
                <a href="#" class="text-light">Terma Penggunaan</a><br/>
                <a href="#" class="text-light">Peneraju Data Terbuka</a>
            </div>
            <div class="col-3">
                <a href="#" class="text-light">Dasar Privasi</a><br/>
                <a href="#" class="text-light">Polisi Data</a><br/>
                <a href="#" class="text-light">Soalan Lazim</a><br/>
                <a href="#" class="text-light">Penafian</a>
            </div>
            <div class="col-3">
                <a href="#" class="text-light">Galeri Aplikasi</a><br/>
                <a href="#" class="text-light">Permohonan Set Data</a><br/>
                <a href="#" class="text-light">Maklum Balas</a><br/>
                <a href="#" class="text-light">Log Masuk</a>
            </div>
            <div class="col-3">
                <a href="#" class="text-light">Hubungi Kami</a><br/>
                <a href="#" class="text-light">Pekeliling Data Terbuka</a><br/>
                <a href="#" class="text-light">Amalan kebebasan maklumat</a>
            </div>
        </div>
        <div class="copyright text-center my-auto pt-3 pb-5">
            <span class="text-muted">Copyright &copy; MAMPU 2020</span>
        </div>
    </div>
</footer>
<div class="container-fluid mb-0 pb-0">
    <div class="row">
        <div class="col pr-top-line1">&nbsp;</div>
        <div class="col pr-top-line2">&nbsp;</div>
    </div>
</div>