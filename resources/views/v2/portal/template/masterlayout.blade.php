<!doctype html>
<html lang="en">
  <head>
    @include('v2.portal.template.partials.head')
  </head>
  <body>

    @include('v2.portal.template.partials.topbar')

    <!-- Begin Page Content -->
    @yield('content')

    @include('v2.portal.template.partials.footer')
    @include('v2.portal.template.partials.footer-scripts')

    @yield('script-after-load')
  </body>
</html>