<html>
<head>
<style>
img.center {
    display: block;
    margin: 0 auto;
}
</style>
</head>
<img src="{{url('http://150.242.183.95/images/data_gov.png')}}" class="center"><br>

<div>
<h4 style="color: #000000">Tuan/Puan, </h4><br>
<p style="color: #000000">Berikut adalah maklumat permohonan set data baru, </p><br>
<table>
	<tr style="height: 30px">
		<td style="width: 10%;">{{_('Tiket ID')}}</td>
		<td>:	{{ $details["tick"] }}</td>
	</tr>

    <tr style="height: 30px">
		<td style="width: 10%">{{_('Pemohon')}}</td>
		<td>:   {{ $details["name"] }}</td>
	</tr>

    <tr style="height: 30px">
		<td style="width: 10%">{{_('No. Telefon')}}</td>
		<td>:   {{ $details["phone"] }}</td>
	</tr>

	<tr style="height: 30px">
		<td style="width: 10%">{{_('E-mel')}}</td>
		<td>:   {{ $details["emel"] }}</td>
	</tr>

	<tr style="height: 30px">
		<td style="width: 10%">{{_('Tajuk')}}</td>
		<td>:   {{ $details["subjek"] }}</td>
	</tr>

	<tr style="height: 30px">
		<td style="width: 10%">{{_('Keterangan')}}</td>
		<td>:   {{ $details["description"] }}</td>
	</tr>

	<tr style="height: 30px">
		<td style="width: 10%">{{_('Catatan')}}</td>
		<td>:   {{ $details["notes"] }}</td>
	</tr><br>
</table>
<p>Sekian, terima kasih</p><br>

</html>