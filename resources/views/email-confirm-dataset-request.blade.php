<!DOCTYPE html>
<html>
<head>
<style>
img.center {
    display: block;
    margin: 0 auto;
}
</style>

 <title>Permohonan Set Data</title>
</head>
<body>
 <img src="{{url('http://150.242.183.95/images/data_gov.png')}}" class="center"><br>

 <div>
 <h4>Tuan/Puan, </h4><br>
 <p>Sukacita dimaklumkan bahawa borang permohonan set data anda telah diterima oleh pihak MAMPU. Kami mengucapkan ribuan terima kasih untuk maklum balas dari pihak anda dan berharap dengan maklum balas anda, pihak kami akan dapat memperbaiki Portal data.gov.my dari semasa ke semasa. Berikut adalah Tiket ID untuk rujukan anda, <span style="font-weight:bold">{{ $details["tick"] }}</span>.</p><br>
 <p>Sekian, terima kasih</p><br>

 <h4>E-mel ini dihantar oleh sistem. Anda tidak perlu membalas e-mel ini</h4>
 </div>

</body>
</html>

