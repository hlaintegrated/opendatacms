<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Super Admin',
            'email' => 'super_admin@odcms.test',
            'password' => bcrypt('abc123')
        ]);

        $user->assignRole('super_admin');
    }
}
