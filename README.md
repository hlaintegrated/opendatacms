## ODCMS  ```Open Data CMS Portal```

[Demo Site](http://150.242.183.95/)


### Installation ###

1. Git clone repo
2. Run `cp .env.example .env`
3. Install required packages `composer install`
4. Generate secret key `php artisan key:generate`
6. Update `.env` for db connection etc

### Initiate Backend ###

1. Run `composer dumpautoload` to load all packages
2. Run `php artisan migrate:refresh --seed` to refresh all databases + seeding
3. Run `php artisan serve` to run

## Developer Guide ##

#### How To Develop Locally ####

1. Make sure you are on master branch `git checkout master`
1. Get latest code update from remote before begin coding `git pull origin`
1. Create new branch from master based on issue number `git checkout -b <branch-name>`
1. Start coding and working on it
1. Stage all your changes `git add .`
1. Commit your code `git commit -m <branch-name-and-some-commit-message>`
1. Push changes to remote as branch `git push origin <branch-name>`
1. Create merge request