"use strict";

module.exports = function(grunt) {
  const sass = require("node-sass");

  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-contrib-cssmin");
  grunt.loadNpmTasks("grunt-contrib-uglify");

  grunt.loadNpmTasks('grunt-sass');

  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    watch: {
      sass: {
        files: "pages/scss/**/*.scss",
        tasks: "sass"
      }
    },
    sass: {
      default: {
        options: {
          implementation: sass,
          outputStyle: "expanded",
          sourceMap: true
        },
        files: {
          "pages/css/themes/light.css": "pages/scss/themes/light/light.scss"
        }
      }
    }
  });

  grunt.registerTask("sass-to-css", function() {
    grunt.task.run(["sass"]);
  });
};