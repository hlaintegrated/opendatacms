<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Permohonan Set Data</title>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.14/moment-timezone.min.js"></script>

        <style>
            .scrollable-menu {
                height: auto;
                max-height: 200px;
                overflow-x: hidden;
            }
            .title {
                font-weight: bold;
            }
            .mandatory {
                color: #ff0000;
            }
            .eg {
                font-size: 0.9rem;
            }
            .btn-text {
                font-weight:bold; 
                width:90px;
            }
            .mb0 {
                font-weight: bold; 
                color: #6c757d;
            }
        </style>
    </head>
    <body>
        <div class="container contact">
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header "><b>Permohonan Set Data</b></div>
                        <div class="card-body">
                        <?php if($message = Session::get('success')): ?>
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong><?php echo e($message); ?></strong>
                            </div>
                            <br>
                            <?php endif; ?>
                                <form action="<?php echo e(route('request.store')); ?>" method="POST">
                                    <?php echo csrf_field(); ?>
                                    <!-- Textarea Nama -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class ="col-form-label title">Name<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <input type="text" name="name" class ="form-control eg" placeholder="Cth: Syed Imran Hafizzudin " value="<?php echo e(old('name')); ?>"></input> 
                                                <span class="text-danger"><?php echo e($errors->first('name')); ?></span>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <!-- Textarea No. telefon -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class ="col-form-label title">No. Telefon<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <input type="text" name="phone" class ="form-control eg" placeholder="Cth: 01123374868" value="<?php echo e(old('phone')); ?>"></input> 
                                                <span class="text-danger"><?php echo e($errors->first('phone')); ?></span>
                                            </div>
                                        </div>
                                    <!-- Textarea E-mel -->
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class ="col-form-label title">Emel<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <input type="text" name="emel" class="form-control eg" placeholder="Cth: syedimran@domain.com" value="<?php echo e(old('emel')); ?>"></input> 
                                                <span class="text-danger"><?php echo e($errors->first('emel')); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Textarea Tajuk Set Data -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class="col-form-label title">Tajuk Set Data<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <input type="text" name="subjek" class="form-control eg" placeholder="Cth: Lokaliti Hotspot Denggi di Malaysia mengikut Negeri" value="<?php echo e(old('subjek')); ?>"></input>
                                                <span class="text-danger"><?php echo e($errors->first('subjek')); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Textarea Keterangan Set Data -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class ="col-form-label title">Keterangan Set Data<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <textarea rows="3" name="description" class="form-control eg" placeholder="Cth: Senarai denggi di Malaysia dari tahun 2000 hingga 2016 oleh negeri"><?php echo e(Request::old('description')); ?></textarea> 
                                                <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Textarea Catatan Lain -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class ="col-form-label title">Catatan Lain</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <textarea rows="3" name="notes" class ="form-control eg" placeholder="Cth: Mempunyai granulariti dataset mengikut statistik mingguan"><?php echo e(Request::old('notes')); ?></textarea> 
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Dropdown Penyumbang Set Data-->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="name" class ="col-form-label title">Penyumbang Set Data<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <select id="report" name="pembekal" class="form-control  eg" value="<?php echo e(old('pembekal')); ?>">
                                                <option selected disabled>Sila Pilih</option>
                                                <?php $__currentLoopData = $sources ['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $src): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value='<?php echo e($src->data_source_name); ?>'><?php echo e($src->data_source_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                                <span class="text-danger"><?php echo e($errors->first('pembekal')); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                    <div class="col-lg-4"></div>  
                                    <!-- Butang Batal Permohonan -->                     
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary btn-md eg btn-text">Batal</button>
                                            </div>
                                        </div>
                                    <!-- Butang Hantar Permohonan -->
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-md eg btn-text">Hantar</button> 
                                            </div>
                                        </div>
                                        <div class="col-lg-4"></div> 
                                    </div>
                                    
                                </form>
                            </div>
                    </div>
                </div>
                <!-- Ruang Panduan -->
                <div class="col-lg-3 mb0">
                    <div><img src="<?php echo e(URL::to('/images/info-icon.png')); ?>"></div><br>
                        <div>
                        <span>Sila isikan borang di sebelah untuk memohon set data</span>
                        </div>  
                        <br>
                        <div>
                        <span>NOTA: Medan bertanda <span class="mandatory">*</span> adalah mandatori</span>
                        </div>
                </div>
            </div>
        </div>
    </body>
    <!-- Script -->
    <script type='text/javascript'>

    </script>
</html>