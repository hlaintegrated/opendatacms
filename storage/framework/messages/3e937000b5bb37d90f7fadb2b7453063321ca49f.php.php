<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<?php echo e(_("Add FAQ Item")); ?>

	</div>
	<div class="panel-body">
		<?php echo Form::open(['method' => 'POST','action'=>['FAQItemController@store',$faq],'class'=>'form-horizontal multi-language-form','data-language-input'=>'question,-answer','files'=>'true','id'=>'faq-item-multi-language-form']); ?>


			<?php echo $__env->make('administrator.pages.faq.item.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

		<div class="form-group">
			<div class="col-sm-3"></div>
			<div class="col-sm-8">
				<button type="submit" class="form-control btn btn-primary"><?php echo e(_("Create")); ?></button>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<?php echo Form::close(); ?>

	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>