<?php $__env->startSection('content'); ?>

<div class="panel panel-default panel-admin">
	<?php $counter = 0; ?>
	<div class="panel-heading">
	<?php echo e(_('News')); ?> / <?php echo e(_('Event')); ?> / <?php echo e(_('Activity')); ?>

	</div>
	<div class="panel-body" >
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td>No.</td>
					<td><?php echo e(_("Title")); ?></td>
					<td><?php echo e(_("Publish Date")); ?></td>
					<td colspan="2"><?php echo e(_("Action")); ?></td>
				</thead>
				<?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td><?php echo e(++$counter); ?></td>
					<td><?php echo e($event->title); ?></td>
					<td><?php echo e($event->publish_date); ?></td>
					<td>
						<button title="<?php echo e(_('Edit Event')); ?>" class="btn btn-success glyphicon glyphicon-edit btn-block" onClick="window.location ='<?php echo e(action('EventController@edit',$event->id)); ?>'"></button>
					</td>
					<td>
						<?php echo Form::model($event,['method'=>'DELETE','action'=>['EventController@destroy',$event->id],'id'=>$event->id]); ?><?php echo Form::close(); ?>

						<button title="<?php echo e(_('Delete Event')); ?>" class="btn btn-danger glyphicon glyphicon-minus btn-block" onclick="deleteConfirmation('<?php echo e($event->id); ?>')"></button>
					</td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</table>
		</div>
	</div>
</div>
<div align="center">
	<?php echo $events->render(); ?>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>