<!doctype html>
<html lang="en">
  <head>
    <?php echo $__env->make('v2.portal.template.partials.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </head>
  <body>

    <?php echo $__env->make('v2.portal.template.partials.topbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <!-- Begin Page Content -->
    <?php echo $__env->yieldContent('content'); ?>

    <?php echo $__env->make('v2.portal.template.partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('v2.portal.template.partials.footer-scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php echo $__env->yieldContent('script-after-load'); ?>
  </body>
</html>