<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<?php echo e(_('Edit Mail Template')); ?> <?php echo e($template->name); ?>

	</div>
	<div class="panel-body">
		<?php echo Form::model($template,['method' => 'PATCH','action'=>['MailTemplateController@update',$template->id],'class'=>'form-horizontal']); ?>


		<div class="form-group">
			<div class="col-sm-2">
				<label class="control-label">Name</label>
			</div>
			<div class="col-sm-10">
				<label class="control-label"><?php echo e($template->name); ?></label>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-2">
				<label class="control-label">Description</label>
			</div>
			<div class="col-sm-10">
				<?php echo Form::textarea("description",null,['class'=>'form-control']); ?>

			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-2">
				<label class="control-label">Template</label>
			</div>
			<div class="col-sm-10">
				<?php echo Form::textarea("template",null,['class'=>'form-control','id'=>'template-editor']); ?>

			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-2">
				<label class="control-label">Available Variables: </label>
			</div>
			<div class="col-sm-10">
				<ul class="list-group">
				<?php $__currentLoopData = $template->template_fields_array(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<li class="list-group-item list-group-item-info">$<?php echo e($field); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-success"><?php echo e(_('Save')); ?></button>
			</div>
		</div>
		<?php echo Form::close(); ?>


	</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('additionalscript'); ?>
<script src="<?php echo e(asset('js/ckeditor/ckeditor.js')); ?>"></script>
<script>
    CKEDITOR.replace( 'template-editor' );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>