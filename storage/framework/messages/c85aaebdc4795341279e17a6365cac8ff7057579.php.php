<?php $__env->startSection('content'); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo e(_('Edit Article')); ?> <?php echo e($article->link); ?>

        </div>
        <div class="panel-body">
            <?php echo Form::model($article,['method' => 'PATCH','action'=>['ArticleController@update',$article->link],'class'=>'form-horizontal multi-language-form','data-language-input'=>'name','id'=>'article-multi-language-form']); ?>


            <?php echo $__env->make('administrator.pages.article.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-4">
                    <button type="submit" class="form-control btn btn-success"><?php echo e(_("Save")); ?></button>
                </div>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading ">
            <div class="row">
                <div class="col-sm-11">
                    <?php echo e(_('Article Item Management')); ?>

                </div>
                <div class="col-sm-1">
                    <a class="btn btn-primary btn-block" href='<?php echo e(action("ArticleItemController@create",$id)); ?>'><i
                                class="glyphicon glyphicon-plus"></i></a>
                </div>
            </div>
        </div>
        <style>
            .card {
                position: relative;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: vertical;
                -webkit-box-direction: normal;
                -ms-flex-direction: column;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 1px solid rgba(0, 0, 0, .125);
                border-radius: .25rem;
            }

            .card-header {
                padding: .75rem 1.25rem;
                margin-bottom: 0;
                background-color: rgba(0, 0, 0, .03);
                border-bottom: 1px solid rgba(0, 0, 0, .125);
            }

            .btn:not(:disabled):not(.disabled) {
                cursor: pointer;
            }

            .btn-link:hover {
                color: #0056b3;
                text-decoration: underline;
                background-color: transparent;
                border-color: transparent;
            }

            .mb-0, .my-0 {
                margin-bottom: 0 !important;
            }

            .card-header .mb-0 .collapsed .f {
                transform: rotate(-90deg);
            }

            .card-header .mb-0 .f {
                transition: .3s transform ease-in-out;
            }
        </style>
        <?php echo Form::open(['method' => 'POST','action'=>['ArticleController@saveposition',$id],'class'=>'form-horizontal']); ?>



        <div class="panel-body">
            <div class="drag-drop-row" id="portlet-row" style="padding:0px">
                <?php $__currentLoopData = $articleitems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $articleitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="card">
                        <div class="card-header panel-heading pointer">

                            <input type="hidden" name="ids[]" value="<?php echo e($articleitem->id); ?>">
                            <div class="mb-0">

                                <div class="col-sm-1"><a class="btn btn-success btn-block"
                                                         href="<?php echo e(action('ArticleItemController@edit',$articleitem->id)); ?>"><span
                                                class="fa fa-edit"></span></a></div>
                                <div class="col-sm-1">
                                    <button type="button" class="btn btn-danger btn-block "
                                            onclick="$(this).closest('.portlet').remove()"><span
                                                class="fa fa-trash"></span></button>
                                </div>
                                <a class="btn btn-link collapsed" style=" display: block" data-toggle="collapse"
                                   data-target="<?php echo e('#'.$articleitem->id); ?>" aria-expanded="true"
                                   aria-controls="collapseOne">
                                    <?php echo e($articleitem->title); ?>

                                    <i class="fa faz f fa-chevron-down pull-right"></i>
                                </a>


                            </div>
                        </div>
                        <div class="card-body collapse" id="<?php echo e($articleitem->id); ?>">
                            <div class="card card-body">

                                <?php if(str_word_count($articleitem->content)>6): ?>
                                    <div style="padding: 10px;overflow: auto">
                                    <?php echo $articleitem->content; ?>

                                    </div>
                                <?php else: ?>
                                    <?php
                                    $articless = \App\Model\ArticleItem::where('parent_id', $articleitem->id)->orderBy('position','asc')->get();

                                    ?>
                                    <div style="margin-top: 10px">
                                        <?php $__currentLoopData = $articless; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $articless): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $articless = $articless->local_content(); ?>

                                            <div class="card" style="margin: 2px 20px 2px 20px">
                                                <div class="card-header panel-heading pointer">

                                                    <input type="hidden" name="ids[]" value="<?php echo e($articless->id); ?>">
                                                    <div class="mb-0">

                                                        <div class="col-sm-1"><a class="btn btn-success btn-block"
                                                                                 href="<?php echo e(action('ArticleItemController@edit',$articless->id)); ?>"><span
                                                                        class="fa fa-edit"></span></a></div>
                                                        <div class="col-sm-1">
                                                            <button type="button" class="btn btn-danger btn-block "
                                                                    onclick="$(this).closest('.portlet').remove()"><span
                                                                        class="fa fa-trash"></span></button>
                                                        </div>
                                                        <a class="btn btn-link collapsed" style=" display: block"
                                                           data-toggle="collapse"
                                                           data-target="<?php echo e('#'.$articless->id); ?>" aria-expanded="true"
                                                           aria-controls="collapseOne">
                                                            <?php echo e($articless->title); ?>

                                                            <i class="fa faz f fa-chevron-down pull-right"></i>
                                                        </a>


                                                    </div>
                                                </div>
                                                <div class="card-body collapse" id="<?php echo e($articless->id); ?>">
                                                    <div class="card card-body" >
                                                        <div style="padding: 10px;overflow: auto">
                                                            <?php echo $articless->content; ?>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="col-sm-4 col-sm-offset-4">
                    <button type="submit" id="save-position" class="form-control btn btn-success"><?php echo e(_("Save")); ?></button>
                </div>
            </div>
        </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(asset('assets/app/js/dragdropobject.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/jquery/jquery-ui.min.js')); ?>"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>