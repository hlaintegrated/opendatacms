<?php if($feedback->status == 'unread'): ?>
	<i class="fa fa-exclamation-circle yellow fa-2x" title="<?php echo e(_('Unread')); ?>">
<?php else: ?>
	<a href="<?php echo e(action('FeedbackController@reportShow',$feedback->status)); ?>"><span class="fa fa-bookmark green fa-2x" title="<?php echo e(_('Read')); ?>"></span></a>				
<?php endif; ?>