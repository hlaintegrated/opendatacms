<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<?php echo e(_('Edit Data Type')); ?> <?php echo e($type->data_type_name); ?>

	</div>
	<div class="panel-body">
		<?php echo Form::model($type,['method' => 'PATCH','action'=>['DataTypeController@update',$type->data_type_id],'class'=>'form-horizontal']); ?>


			<?php echo $__env->make('administrator.pages.datatype.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-success"><?php echo e(_('Save')); ?></button>
			</div>
		</div>
		<?php echo Form::close(); ?>


	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>