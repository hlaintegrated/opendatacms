<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
	
	<?php $counter = 0; ?>
	<div class="panel-heading">
		<?php echo e(_("Language Management")); ?>

	</div>
	<div class="panel-body" >
            <?php echo Form::open(['method'=>'POST',"action"=>['SystemSettingController@saveLanguage',"Languages"]]); ?>

		<div>
			<table class="table table-bordered" align="center" style="white-space: nowrap" id="language">
				<thead>
					<td>No.</td>
					<td><?php echo e(_("Code")); ?></td>
					<td><?php echo e(_("Name")); ?></td>
                    <td><?php echo e(_('Remove')); ?></td>
				</thead>
				<?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="setting-row">
                    <td><?php echo e(++$counter); ?></td>
                    <td><input type="text" class="form-control" name="codes[]" placeholder="Code, ex: en_US, ms_MY" value="<?php echo e($key); ?>"></td>
                    <td><input type="text" class="form-control" name="values[]" placeholder="Value, ex: English, Bahasa Malaysia" value="<?php echo e($value); ?>"></td>
                    <td><button type="button" class="btn btn-danger btn-block fa fa-trash" onclick="$(this).closest('tr').remove()"></button>
                </tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</table>
            <div class="row">
                <div class="col-md-3 col-md-offset-2">
                    <button type="button" class="btn btn-primary btn-block" onclick="addLanguageRow()"><?php echo e(_('Add')); ?></button>
                </div>
                <div class="col-md-offset-2 col-md-3">
                    <button class="btn btn-success btn-block" type='submit' id="save-language"><?php echo e(_("Save")); ?></button>
                </div>
            </div>
		</div>
            <?php echo Form::close(); ?>   
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>