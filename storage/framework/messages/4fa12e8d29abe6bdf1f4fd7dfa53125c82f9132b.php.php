<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
	
	<div class="panel-heading">
		<?php echo e(_("Language Management")); ?>

	</div>
	<div class="panel-body" >
            <?php echo Form::open(['method'=>'POST',"action"=>['SystemSettingController@saveLanguage',$field]]); ?>

		<div>
			<table class="table table-bordered" align="center" style="white-space: nowrap" id="language">
				<thead>
					<td><?php echo e(_("Language")); ?></td>
					<td><?php echo e(_("Message")); ?></td>
				</thead>
				<?php $__currentLoopData = Settings::languages(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="setting-row">
					<input type="hidden" name="codes[]" value="<?php echo e($key); ?>">
                    <td><label class="form-label"><?php echo e($value); ?></label></td>
                    <td><textarea rows="1" class="form-control" name="values[]" placeholder="Value, ex: English, Bahasa Malaysia" style="resize: vertical"><?php echo e(array_key_exists($key,$settings)?$settings[$key]:''); ?></textarea></td>
                </tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</table>
            <div class="row">
                <div class="col-md-offset-4 col-md-4">
                    <button class="btn btn-success btn-block" type='submit' id="save-localized"><?php echo e(_("Save")); ?></button>
                </div>
            </div>
		</div>
            <?php echo Form::close(); ?>   
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>