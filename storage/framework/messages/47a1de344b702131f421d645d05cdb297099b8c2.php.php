<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<?php echo e(_('Edit Article Item')); ?>

	</div>
	<div class="panel-body">
		<?php echo Form::model($articleitem,['method' => 'POST','action'=>['ArticleItemController@update',$articleitem->id],'class'=>'form-horizontal multi-language-form','data-language-input'=>'title,-content','files'=>'true','id'=>'article-item-multi-language-form']); ?>


			<?php echo $__env->make('administrator.pages.article.item.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

		<div class="form-group">
			<div class="col-sm-3"></div>
			<div class="col-sm-8">
				<button type="submit" class="form-control btn btn-success"><?php echo e(_("Save")); ?></button>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<?php echo Form::close(); ?>

	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>