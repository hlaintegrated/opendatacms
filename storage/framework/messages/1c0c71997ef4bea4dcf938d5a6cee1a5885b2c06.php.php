<?php $__env->startSection('content'); ?>

<div class="panel panel-default" style="margin-top:20px">
	<?php $counter = 0; ?>
	<div class="panel-heading">
		<?php echo e(_('Dataset Request Management')); ?>

	</div>
	<div class="panel-body" >
        <div class="dropdown" style="padding: 10px">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select Filter</button>
            <ul class="dropdown-menu">
                <li><a href="<?php echo e(action('DatasetRequestManagementController@index',$id)); ?>?status=all">All</a></li>
                <li><a href="<?php echo e(action('DatasetRequestManagementController@index',$id)); ?>?status=in_progress">In Progress</a></li>
                <li><a href="<?php echo e(action('DatasetRequestManagementController@index',$id)); ?>?status=completed">Completed</a></li>
                <li><a href="<?php echo e(action('DatasetRequestManagementController@index',$id)); ?>?status=rejected">Rejected</a></li>
            </ul>
        </div>
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td><?php echo e(_('No')); ?>.</td>
					<td><?php echo e(_("Sender")); ?></td>
					<td><?php echo e(_("Title")); ?></td>
					<td><?php echo e(_("Description")); ?></td>
					<td><?php echo e(_("Type")); ?></td>
					<td><?php echo e(_("Source")); ?></td>
					<td align="center"><?php echo e(_("Status")); ?></td>
					<td align="center"><?php echo e(_("Detail")); ?></td>
				</thead>
				<?php $__currentLoopData = $dataset_requests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dataset_request): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr>	
						<td><?php echo e($dataset_requests->total()-$dataset_requests->perPage()*($dataset_requests->currentPage()-1)-$counter); ?></td>
						<td><?php echo e($dataset_request['name']); ?></td>
						<td><?php echo e(strlen($dataset_request['title']) > 15? substr($dataset_request['title'],0,12).'...':$dataset_request['title']); ?></td>
						<td><?php echo e(strlen($dataset_request['description']) > 30? substr($dataset_request['description'],0,27).'...':$dataset_request['description']); ?></td>
<td align="center"><p title="<?php $__currentLoopData = $dataset_request->types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($type->data_type_name); ?>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>"><?php echo e(count($dataset_request->types)); ?></p></td>
<td align="center"><p title="<?php $__currentLoopData = $dataset_request->sources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $source): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($source->data_source_name); ?>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>"><?php echo e(count($dataset_request->sources)); ?></p></td>
						<td  align="center">
							<?php echo $__env->make('administrator.pages.datasetrequest.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
						</td>
						<td  align="center">
							<a class="btn btn-success fa fa-file" href="<?php echo e(action('DatasetRequestManagementController@edit',$dataset_request->request_id)); ?>" title="<?php echo e(_('Show Detail')); ?>"></a>
						</td>
					</tr>
					<?php $counter++; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</table>
		</div>
	</div>
</div>

<div align="center">
	<ul class="pagination pagination-lg">
		<?php for($i = 1; $i <= ($dataset_requests->total()+$dataset_requests->perPage())/$dataset_requests->perPage(); $i++): ?>
		<li><a href="datasetrequest?page=<?php echo e($i); ?>"><?php echo e($i); ?></a></li>
		<?php endfor; ?>
	</ul>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.request', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>