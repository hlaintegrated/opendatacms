<?php $__env->startSection('additionalstyle'); ?>
<link rel="stylesheet" href="<?php echo e(asset('assets/app/css/dragdropobject.css')); ?>" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('additionalscript'); ?>
<script src="<?php echo e(asset('assets/app/js/dragdropobject.js')); ?>"></script>
<script src="<?php echo e(asset('assets/jquery/jquery-ui.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<!-- START PAGE CONTENT -->
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Tetapan Menu</a></li>
                    <li class="breadcrumb-item active">Menu Header</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		<div class="row">
			<div class="form-group col-md-10">
				<div class="form-group">
					<div class="col-md-2">
						<label for="menu" class="control-label col-md-2"><?php echo e(_('Menu')); ?></label>
					</div>
					<div class="col-md-10">
						<select class="form-control" required name="menu" id="portlet-add">
							<option value="noselect" disabled selected><?php echo e(_("Select Available Menu")); ?></option>
							<?php $__currentLoopData = $selects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $select): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($select->link); ?>"><?php echo e($select->link); ?> - <?php echo e($select->title); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<button class="btn btn-primary" onclick="addMenu()"><?php echo e(_("Add")); ?></button>
			</div>
		</div>



		<div class="row panel panel-default">

			<div class="panel-heading">
				<?php echo e(_($page." Menu Position Management")); ?>

			</div>
			<div class="panel-body" >
				<div class="drag-drop-row" id="portlet-row" style="padding:0px">
				<?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="portlet panel panel-default" id="<?php echo e($menu->link); ?>">
		                <input type="hidden" name="links[]" value="<?php echo e($menu->link); ?>">
						<div class="portlet-header panel-heading"><?php echo e($menu->title); ?></div>
						<div class="portlet-content panel-body">Link: <?php echo e($menu->link); ?>

							<button type="button" title="<?php echo e(_('Remove Menu')); ?>"  onClick="deleteConfirmation('menu<?php echo e($menu->link); ?>')" class="btn btn-danger glyphicon glyphicon-minus" style="float:right"></button>
						</div>
					</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		</div>
        <div class="col-sm-4 col-sm-offset-4">
		    <button id="save-footer" class="btn btn-success btn-block"><?php echo e(_("Save")); ?></button>
        </div>
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('v2.admin.template.adminlayout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>