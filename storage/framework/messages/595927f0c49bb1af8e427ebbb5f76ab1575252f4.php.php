<?php $__env->startSection('content'); ?>
<!-- START PAGE CONTENT -->
<div class="content">
	<!-- START JUMBOTRON -->
	<div class="jumbotron" data-pages="parallax">
		<div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
			<div class="inner">
				<!-- START BREADCRUMB -->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Pengurusan Kluster</a></li>
					<li class="breadcrumb-item active">Icon Management</li>
				</ol>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- END JUMBOTRON -->
	<!-- START CONTAINER FLUID -->
	<div class="container-fluid container-fixed-lg">
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		<?php echo Form::open(['action'=>['ClusterController@save'],'files' => 'true']); ?>

		<div class="panel panel-default">
			<?php $counter = 0; ?>
			<div class="panel-heading">
				<?php echo e(_('Cluster Management')); ?>

			</div>
			<div class="panel-body">
				<div style="overflow:auto">
					<table class="table table-bordered cluster-view-cell" align="center">
						<thead>
							<td><?php echo e(_('No')); ?>.</td>
							<td><?php echo e(_('Link')); ?></td>
							<td><?php echo e(_('Title')); ?></td>
							<td><?php echo e(_('Home Show')); ?></td>
							<td><?php echo e(_('Image')); ?></td>
							<td><?php echo e(_('Hover Image')); ?></td>
						</thead>
						<?php $__currentLoopData = $clusters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cluster): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

						<tr>
							<input type="hidden" name="link[]" value="<?php echo e($cluster->link); ?>">
							<td><?php echo e(++$counter); ?></td>
							<td>
								<p><?php echo e($cluster->link); ?></p>
							</td>
							<td>
								<p><?php echo e(T::_($cluster->title)); ?></p>
							</td>
							<td><?php echo Form::checkbox("show$cluster->link",null,$cluster->home_show); ?></td>
							<td>
								<img src='<?php echo e(asset("images/cluster/$cluster->image")); ?>' style='max-height:50px; max-width:50px'>
								<?php echo Form::file('image[]',null,[ 'id' => 'image','accept' =>'.png,.jpg,.bmp']); ?>

							</td>
							<td>
								<img src='<?php echo e(asset("images/cluster/$cluster->hover_image")); ?>' style='max-height:50px; max-width:50px'>
								<?php echo Form::file('hover_image[]',null,['id' => 'image','accept' =>'.png,.jpg,.bmp']); ?>

							</td>

						</tr>

						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</table>
				</div>
			</div>
			<div class="panel-footer">
				<div class="row">
					<button class="btn btn-success col-md-4 col-md-offset-4"><?php echo e(_('Save')); ?></button>
				</div>
			</div>
		</div>
		<?php echo Form::close(); ?>

		<!-- END PLACE PAGE CONTENT HERE -->
	</div>
	<!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('v2.admin.template.adminlayout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>