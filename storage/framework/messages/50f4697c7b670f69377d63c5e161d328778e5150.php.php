<ul class="nav nav-tabs" id="language-selector">
    <?php $__currentLoopData = Settings::languages(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code => $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <li class="tab-container" id="<?php echo e($code); ?>-tab"><a class="language-tab" data-language="<?php echo e($code); ?>"><?php echo e($name); ?></a></li>
    <?php echo Form::hidden($code.'-question',null,['id'=>"$code".'-question']); ?>

    <?php echo Form::hidden($code.'-answer',null,['id'=>"$code".'-answer']); ?>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>

<!-- Text Box for Question -->
<div class="form-group">
	<div class="col-md-1"></div>
	<?php echo Form::label('question',_('Question').'*',['class' => 'control-label col-md-2']); ?>

	<div class="col-md-8">
		<?php echo Form::text('question',null,['class'=>'form-control', 'id' => 'question','required'=>'required']); ?>

	</div>
	<div class="col-md-1"></div>
</div>
<!-- Text Area for Answer -->
<div class='form-group'>
   <div class='col-md-1'></div>
   <?php echo Form::label('answer',_('Answer').'*',['class' => 'control-label col-md-2']); ?>

   <div class='col-md-8'>
       <?php echo Form::textarea('answer',null,['class'=>'form-control', 'id' => 'answer','required'=>'required']); ?>

   </div>
   <div class='col-md-1'></div>
</div>


<?php $__env->startSection('additionalscript'); ?>
    <script src="<?php echo e(asset('js/ckeditor/ckeditor.js')); ?>"></script>
    <script>
        CKEDITOR.replace( 'answer' )
    </script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('loader'); ?>
    <?php echo $__env->make('partials.loader', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>