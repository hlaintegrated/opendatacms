<?php $__env->startSection('content'); ?>
<!-- START PAGE CONTENT -->
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
                    <li class="breadcrumb-item active">Pengurusan Pengguna</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="panel panel-default panel-admin">
            <?php $counter = 0; ?>
            <div class="panel-heading">
                Pengurusan Pengguna
            </div>
            <div class="panel-body">

                <div class="row" style="margin-bottom: 10px">
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label class="control-label">Search Name</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-block btn-primary"><?php echo e(_("Search")); ?></button>
                        </div>
                    </div>
                </div>
               
                <div style="overflow:auto" class="row">
                    <table class="table table-bordered" align="center" style="white-space: nowrap">
                        <thead>
                            <td>No.</td>
                            <td><?php echo e(_("Username")); ?></td>
                            <td><?php echo e(_("Created")); ?></td>
                            <td><?php echo e(_("Full Name")); ?></td>
                            <td><?php echo e(_("Email")); ?></td>
                            <td><?php echo e(_("Sysadmin")); ?></td>
                            <?php if(env("TICKETING","default") == "default"): ?>
                            <td><?php echo e(_("Data Source")); ?></td><?php endif; ?>
                            <td><?php echo e(_("State")); ?></td>
                            <td class="text-center" colspan="4"><?php echo e(_("Action")); ?></td>
                        </thead>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e(++$counter); ?></td>
                            <td><?php echo e($user->name); ?></td>
                            <td><?php echo e($user->created); ?></td>
                            <td><?php echo e($user->fullname); ?></td>
                            <td><?php echo e($user->email); ?></td>
                            <td align="center">
                                <?php if($user->sysadmin): ?>
                                <i class="fa fa-check-circle fa-2x green" title="<?php echo e(_('Sysadmin')); ?>"></i>
                                <?php else: ?>
                                <i class="fa fa-times-circle fa-2x red" title="<?php echo e(_('Not Sysadmin')); ?>"></i>
                                <?php endif; ?>
                            </td>
                            <?php if(env("TICKETING","default") == "default"): ?>
                            <td align="center">
                                <?php if($user->data_source): ?>
                                <i class="fa fa-check-circle fa-2x green" title="<?php echo e(_('Data Source')); ?>"></i>
                                <?php else: ?>
                                <i class="fa fa-times-circle fa-2x red" title="<?php echo e(_('Not Data Source')); ?>"></i>
                                <?php endif; ?>
                            </td><?php endif; ?>
                            <td align="center">
                                <?php if($user->state == 'active'): ?>
                                <i class="fa fa-check-circle fa-2x green" title="<?php echo e(_('Active')); ?>"></i>
                                <?php elseif($user->state =='suspended'): ?>
                                <i class="fa fa-times-circle fa-2x red" title="<?php echo e(_('Suspended')); ?>"></i>
                                <?php else: ?>
                                <a href="<?php echo e(action("UserController@resend",$user->id)); ?>"><i class="fa fa-envelope fa-2x yellow" title="<?php echo e(_('Waiting For Activation. Click here for resending email')); ?>"></i></a>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if(!$user->sysadmin): ?>
                                <button class="btn btn-success glyphicon glyphicon-edit" title="<?php echo e(_('Edit User')); ?>" onClick="window.location ='<?php echo e(action('UserController@edit',$user->id)); ?>'"></button>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if(!$user->sysadmin): ?>
                                <button class="btn btn-danger glyphicon glyphicon-remove-circle" title="<?php echo e(_('Delete User')); ?>" onClick="alerts('<?php echo e($user->id); ?>','<?php echo e($user->name); ?>')"></button>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if(!$user->sysadmin): ?>
                                <?php if($user->state == 'active'): ?>
                                <button class="btn btn-danger glyphicon glyphicon-minus" title="<?php echo e(_('Suspend User')); ?>" onClick="window.location = '<?php echo e(action('UserController@suspend',$user->id)); ?>'"></button>
                                <?php else: ?>
                                <button class="btn btn-primary glyphicon glyphicon-plus" title="<?php echo e(_('Unsuspend User')); ?>" onClick="window.location = '<?php echo e(action('UserController@unsuspend',$user->id)); ?>'"></button>
                                <?php endif; ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <a class="btn btn-primary glyphicon glyphicon-envelope" title="<?php echo e(_('Reset Activation And Password Reset Email')); ?>" href="<?php echo e(action("UserController@resend",$user->id)); ?>"></a>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                </div>
            </div>
        </div>
        <div align="center">
            <ul class="pagination pagination-lg">
            </ul>
        </div>
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>
    function alerts(user, name) {
        console.log(user, name);
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete ' + name + '?',
            buttons: {
                confirm: function() {
                    window.location = "/administrator/user/" + user + "/destroy";
                },
                cancel: function() {
                    $.alert('Canceled!');
                },

            }
        });
    }
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('v2.admin.template.adminlayout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>