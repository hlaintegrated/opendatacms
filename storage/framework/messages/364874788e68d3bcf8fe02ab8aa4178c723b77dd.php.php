<?php $__env->startSection('content'); ?>
<section id="section-latest-news" class="section">
	<h2 class="section-title h1 clearfix">
		<?php echo e(_('Images')); ?>

		<button class="btn btn-primary" style="float:right" onclick="window.location='<?php echo e(action('ImageController@create')); ?>'"><i class="glyphicon glyphicon-plus"></i></button>
	</h2>
	<div class = "panel-group">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a data-toggle="collapse" href="#private"><?php echo e(_('Private Images')); ?> </a><span class="badge pull-right"><?php echo e(count($private)); ?></span></h4>
			</div>
			<div class="panel-collapse collapse" id="private">
				<ul class="list-group">
					<?php $__currentLoopData = $private; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li class="row list-group-item">
							<div class="col-md-1"><img class="admin-image" src="<?php echo e(action('ImageController@show',$p->image_id)); ?>"></div>
							<div class="col-md-1"><?php echo ($p->carousel)?_('Carousel'):''; ?></div>
							<div class="col-md-8" style="overflow:hidden"><b><?php echo e($p->name); ?></b><br/><a href="<?php echo e(action('ImageController@show',$p->image_id)); ?>"><?php echo e(action('ImageController@show',$p->image_id)); ?></a></div>
							<div class="col-md-1"><button title="<?php echo e(_('Edit Image')); ?>" class="btn btn-success glyphicon glyphicon-edit btn-block btn-mampu" onclick="window.location='<?php echo e(action('ImageController@edit',$p->image_id)); ?>'"></button></div>
							<?php echo Form::model($p,['method'=>'DELETE','action'=>['ImageController@destroy',$p->image_id],'id'=>$p->image_id]); ?><?php echo Form::close(); ?>

							<div class="col-md-1"><button title="<?php echo e(_('Delete Image')); ?>" type="submit" class="btn btn-danger glyphicon glyphicon-minus btn-block btn-mampu" onclick="deleteConfirmation('<?php echo e($p->image_id); ?>')"></button></div>
						</li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div>
		</div>
	</div>
	<div class = "panel-group">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a data-toggle="collapse" href="#public"><?php echo e(_('Public Images')); ?> </a><span class="badge pull-right"><?php echo e(count($public)); ?></span></h4>
			</div>
			<div class="panel-collapse collapse" id="public">
				<ul class="list-group">
					<?php $__currentLoopData = $public; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li class="row list-group-item">
							<div class="col-md-2"><img class="admin-image" src="<?php echo e(action('ImageController@show',$p->image_id)); ?>"></div>
							<div class="col-md-1"><?php echo ($p->carousel)?_('Carousel'):''; ?></div>
							<div class="col-md-8" style="overflow:hidden"><b><?php echo e($p->name); ?></b><br/><a href="<?php echo e(action('ImageController@show',$p->image_id)); ?>"><?php echo e(action('ImageController@show',$p->image_id)); ?></a></div>
							<div class="col-md-1"><button title="<?php echo e(_('Edit Image')); ?>" class="btn btn-success glyphicon glyphicon-edit btn-block btn-mampu" onclick="window.location='<?php echo e(action('ImageController@edit',$p->image_id)); ?>'"></button></div>
							<?php echo Form::model($p,['method'=>'DELETE','action'=>['ImageController@destroy',$p->image_id],'id'=>$p->image_id]); ?><?php echo Form::close(); ?>

							<div class="col-md-1"><button title="<?php echo e(_('Delete Image')); ?>" type="submit" class="btn btn-danger glyphicon glyphicon-minus btn-block btn-mampu" onclick="deleteConfirmation('<?php echo e($p->image_id); ?>')"></button></div>
						</li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div>
		</div>
	</div>
	<div class = "panel-group">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a data-toggle="collapse" href="#others"><?php echo e(_("Other's Images")); ?> </a><span class="badge pull-right"><?php echo e(count($others)); ?></span></h4>
			</div>
			<div class="panel-collapse collapse" id="others">
				<ul class="list-group">
					<?php $__currentLoopData = $others; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li class="row list-group-item">
							<div class="col-md-2"><img class="admin-image" src="<?php echo e(action('ImageController@show',$p->image_id)); ?>"></div>
							<div class="col-md-1"><?php echo ($p->carousel)?_('Carousel'):''; ?></div>
							<div class="col-md-8" style="overflow:hidden"><b><?php echo e($p->name); ?></b><br/><a href="<?php echo e(action('ImageController@show',$p->image_id)); ?>"><?php echo e(action('ImageController@show',$p->image_id)); ?></a></div>
							<div class="col-md-2"></div>
						</li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>


						
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>