<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<?php echo e(_('Edit user')); ?> <?php echo e($user->fullname); ?>

	</div>
	<div class="panel-body">
		<?php echo Form::model($user,['method' => 'PATCH','action'=>['UserController@update',$user->id],'class'=>'form-horizontal']); ?>

<input type="hidden" name="id" value="<?php echo e($user->id); ?>">
			<?php echo $__env->make('administrator.pages.user.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-success"><?php echo e(_('Save')); ?></button>
			</div>
		</div>
		<?php echo Form::close(); ?>


	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>