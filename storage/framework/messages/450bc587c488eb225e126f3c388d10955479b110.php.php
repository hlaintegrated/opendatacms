<?php $__env->startSection('content'); ?>
<button onclick="window.location='<?php echo e(action('DatasetRequestController@edit',$dataset_request->request_id)); ?>'" class="btn btn-success">Edit</button>
<div class="panel panel-default" style="margin-top:20px">
	<div class="panel-heading">
		<?php echo e(_('Dataset Request Detail')); ?>

	</div>
	<div class="panel-body">
		<table class="table-striped table-hover" width="100%">
			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Sender')); ?></td>
				<td><?php echo e($dataset_request->name); ?></td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Email')); ?></td>
				<td><?php echo e($dataset_request->email); ?></td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Phone')); ?></td>
				<td><?php echo e($dataset_request->phone); ?></td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Title')); ?></td>
				<td><?php echo e($dataset_request->title); ?></td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Description')); ?></td>
				<td><?php echo e($dataset_request->description); ?></td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Remarks')); ?></td>
				<td><?php echo e($dataset_request->notes); ?></td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Sources')); ?></td>
				<td>
					<ul>
						<?php $__currentLoopData = $dataset_request->sources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $source): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><?php echo e(T::_($source->data_source_name)); ?></li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</ul>
				</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Types')); ?></td>
				<td>
					<ul>
						<?php $__currentLoopData = $dataset_request->types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><?php echo e($type->data_type_name); ?></li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</ul>
				</td>
			</tr>



			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Status')); ?></td>
				<td>
					<?php echo $__env->make('administrator.pages.datasetrequest.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
				</td>
			</tr>
		</table>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>