<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
	
	<div class="panel-heading">
		<?php echo e(_("Setting Management")); ?>

	</div>
	<div class="panel-body" >
            <?php echo Form::open(['method'=>'POST',"action"=>['SystemSettingController@saveOther',$field]]); ?>

		<div>
			<table class="table table-bordered" align="center" style="white-space: nowrap" id="language">
				<thead>
					<td><?php echo e(_("Key")); ?></td>
					<td><?php echo e(_("Value")); ?></td>
				</thead>
                <tr class="setting-row">
                    <td><label class="form-label"><?php echo e($field); ?></label></td><td>
				<?php if($setting->name == 'Default Color Theme'): ?>
						<select name="value" class="form-control">
							<?php $__currentLoopData = Settings::theme_colors(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($color); ?>" <?= $color == $setting->value?"selected":"" ?>><?php echo e($color); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
				<?php else: ?>
                    	<textarea rows="1" class="form-control" name="value" style="resize: vertical"><?php echo e($setting->value); ?></textarea>
                <?php endif; ?>
                	</td>
                </tr>
			</table>
            <div class="row">
                <div class="col-md-offset-4 col-md-4">
                    <button class="btn btn-success btn-block" type='submit' id="save-localized"><?php echo e(_("Save")); ?></button>
                </div>
            </div>
		</div>
            <?php echo Form::close(); ?>   
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>