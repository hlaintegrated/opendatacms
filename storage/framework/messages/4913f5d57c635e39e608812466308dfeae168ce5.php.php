<?php $__env->startSection('content'); ?>
<div class="panel panel-default" style="margin-top:20px">
	<div class="panel-heading">
		<?php echo e(_('Data Type Detail')); ?>

	</div>
	<div class="panel-body">
		<table class="table-striped table-hover" width="100%">
		<tr style="height: 30px">
			<td style="width: 20%"><?php echo e(_('Name')); ?></td>
			<td><?php echo e($type->data_type_name); ?></td>
		</tr>
		
		<tr style="height: 30px">
			<td style="width: 20%"><?php echo e(_('Detail')); ?></td>
			<td><?php echo e($type->data_type_detail); ?></td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%"><?php echo e(_('Availability')); ?></td>
			<td><?php echo $__env->make('administrator.pages.datatype.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></td>
		</tr>
		</table>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>