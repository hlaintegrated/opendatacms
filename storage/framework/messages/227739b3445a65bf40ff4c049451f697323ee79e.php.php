<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    //
    public function index()
    {
        return view('v2/admin/template/public_pages/login');
    }
}
