<?php

namespace App\Http\Controllers\V2;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MailTemplate;
use App\Http\Requests\MailTemplateRequest;

class MailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $templates = MailTemplate::all();
        $templates = array();
        return view("v2/admin/template/pages/mailtemplate/index",compact("templates"));

    }


    public function edit($id)
    {
        $template = MailTemplate::find($id);
        return view("administrator.pages.mailtemplate.edit",compact("template"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MailTemplateRequest $request, $id)
    {
        $template = MailTemplate::find($id);
        $template->update($request->all());
        return redirect(action("MailTemplateController@index"))->with("message",_("Template Updated"));
    }
}
