<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('assets/jquery/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/app/js/dragdropobject.js')); ?>"></script>
<script>
function addHome(name)
{
    var target = '<?php echo e(url("administrator/card/home/")); ?>/'+name;
    $.get(target,function(data){
        $("#portlet-row").append(data);
    });
    $("#portlet-row").sortable('refresh');
}
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php $__currentLoopData = $homes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $home): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php echo Form::model($home,['method'=>'DELETE','action'=>['HomeController@destroy',$home->id],'id'=>'home'.$home->id]); ?><?php echo Form::close(); ?> 
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php echo Form::open(['method' => 'POST','action'=>['HomeController@save'],'class'=>'form-horizontal']); ?>

   
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-10">
            <?php echo e(_('Home Widget Management')); ?>

            </div>
            <div class="col-sm-2">
                <a href="<?php echo e(action('HomeController@create')); ?>" class="btn btn-primary"><?php echo e(_('Add Widget')); ?></a>                    
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="drag-drop-row" id="portlet-row" style="padding:0px">
            <?php $__currentLoopData = $homes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $home): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="portlet panel panel-default" id="<?php echo e($home->id); ?>">
                <input type="hidden" name="id[]" value="<?php echo e($home->id); ?>">
                <div class="portlet-header panel-heading"><?php echo e($home->title); ?></div>
                <div class="portlet-content panel-body">Description: <?php echo e($home->description); ?>

                    <button title="<?php echo e(_('Remove Home')); ?>"  type="button" onClick="deleteConfirmation('home<?php echo e($home->id); ?>')" class="btn btn-danger glyphicon glyphicon-minus" style="float:right"></button>
                    <a href="<?php echo e(action('HomeController@edit',$home->id)); ?>" class="btn btn-success glyphicon glyphicon-edit" style="float:right; margin-right: 1%"></a>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    <div class="panel-footer">
        <div class='form-group'>
            <div class='col-sm-3'></div>
            <div class='col-sm-8'>
                <button type='submit' class='form-control btn btn-primary'><?php echo e(_('Save')); ?></button>
            </div>
            <div class='col-sm-1'></div>
        </div>
    </div>
</div>

<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>