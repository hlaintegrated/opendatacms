<?php $__env->startSection('content'); ?>
<div class="panel panel-default" style="margin-top:20px">
	<div class="panel-heading">
		<?php echo e(_('Generated Reports')); ?>

	</div>
	<div class="panel-body" >
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td><?php echo e(_('No.')); ?>.</td>
					<td><?php echo e(_("Created At")); ?></td>
					<?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<td><?php echo e(_($type)); ?></td>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<td align="center"><?php echo e(_("Detail")); ?></td>
				</thead>
				<?php $__currentLoopData = $reports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $number=>$report): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr>	
						<td><?php echo e($number); ?></td>
						<?php $__currentLoopData = $report; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<td><?php echo e($field); ?></td>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<td  align="center">
							<button class="btn btn-success fa fa-file" onclick="window.location='<?php echo e(action('FeedbackController@reportShow',$number)); ?>'" title="<?php echo e(_('Show Detail')); ?>"></button>
						</td>
					</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</table>
		</div>
	</div>
</div>

<div align="center">
	<ul class="pagination pagination-lg">
		<?php for($i = 1; $i <= $pages; $i++): ?>
		<li><a href="reports?page=<?php echo e($i); ?>"><?php echo e($i); ?></a></li>
		<?php endfor; ?>
	</ul>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>