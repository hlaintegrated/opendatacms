<?php $__env->startSection('content'); ?>

    <div class="panel panel-default panel-admin">
        <div class="panel-heading">
            <?php echo e(_("Audit Trail")); ?>

        </div>
        <div class="panel-body" >
            <div style="overflow:auto">
                
                    
                        
                    
                    
                
                <div class="row" style="padding-bottom:10px;">
                    <div class="col-sm-5">
                        <div class="form-inline">
                            <div class="form-group mb-3">
                                <input type="text" class="form-control" value="<?php echo e($search); ?>" id="search">
                            </div>
                            <button class="btn btn-primary mb-2" onclick="searchme()" >Search</button>
                        </div>
                    </div>
                </div>

                <div>

                    <table class="table table-bordered" align="center" style="white-space: nowrap">
                        <thead>
                        <td>No.</td>
                        <td><?php echo e(_("User")); ?></td>
                        <td><?php echo e(_("Ref")); ?></td>
                        <td><?php echo e(_("Time")); ?></td>
                        <td><?php echo e(_("Action")); ?></td>
                        <td></td>
                        </thead>
                        <?php $__currentLoopData = $audits; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $audit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($audit->id); ?></td>
                                <td><?php echo e($audit->user->name); ?></td>
                                <td><?php echo e(substr($audit->ref, 0, 30)); ?>... <span title="<?php echo e($audit->ref); ?>" style="point"  data-toggle="tooltip" ><i class="fa fa-eye"></i></span></td>
                                <td><?php echo e($audit->at); ?></td>
                                <td><?php echo e(substr($audit->message,0,30)); ?>... <span title="<?php echo e($audit->message); ?>" style="point"  data-toggle="tooltip" ><i class="fa fa-eye"></i></span></td>
                                <td><button class="btn btn-primary" onclick="view(<?php echo e($audit->id); ?>)">View</button></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div align="center">
        <ul class="pagination pagination-lg">
            <?php echo $audits->render(); ?>

            
                
            
        </ul>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="view_diff_modal" tabindex="-1" role="dialog" aria-labelledby="view_diff_modal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">View Diff</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="diff_item">
                    loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function view(id) {
            $('#view_diff_modal').modal('toggle');
            $( "#diff_item" ).load( "<?php echo e(url('administrator/audit')); ?>/" + id + "/view");
        }

        function searchme() {
            document.location.href= "<?php echo e(url('administrator/audit')); ?>/" + $('#search').val();
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>