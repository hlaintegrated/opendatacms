<?php $__env->startSection('content'); ?>

<div class="panel panel-default panel-admin">
	<?php $counter = 0; ?>
	<div class="panel-heading">
		<?php echo e(_('Feedback Management')); ?>

	</div>
	<div class="panel-body" >
        <div class="dropdown" style="padding: 10px">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><?php echo e(_('Select Filter')); ?></button>
            <ul class="dropdown-menu">
                <li><a href="<?php echo e(action('FeedbackController@index')); ?>?status=all"><?php echo e(_('All')); ?></a></li>
                <li><a href="<?php echo e(action('FeedbackController@index')); ?>?status=unread"><?php echo e(_('Unread')); ?></a></li>
                <li><a href="<?php echo e(action('FeedbackController@index')); ?>?status=read"><?php echo e(_('Read')); ?></a></li>
            </ul>
        </div>
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td><?php echo e(_('No')); ?>.</td>
					<td><?php echo e(_("Sender's Name")); ?></td>
					<td><?php echo e(_("Email Address")); ?></td>
					<td><?php echo e(_("Feedback Type")); ?></td>
					<td><?php echo e(_("Title")); ?></td>
					<td><?php echo e(_("Description")); ?></td>
					<td><?php echo e(_("Reference")); ?></td>
					<td align="center"><?php echo e(_("Feedback Status")); ?></td>
					<td><?php echo e(_("Created Date")); ?></td>
					<td align="center"><?php echo e(_("View Detail")); ?></td>
				</thead>
				<?php $__currentLoopData = $feedbacks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feedback): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr>	
						<td><?php echo e($feedbacks->total()-$feedbacks->perPage()*($feedbacks->currentPage()-1)-$counter); ?></td>
						<td><?php echo e($feedback['name']); ?></td>
						<td><?php echo e($feedback['email']); ?></td>
						<td><?php echo e($feedback['type']); ?></td>
						<td><?php echo e(strlen($feedback['title']) > 15? substr($feedback['title'],0,12).'...':$feedback['title']); ?></td>
						<td><?php echo e(strlen($feedback['description']) > 30? substr($feedback['description'],0,27).'...':$feedback['description']); ?></td>
						<td><?php echo e($feedback['reference']); ?></td>
						<td  align="center">
						<?php echo $__env->make('administrator.pages.feedback.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
						</td>
                        <td>
                            <?php echo e($feedback['created_at']); ?>

                        </td>
						<td  align="center">
							<button class="btn btn-success fa fa-file" onclick="window.location='<?php echo e(action('FeedbackController@show',$feedback->feedback_id)); ?>'" title="<?php echo e(_('Show Detail')); ?>"></button>
						</td>
					</tr>
					<?php $counter++?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</table>
		</div>
	</div>
</div>

<div align="center">
	<ul class="pagination pagination-lg">
		<?php for($i = 1; $i <= ($feedbacks->total()+$feedbacks->perPage())/$feedbacks->perPage(); $i++): ?>
		<li><a href="feedback?page=<?php echo e($i); ?>"><?php echo e($i); ?></a></li>
		<?php endfor; ?>
	</ul>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>