<?php $__env->startSection('content'); ?>
<!-- START PAGE CONTENT -->
<div class="content">
	<!-- START JUMBOTRON -->
	<div class="jumbotron" data-pages="parallax">
		<div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
			<div class="inner">
				<!-- START BREADCRUMB -->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Tetapan CMS</a></li>
					<li class="breadcrumb-item active">Mail Template</li>
				</ol>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- END JUMBOTRON -->
	<!-- START CONTAINER FLUID -->
	<div class="container-fluid container-fixed-lg">
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		<div class="panel panel-default panel-admin">

			<?php $counter = 0; ?>
			<div class="panel-heading">
				<?php echo e(_('Mail Template Management')); ?>

			</div>
			<div class="panel-body">
				<div style="overflow:auto">
					<table class="table table-bordered" align="center" style="white-space: nowrap">
						<thead>
							<td>No.</td>
							<td><?php echo e(_("Name")); ?></td>
							<td><?php echo e(_("Description")); ?></td>
							<td colspan="1"><?php echo e(_("Edit")); ?></td>
						</thead>
						<?php $__currentLoopData = $templates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $template): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td><?php echo e(++$counter); ?></td>
							<td><?php echo e($template->name); ?></td>
							<td><?php echo e($template->description); ?></td>
							<td>
								<a class="btn btn-primary glyphicon glyphicon-edit" href="<?php echo e(action("MailTemplateController@edit",$template->id)); ?>"></a>
							</td>
						</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</table>
				</div>
			</div>
		</div>
		<!-- END PLACE PAGE CONTENT HERE -->
	</div>
	<!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('v2.admin.template.adminlayout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>