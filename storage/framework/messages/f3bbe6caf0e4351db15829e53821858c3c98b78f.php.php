<?php $__env->startSection('content'); ?>
<!-- START PAGE CONTENT -->
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">TETAPAN CMS</a></li>
                    <li class="breadcrumb-item active">Translation</li>
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="panel panel-default">

            <?php $counter = 0; ?>
            <div class="panel-heading">
                <?php echo e(_("Adding New Data Translation")); ?>

            </div>
            <div class="panel-body">
                <?php echo Form::open(["method"=>"POST","TranslationController@update"]); ?>

                <div>
                    <table class="table table-bordered" align="center" style="white-space: nowrap" id="language">
                        <thead>
                            <td><?php echo e(_("Language")); ?></td>
                            <td><?php echo e(_("Translation Key")); ?></td>
                            <td><?php echo e(_("Translated Term")); ?></td>
                            <td><?php echo e(_("Delete")); ?></td>
                        </thead>
                    </table>
                    <div class="row">
                        <div class="col-md-3 col-md-offset-2">
                            <button type="button" class="btn btn-primary btn-block" onclick="addTranslationRow()"><?php echo e(_("Add")); ?></button>
                        </div>
                        <div class="col-md-offset-2 col-md-3">
                            <button class="btn btn-success btn-block" type="submit"><?php echo e(_("Save")); ?></button>
                        </div>
                    </div>
                </div>
                <?php echo Form::close(); ?>

            </div>
        </div>
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('v2.admin.template.adminlayout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>