<?php $__env->startSection('content'); ?>

<div class="panel panel-default panel-admin" >
	<?php $counter = 0; ?>
	<div class="panel-heading">
		<?php echo e(_('Data Type Management')); ?>

	</div>
	<div class="panel-body" >
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td>No.</td>
					<td><?php echo e(_("Name")); ?></td>
					<td><?php echo e(_("Detail")); ?></td>
					<td><?php echo e(_("Availability")); ?></td>
					<td colspan="3"><?php echo e(_("Action")); ?></td>
				</thead>
				<?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td><?php echo e(++$counter); ?></td>
					<td><?php echo e($type->data_type_name); ?></td>
					<td><a href="<?php echo e(action('DataTypeController@show',$type->data_type_id)); ?>"><?php echo e(strlen($type->data_type_detail) > 30 ? substr($type->data_type_detail,0,27).'...':$type->data_type_detail); ?></a></td>
					<td align="center">
						<?php echo $__env->make('administrator.pages.datatype.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
					</td>
					<td>
						<button class="btn btn-success btn-block glyphicon glyphicon-edit" title="<?php echo e(_('Edit Data Type')); ?>" onClick="window.location ='<?php echo e(action('DataTypeController@edit',$type->data_type_id)); ?>'"></button>
					</td>
					<td>
						<?php if($type->available): ?>
							<button class="btn btn-danger btn-block glyphicon glyphicon-minus" title="<?php echo e(_('Deactive Data Type')); ?>" onClick="window.location = '<?php echo e(action('DataTypeController@deactivate',$type->data_type_id)); ?>'"></button>
						<?php else: ?>

							<button class="btn btn-primary btn-block glyphicon glyphicon-plus" title="<?php echo e(_('Activate Data Type')); ?>" onClick="window.location = '<?php echo e(action('DataTypeController@activate',$type->data_type_id)); ?>'"></button>
						<?php endif; ?>
					</td>
					<td>
						<?php echo Form::model($type,['method'=>'DELETE','action'=>['DataTypeController@destroy',$type->data_type_id],'id'=>$type->data_type_id]); ?><?php echo Form::close(); ?>

						<button title="<?php echo e(_('Delete Data Type')); ?>" type="submit" class="btn btn-danger btn-block glyphicon glyphicon-trash" onclick="deleteConfirmation('<?php echo e($type->data_type_id); ?>')"></button>
					</td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>