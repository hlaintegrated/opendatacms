<?php $__env->startSection('content'); ?>

	<?php $__currentLoopData = $feedbackgroups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type => $feedbacks): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<div class="panel panel-default" style="margin-top:20px">
		<div class="panel-heading">
			<?php echo e(_($type)); ?>

		</div>
		<?php $counter = 0; ?>
		<div class="panel-body" >
			<div style="overflow:auto">
				<table class="table table-bordered" align="center" style="white-space: nowrap">
					<thead>
						<td><?php echo e(_('No')); ?>.</td>
						<td><?php echo e(_("Sender")); ?></td>
						<td><?php echo e(_("Title")); ?></td>
						<td><?php echo e(_("Description")); ?></td>
						<td align="center"><?php echo e(_("Detail")); ?></td>
					</thead>
					<?php $__currentLoopData = $feedbacks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feedback): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>	
							<td><?php echo e(++$counter); ?></td>
							<td><?php echo e($feedback['name']); ?></td>
							<td><?php echo e(strlen($feedback['title']) > 15? substr($feedback['title'],0,12).'...':$feedback['title']); ?></td>
							<td><?php echo e(strlen($feedback['description']) > 30? substr($feedback['description'],0,27).'...':$feedback['description']); ?>

							</td>
							
							<td  align="center">
								<button class="btn btn-success fa fa-file" onclick="window.location='<?php echo e(action('FeedbackController@show',$feedback->feedback_id)); ?>'" title="<?php echo e(_('Show Detail')); ?>"></button>
							</td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</table>
			</div>
		</div>
	</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>