<?php $__env->startSection('content'); ?>
<!-- START PAGE CONTENT -->
<div class="content">
	<!-- START JUMBOTRON -->
	<div class="jumbotron" data-pages="parallax">
		<div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
			<div class="inner">
				<!-- START BREADCRUMB -->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Tetapan CMS</a></li>
					<li class="breadcrumb-item active">Tetapan Sistem</li>
				</ol>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- END JUMBOTRON -->
	<!-- START CONTAINER FLUID -->
	<div class="container-fluid container-fixed-lg">
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		<div class="panel panel-default">

			<?php $counter = 0; ?>
			<div class="panel-heading">
				<?php echo e(_("System Management")); ?>

			</div>
			<div class="panel-body">
				<div style="overflow:auto">
					<?php
					$index = 0; ?>
					<?php $__currentLoopData = $grouping; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $title=>$group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" href="#section<?php echo e($index); ?>"><?php echo e($title); ?></a>
								</h4>
							</div>
							<div class="panel-collapse collapse" id="section<?php echo e($index); ?>">
								<ul class="list-group">
									<?php $__currentLoopData = $group; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $element): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php if($setting->name == $element): ?>
									<li class="list-group-item">
										<div class="row">
											<div class="col-xs-4"><?php echo e($element); ?></div>
											<div class="col-xs-8">
												<?php if($setting->type== 'localized'): ?>
												<a class="btn btn-success" href="<?php echo e(action('SystemSettingController@localized',$setting->name)); ?>"><?php echo e(_('Change Values')); ?> </a>
												<?php echo Settings::getlocalized($setting->name,null,true); ?>

												<?php else: ?>
												<a class="btn btn-success" href="<?php echo e(action('SystemSettingController@other',$setting->name)); ?>"><?php echo e(_('Change Values')); ?> </a>
												<?php echo $setting->value; ?>

												<?php endif; ?>
											</div>
										</div>
									</li>
									<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</ul>
							</div>
						</div>
					</div>
					<?php $index++; ?>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		</div>
		<!-- END PLACE PAGE CONTENT HERE -->
	</div>
	<!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('v2.admin.template.adminlayout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>