<?php $__env->startSection('content'); ?>
<!-- START PAGE CONTENT -->
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <!-- <li class="breadcrumb-item active">Layout</li> -->
                </ol>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="panel panel-default">

            <?php $counter = 0; ?>
            <div class="panel-heading">
                <?php echo e(_("Banner Management")); ?>

            </div>
            <div class="panel-body">
                
                <div class='form-group'>
                    <div class="row">
                        <?php echo Form::label('home_banner',_('Home Page Banner'),['class' => 'control-label col-md-3']); ?>

                        <div class='col-md-7'>
                            <input type="file" name="home_banner" class="form-control" id="home_banner" accept="image/*">
                        </div>
                        <a class="btn btn-primary col-md-1" download="home-banner.png" href="<?php echo e(asset("images/home_banner")); ?>"><?php echo e(_("Download")); ?></a>
                    </div>
                </div>
                <div class='form-group'>
                    <div class="row">
                        <?php echo Form::label('home_bottom_banner',_('Home Page Bottom Banner'),['class' => 'control-label col-md-3']); ?>

                        <div class='col-md-7'>
                            <input type="file" name="home_bottom_banner" class="form-control" id="home_bottom_banner" accept="image/*">
                        </div>
                        <a class="btn btn-primary col-md-1" download="home-bottom-banner.png" href="<?php echo e(asset("images/home_bottom_banner")); ?>"><?php echo e(_("Download")); ?></a>
                    </div>
                </div>
                <div class='form-group'>
                    <div class="row">
                        <?php echo Form::label('page_banner',_('Landing Page Banner'),['class' => 'control-label col-md-3']); ?>

                        <div class='col-md-7'>
                            <input type="file" name="page_banner" class="form-control" id="page_banner" accept="image/*">
                        </div>
                        <a class="btn btn-primary col-md-1" download="landing-page-banner.png" href="<?php echo e(asset("images/page_banner")); ?>"><?php echo e(_("Download")); ?></a>
                    </div>
                </div>
                <div class='form-group'>
                    <div class="row">
                        <button class="btn btn-primary col-md-4 col-md-offset-4" id="save-banner"><?php echo e(_("Save")); ?></button>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('v2.admin.template.adminlayout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>