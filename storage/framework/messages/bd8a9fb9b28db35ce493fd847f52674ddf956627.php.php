<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<?php echo e(_("Edit Dataset Publisher")); ?>

	</div>
	<div class="panel-body">
<!-- Create Form for Source -->
<?php echo Form::model($source,['method'=>'PATCH','action'=>['DataSourceController@update',$source->data_source_id],'class'=>'form-horizontal','files' => 'true']); ?>

<?php echo $__env->make('administrator.pages.datasource.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="form-group">
	<div class="col-sm-4 col-sm-offset-4">
		<button type="submit" class="form-control btn btn-success btn-block"><?php echo e(_("Save")); ?></button>
	</div>
</div>

<?php echo Form::close(); ?>

	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>