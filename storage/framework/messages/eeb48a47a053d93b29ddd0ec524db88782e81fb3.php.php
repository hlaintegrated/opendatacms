<?php $__env->startSection('content'); ?>

<!-- Create Form for Event -->
<?php echo Form::model($event,['method'=>'PATCH','action'=>['EventController@update',$event->id],'class'=>'form-horizontal multi-language-form','data-language-input'=>'title,-content','files' => 'true']); ?>

<?php echo $__env->make('administrator.pages.event.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="form-group">
	<div class="col-sm-4 col-sm-offset-4">
		<button type="submit" class="form-control btn btn-success btn-block"><?php echo e(_("Save")); ?></button>
	</div>
</div>

<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>