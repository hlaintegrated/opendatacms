<?php if($type->available): ?>
	<i class="fa fa-check-circle fa-2x green" title="<?php echo e(_('Available')); ?>">
<?php else: ?>
	<i class="fa fa-times-circle fa-2x red" title="<?php echo e(_('Not Available')); ?>" >
<?php endif; ?>