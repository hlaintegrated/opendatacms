
<?php $__env->startSection('additionalstyle'); ?>
<link rel="stylesheet" href="<?php echo e(asset('assets/app/css/dragdropobject.css')); ?>" type="text/css" />
<?php $__env->stopSection(); ?>

<!-- Text Box for Link -->
<div class="form-group">
	<div class="col-md-1"></div>
	<!-- <?php echo Form::label('lnk',_('Link').'*',['class' => 'control-label col-md-2']); ?> -->
	<div class="col-md-8">
		<!-- <?php echo Form::text('link',null,['class'=>'form-control', 'id' => 'lnk','required'=>'required']); ?> -->
	</div>
	<div class="col-md-1"></div>
</div>

<div class="form-group">
	<div class="col-md-1"></div>
	<!-- <?php echo Form::label('target',_('Target').'*',['class' => 'control-label col-md-2']); ?> -->
	<div class="col-md-8">
		<!-- <?php echo Form::select('target', $links,null,['class'=>'form-control','id'=>'target','onchange'=>'targetchange()']); ?> -->
	</div>
	<div class="col-md-1"></div>
</div>

<!-- Text Box for Other Target -->
<div class='form-group' id="other">
   <div class='col-md-1'></div>
   <!-- <?php echo Form::label('other_target',_('Other Target').'*',['class' => 'control-label col-md-2']); ?> -->
   <div class='col-md-8'>
       <!-- <?php echo Form::text('other_target',null,['class'=>'form-control', 'id' => 'other_target']); ?> -->
   </div>
   <div class='col-md-1'></div>
</div>
<ul class="nav nav-tabs" id="language-selector">
    <?php $__currentLoopData = Settings::languages(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code => $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <li class="tab-container" id="<?php echo e($code); ?>-tab"><a class="language-tab" data-language="<?php echo e($code); ?>"><?php echo e($name); ?></a></li>
    <!-- <?php echo Form::hidden($code.'-title',null,['id'=>"$code".'-title']); ?> -->
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>

<!-- Text Box for Title -->
<div class='form-group'>
   <div class='col-md-1'></div>
   <!-- <?php echo Form::label('title',_('Title').'*',['class' => 'control-label col-md-2']); ?> -->
   <div class='col-md-8'>
       <!-- <?php echo Form::text('title',null,['class'=>'form-control', 'id' => 'title']); ?> -->
   </div>
   <div class='col-md-1'></div>
</div>

<?php $__env->startSection('additionalscript'); ?>
<script>
    function targetchange(){
        if ($("#target").val() == 'other')
        {
            $("#other").show('fast');
            $("#other_target").attr("required");
        }
        else
        {
            $("#other").hide('fast');
            $("#other_target").removeAttr("required");
        }
    }
    targetchange();
</script>
<?php $__env->stopSection(); ?>