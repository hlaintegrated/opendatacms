<?php $__env->startSection('content'); ?>

<?php echo Form::model($image,['method'=>'PATCH','action'=>['ImageController@update',$image->image_id],'class'=>'form-horizontal','files' => 'true']); ?>

<?php echo $__env->make('administrator.pages.image.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="form-group">
	<div class="col-sm-4 col-sm-offset-4">
		<button type="submit" class="form-control btn btn-success btn-block"><?php echo e(_("Save")); ?></button>
	</div>
</div>

<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>