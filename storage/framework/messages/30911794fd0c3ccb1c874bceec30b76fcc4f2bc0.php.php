<ul class="nav nav-tabs" id="language-selector">
    <?php $__currentLoopData = Settings::languages(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code => $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <li class="tab-container" id="<?php echo e($code); ?>-tab"><a class="language-tab" data-language="<?php echo e($code); ?>"><?php echo e($name); ?></a></li>
    <?php echo Form::hidden($code.'-title',null,['id'=>"$code".'-title']); ?>

    <?php echo Form::hidden($code.'-content',null,['id'=>"$code".'-content']); ?>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>

<!-- Text Box for title -->
<div class="form-group">
	<div class="col-md-1"></div>
	<?php echo Form::label('title',_('Title').'*',['class' => 'control-label col-md-2']); ?>

	<div class="col-md-8">
		<?php echo Form::text('title',null,['class'=>'form-control', 'id' => 'title','required'=>'required']); ?>

	</div>
	<div class="col-md-1"></div>
</div>
<!-- Text Area for content -->
<div class='form-group'>
   <div class='col-md-1'></div>
   <?php echo Form::label('content',_('Content').'*',['class' => 'control-label col-md-2']); ?>

   <div class='col-md-8'>
       <?php echo Form::textarea('content',null,['class'=>'form-control', 'id' => 'content','required'=>'required']); ?>

   </div>
   <div class='col-md-1'></div>
</div>
<div class='form-group'>
    <div class='col-md-1'></div>
    <?php echo Form::label('parent','Parent',['class' => 'control-label col-md-2']); ?>

    <div class='col-md-8'>
        <?php if(isset($articleitem)): ?>
          <?php echo Form::select('parent_id',\App\Model\ArticleItem::parent($articleitem->id),null,['class'=>'form-control', 'id' => 'status']); ?>

        <?php else: ?>
          <?php echo Form::select('parent_id',\App\Model\ArticleItem::parent(),null,['class'=>'form-control', 'id' => 'status']); ?>

        <?php endif; ?>  
    </div>
    <div class='col-md-1'></div>
</div>

<?php $__env->startSection('additionalscript'); ?>
    <script>



    </script>
    <script src="<?php echo e(asset('js/ckeditor4/ckeditor.js')); ?>"></script>
    <script>
        CKEDITOR.replace( 'content', {
            filebrowserUploadUrl: '<?php echo e(url('/administrator/send_uploads/?_token='. csrf_token() )); ?>'
        } );

        
            

            
            
            
            
            

            
            
        
    </script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('loader'); ?>
    <?php echo $__env->make('partials.loader', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>