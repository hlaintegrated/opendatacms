<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\data_source;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;

class RequestController extends Controller
{
    public function index(Request $request)
    {
        $ticket ['ticket']=Ticket::OrderBy('id','asc')->paginate(5);

        $sources['data'] = data_source::getDataSource();

        return view('portal.request',compact('ticket','sources'));
    }
    
    public function store(Request $request)
    {
        request()->validate([
            'subjek' => 'required',
            'name' => 'required',
            'pembekal' => 'required',
            'emel' => 'required',
            'description' => 'required',
            'notes' => 'required',
            'phone' => 'required',
        ]);

        $ticketid = substr(now()->year,2).now()->format('m');

        $data = $this->checkTicket($ticketid);

        $status = "Permintaan Baru";

        $ticket = array(
            'tick' => $data,
            'subjek' => $request->subjek,
            'status' => $status,
            'name' => $request->name,
            'pembekal' => $request->pembekal,
            'emel' => $request->emel,
            'description' => $request->description,
            'notes' => $request->notes,
            'phone' => $request->phone
        );
        Ticket::create($ticket);

        

        $emailto = data_source::select('data_source_email')->where('data_source_id','=', $request->pembekal)->first();
       
        Mail::send('email-confirm-dataset-request', ['details' => $ticket], function ($a) use ($request) {
           $a->to($request->emel)->subject('Permohonan Set Data Baru');
        });

        //Mail::send('email-dataset-request', ['details' => $ticket], function ($m) use ($emailto) {
        //$m->to($emailto->data_source_email)->subject('Permohonan Set Data Baru');
        //});

        return Redirect::to("portal/request");

    }

    public function checkTicket($ticketid){

        $tct = Ticket::where('tick', 'LIKE', $ticketid.'%')->max('tick');

        if($tct == null){
            $newtct = $ticketid."0001";
            return $newtct;
        }
        else{
            return ($tct + 1);
        }

    }
}