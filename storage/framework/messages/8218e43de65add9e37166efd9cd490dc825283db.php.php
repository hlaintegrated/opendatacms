
		<!-- Date for Publish Date -->
		<div class="form-group">
			<div class="col-md-1"></div>
			<?php echo Form::label('publish_date',_('Publish Date'),['class' => 'control-label col-md-2']); ?>

			<div class="col-md-8">
				<?php echo Form::input('date','publish_date',null,['class'=>'form-control', 'id' => 'publish_date']); ?>

			</div>
			<div class="col-md-1"></div>
		</div>

<ul class="nav nav-tabs" id="language-selector">
    <?php $__currentLoopData = Settings::languages(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code => $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <li class="tab-container" id="<?php echo e($code); ?>-tab"><a class="language-tab" data-language="<?php echo e($code); ?>"><?php echo e($name); ?></a></li>
    <?php echo Form::hidden($code.'-title',null,['id'=>"$code".'-title']); ?>

    <?php echo Form::hidden($code.'-content',null,['id'=>"$code".'-content']); ?>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>
		<!-- Text Box for Title -->
		<div class="form-group">
			<div class="col-md-1"></div>
			<?php echo Form::label('title',_('Title').'*',['class' => 'control-label col-md-2']); ?>

			<div class="col-md-8">
				<?php echo Form::text('title',null,['class'=>'form-control', 'id' => 'title']); ?>

			</div>
			<div class="col-md-1"></div>
		</div>

		<!-- Text Area for Description -->
		<div class="form-group">
			<div class="col-md-1"></div>
			<?php echo Form::label('content',_('Description').'*',['class' => 'control-label col-md-2']); ?>

			<div class="col-md-8">
				<?php echo Form::textarea('content',null,['class'=>'form-control', 'id' => 'content']); ?>


			</div>
			<div class="col-md-1"></div>
		</div>


		<?php $__env->startSection('loader'); ?>
			<?php echo $__env->make('partials.loader', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		<?php $__env->stopSection(); ?>

<?php $__env->startSection('additionalscript'); ?>
	<script src="<?php echo e(asset('js/ckeditor4/ckeditor.js')); ?>"></script>
	<script>
        CKEDITOR.replace( 'content', {
            filebrowserUploadUrl: '<?php echo e(url('/administrator/send_uploads/?_token='. csrf_token() )); ?>'
        } );
	</script>
<?php $__env->stopSection(); ?>