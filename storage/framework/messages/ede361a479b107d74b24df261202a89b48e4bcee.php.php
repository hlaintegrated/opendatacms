
<div class="row">
    <?php for($i = 0; $i < 2; $i++): ?>
    <div class="col-md-6 ">
        <div class="panel panel-2column home-widget">
            <div class="panel-heading">
                <input type="text" name="-title[]" class="form-control" placeholder="Title" value="<?php echo e(isset($content)?$content[$i]['title']:''); ?>">
            </div>
            <div class="panel-body">
                <div class="col-sm-12">
                    <div class="form-group">
                        <input type="text" name="-description[]" class="form-control" placeholder="<?php echo e(_('Description')); ?>" value="<?php echo e(isset($content)?$content[$i]['description']:''); ?>">
                    </div>
                    <div class="form-group">
                        <?php echo Form::select('-chart[]',Home::charts(),isset($content)?$content[$i]['chart']:'null',['class'=>'form-control']); ?>

                    </div>
                    <div class="form-group">
                        <input type="text" name="-labels[]" class="form-control labels" placeholder="<?php echo e(_('Labels(separated by comma)')); ?>" value="<?php echo e(isset($content)?$content[$i]['labels']:''); ?>">
                    </div>
                    <div class="form-group">
                        <input type="text" name="-values[]" class="form-control values" placeholder="<?php echo e(_('Values(separated by comma)')); ?>" value="<?php echo e(isset($content)?$content[$i]['values']:''); ?>">
                    </div>
                    <div class="form-group">
                        <button type="button" onclick="generatechart(<?php echo e($i); ?>)" class="btn btn-success btn-block"><?php echo e(_("Generate Chart From File")); ?></button>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row form-group">
                    <div class="col-md-6">
                        <input type='text' name='-linktitle[]' class='form-control' placeholder='Link Title' value='<?php echo e(isset($content)?$content[$i]['linktitle']:''); ?>'>
                    </div>
                    <div class="col-md-6">
                        <input type='text' name='-linkref[]' class='form-control' placeholder='Link Ref' value='<?php echo e(isset($content)?$content[$i]['linkref']:''); ?>'>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endfor; ?>
</div>