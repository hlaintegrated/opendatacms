<?php

namespace App\Http\Controllers\V2;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\TermTranslation;
use App\Models\Setting;
use Input;
use App\Http\Library\APIRequest;
use URL;
use LaravelGettext;

class TranslationController extends Controller
{
    public function index(){
        // $translations = TermTranslation::orderBy('term')->get();
        // $languages = json_decode(Setting::get('Languages'),true);

        $translations = array();
        $languages = array();
        return view('v2/admin/template/pages/translation/index',compact('translations','languages'));
    }

    public function update()
    {
        $inputs = Request::all();
        $lang_codes = $inputs['lang_codes'];
        $translation_keys = $inputs['translation_keys'];
        $translated_terms = $inputs['translated_terms'];
        for($i = 0; $i < count($translation_keys); $i++)
        {
            $input = ['lang_code'=>$lang_codes[$i],'term'=>$translation_keys[$i],'term_translation'=>$translated_terms[$i]];
            try {
                $translation = TermTranslation::where('lang_code', $lang_codes[$i])->where('term',$translation_keys[$i])->firstOrFail();
                $translation->update($input);
            }
            catch(\Exception $exception){
                TermTranslation::create($input);
            }
            APIRequest::updateTranslation($input);
        }
        return redirect(action("TranslationController@index"))->with('message',_("Translation Updated Successfully"));
    }

    public function switchLanguage($id = 'ms_MY')
    {
        LaravelGettext::setLocale($id);
        return redirect(URL::previous());
    }
}
