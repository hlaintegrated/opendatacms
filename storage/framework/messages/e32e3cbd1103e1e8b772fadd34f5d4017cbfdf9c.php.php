<!-- BEGIN SIDEBAR -->
<!-- BEGIN SIDEBPANEL-->
<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
    <div class="sidebar-overlay-slide from-top" id="appMenu">

    </div>
    <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
        <img src="../images/dtsa.png" alt="logo" class="brand" data-src="../images/dtsa.png" data-src-retina="../images/dtsa.png" height="26">
    </div>
    <!-- END SIDEBAR MENU HEADER-->
    <!-- START SIDEBAR MENU -->
    <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
            <li class="m-t-30">
                <a href="#" class="detailed">
                    <span class="title">Dashboard</span>
                    <span class="details">12 notifications</span>
                </a>
                <span class="icon-thumbnail "><i data-feather="shield"></i></span>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengguna</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="/administrator/user">Senarai Pengguna</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="/administrator/user/create">Cipta Pengguna Baru</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="https://osticket.data.gov.my/scp/">
                    <span class="title">Permohonan Data</span>
                </a>
                <span class="icon-thumbnail "><i data-feather="list"></i></span>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Laporan</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="http://www.data.gov.my/data/ms_MY/report">Broken Links & Openness</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="http://www.data.gov.my/data/ms_MY/dsw_reports">Dataset Approval Workflow</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="http://www.data.gov.my/data/ms_MY/fq_reports">Update Frequency</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Tetapan Menu</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="/administrator/menu">Senarai Menu</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="/administrator/menu/create">Cipta Menu Baru</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="/administrator/menu/section/header">Menu Header</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="/administrator/menu/section/footer">Menu Footer</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Tetapan CMS</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="/administrator/settings">Tetapan Sistem</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="/administrator/translation">Translation</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="/administrator/settings/banner">Banner</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="/administrator/mailtemplate">Mail Template</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengurusan Kluster</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="/administrator/clusters">Icon Management</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="/administrator/clusters/position">Position Management</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengurusan Hebahan</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">Senarai Hebahan</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="#">Cipta Hebahan Baru</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengurusan Laman</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">Senarai Laman</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="#">Cipta Laman Baru</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="#">
                    <span class="title">Pengurusan Imej</span>
                </a>
                <span class="icon-thumbnail "><i data-feather="list"></i></span>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengurusan Soalan Lazim</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">Senarai Soalan Lazim</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="#">Cipta Soalan Lazim Baru</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Article Management</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">Article List</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="#">Create New Article</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengurusan Penyumbang Data</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">Senarai Penyumbang Set Data</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="#">Cipta Penyumbang Set Data</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="#">Reload Semula Penyumbang Set Data</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Pengurusan Jenis Data</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">Senarai Jenis Data</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                    <li class="">
                        <a href="#">Cipta Jenis Data Baru</a>
                        <span class="icon-thumbnail">sp</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="#">
                    <span class="title">Pengurusan Set Data Pilihan</span>
                </a>
                <span class="icon-thumbnail "><i data-feather="list"></i></span>
            </li>
            <li class="">
                <a href="#">
                    <span class="title">Audit Trail</span>
                </a>
                <span class="icon-thumbnail "><i data-feather="list"></i></span>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->
</nav>
<!-- END SIDEBAR -->
<!-- END SIDEBAR -->