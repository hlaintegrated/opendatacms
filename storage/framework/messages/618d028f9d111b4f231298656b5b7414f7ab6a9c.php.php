
<div id="wysiwyg">
    <div class="form-group" id="wysiwyg-english">
        <div class="col-md-1"></div>
        <?php echo Form::label('wysiwygcontent','English',['class' => 'control-label col-md-2']); ?>

        <div class="col-md-8">
            <?php echo Form::textarea('wysiwygcontent_en',null,['class'=>'form-control', 'id' => 'wysiwygcontent_en','rows'=>'10','cols'=>'80']); ?>

            <script>
                CKEDITOR.replace( 'wysiwygcontent_en' )
            </script>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="form-group" id="wysiwyg-bahasa">
        <div class="col-md-1"></div>
        <?php echo Form::label('wysiwygcontent','Bahasa',['class' => 'control-label col-md-2']); ?>

        <div class="col-md-8">
            <?php echo Form::textarea('wysiwygcontent_ms',null,['class'=>'form-control', 'id' => 'wysiwygcontent_ms','rows'=>'10','cols'=>'80']); ?>

            <script>
                CKEDITOR.replace( 'wysiwygcontent_ms' )
            </script>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>