<?php $__env->startSection('script'); ?>
<script>
	homeedit(<?php echo e($home->id); ?>);
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-default ">
	<div class="panel-heading ">
		Edit home <?php echo e($home->title); ?>

	</div>
	<div class="panel-body ">
		<?php echo Form::model($home,['id'=>'homeform','method' => 'PATCH','action'=>['HomeController@update',$home->id],'class'=>'form-horizontal','files'=>true]); ?>


			<?php echo $__env->make('administrator.pages.home.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-success"><?php echo e(_("Save")); ?></button>
			</div>
		</div>
		<?php echo Form::close(); ?>

	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>