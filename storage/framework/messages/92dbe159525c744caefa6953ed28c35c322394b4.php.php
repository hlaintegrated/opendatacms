<!-- Text Box for Link -->
<div class="form-group">
	<div class="col-md-1"></div>
	<?php echo Form::label('lnk',_('Link').'*',['class' => 'control-label col-md-2']); ?>

	<div class="col-md-8">
		<?php echo Form::text('link',null,['class'=>'form-control', 'id' => 'lnk','required'=>'required']); ?>

	</div>
	<div class="col-md-1"></div>
</div>

<!-- Text Area for Background -->
<div class='form-group' id="background-container">
    <div class='col-md-1'></div>
        <?php echo Form::label('background',_('Background'),['class' => 'control-label col-md-2']); ?>

        <div class='col-md-4'>
        <?php echo Form::file('background',null,['class'=>'form-control', 'id' => 'background','accept'=>'.png,.bpm,.jpg']); ?>

    </div>
    <div class="col-md-4">
        <?php echo e(_('Remove Background')); ?> <input type="checkbox" name="clear">
    </div>
    <div class='col-md-1'></div>
</div>

<ul class="nav nav-tabs" id="language-selector">
    <?php $__currentLoopData = Settings::languages(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code => $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <li class="tab-container" id="<?php echo e($code); ?>-tab"><a class="language-tab" data-language="<?php echo e($code); ?>"><?php echo e($name); ?></a></li>
    <?php echo Form::hidden($code.'-title',null,['id'=>"$code".'-title']); ?>

    <?php echo Form::hidden($code.'-content',null,['id'=>"$code".'-content']); ?>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>
<!-- Text Box for Title -->
<div class="form-group" id="target">
	<div class="col-md-1"></div>
	<?php echo Form::label('title',_('Title').'*',['class' => 'control-label col-md-2']); ?>

	<div class="col-md-8">
		<?php echo Form::text('title',null,['class'=>'form-control', 'id' => 'title','required'=>'required']); ?>

	</div>
	<div class="col-md-1"></div>
</div>

<!-- Text Area for Content -->
<div>
    <div class="form-group">
        <div class="col-md-1"></div>
        <?php echo Form::label('content',_('Content'),['class' => 'control-label col-md-2']); ?>

        <div class="col-md-8">
            <?php echo Form::textarea('content',null,['class'=>'form-control', 'id' => 'content','rows'=>'10','cols'=>'80']); ?>


        </div>
        <div class="col-md-1"></div>
    </div>
</div>

<?php $__env->startSection('additionalscript'); ?>
    <script src="<?php echo e(asset('js/ckeditor/ckeditor.js')); ?>"></script>
    <script>
        CKEDITOR.replace( 'content' )
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('loader'); ?>
    <?php echo $__env->make('partials.loader', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>