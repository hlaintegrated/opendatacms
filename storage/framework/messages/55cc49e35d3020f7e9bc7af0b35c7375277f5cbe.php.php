<div class="row">
	<div class="form-group col-md-10">
		<div class="form-group">
			<div class="col-md-2">
				<label for="menu" class="control-label col-md-2"><?php echo e(_('Menu')); ?></label>
			</div>
			<div class="col-md-10">
				<select class="form-control" name="menu" id="portlet-add">
					<option id="noselect" value="noselect"><?php echo e(_("Select Available Menu")); ?></option>
					<?php $__currentLoopData = $selects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $select): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<option id="<?php echo e($select->link); ?>" value="<?php echo e($select->link); ?>,<?php echo e($select->title); ?>"><?php echo e($select->link); ?> - <?php echo e($select->title); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<button class="btn btn-primary" type="button" onclick="addMenu()"><?php echo e(_("Add")); ?></button>
	</div>
</div>

<div class="row panel panel-default">

	<div class="panel-heading">
		<?php echo e(_($page." Menu Position Management")); ?>

	</div>
	<div class="panel-body" >
		<div class="drag-drop-row" id="portlet-row" style="padding:0px">
		<?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div class="portlet panel panel-default" id="<?php echo e($menu->link); ?>">
                <input type="hidden" name="links[]" value="<?php echo e($menu->link); ?>">
				<div class="portlet-header panel-heading"><?php echo e($menu->title); ?></div>
				<div class="portlet-content panel-body">Link: <?php echo e($menu->link); ?>

					<button type="button" title="<?php echo e(_('Remove Menu')); ?>"  onClick="removeMenu('<?php echo e($menu->link); ?>,<?php echo e($menu->link); ?>,<?php echo e($menu->title); ?>')" class="btn btn-danger glyphicon glyphicon-minus" style="float:right"></button>
				</div>
			</div>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
	</div>
</div>