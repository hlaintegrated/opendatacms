<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<?php echo e(_('Edit FAQ')); ?> <?php echo e($faq->link); ?>

	</div>
	<div class="panel-body">
		<?php echo Form::model($faq,['method' => 'PATCH','action'=>['FAQController@update',$faq->link],'class'=>'form-horizontal multi-language-form','data-language-input'=>'name','id'=>'faq-multi-language-form']); ?>


			<?php echo $__env->make('administrator.pages.faq.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-4">
				<button type="submit" class="form-control btn btn-success"><?php echo e(_("Save")); ?></button>
			</div>
		</div>
		<?php echo Form::close(); ?>

	</div>
</div>
<div class="panel panel-default">
    <div class="panel-heading ">
        <div class="row">
            <div class="col-sm-11">
            <?php echo e(_('FAQ Item Management')); ?>

            </div>
            <div class="col-sm-1">
                <a class="btn btn-primary btn-block" href='<?php echo e(action("FAQItemController@create",$id)); ?>'><i class="glyphicon glyphicon-plus"></i></a>
            </div>
        </div>
    </div>
    
    <?php echo Form::open(['method' => 'POST','action'=>['FAQController@saveposition',$id],'class'=>'form-horizontal']); ?>

    <div class="panel-body">
        <div class="drag-drop-row" id="portlet-row" style="padding:0px">
        <?php $__currentLoopData = $faqitems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $faqitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="portlet panel panel-default pointer">
                <div class="portlet-header panel-heading pointer">
                    <input type="hidden" name="ids[]" value="<?php echo e($faqitem->id); ?>">
                    <div class="row">
                        <div class="col-sm-10"><?php echo e($faqitem->question); ?></div>
                        <div class="col-sm-1"><a class="btn btn-success btn-block" href="<?php echo e(action('FAQItemController@edit',$faqitem->id)); ?>"><span class="fa fa-edit"></span></a></div>
                        <div class="col-sm-1"><button type="button" class="btn btn-danger btn-block " onclick="$(this).closest('.portlet').remove()"><span class="fa fa-trash"></span></button></div>
                    </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    <div class="panel-footer">
		<div class="form-group row">
            <div class="col-sm-4 col-sm-offset-4">
				<button type="submit" id="save-position" class="form-control btn btn-success"><?php echo e(_("Save")); ?></button>
			</div>
		</div>
    </div>
    <?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('assets/app/js/dragdropobject.js')); ?>"></script>
<script src="<?php echo e(asset('assets/jquery/jquery-ui.min.js')); ?>"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>