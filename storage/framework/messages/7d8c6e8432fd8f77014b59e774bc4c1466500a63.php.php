<!-- Text Box for Link -->
<div class="form-group">
	<div class="col-md-1"></div>
	<?php echo Form::label('lnk',_('Link').'*',['class' => 'control-label col-md-2']); ?>

	<div class="col-md-8">
		<?php echo Form::text('link',null,['class'=>'form-control', 'id' => 'lnk','required'=>'required']); ?>

	</div>
	<div class="col-md-1"></div>
</div>

<!-- Text Area for Status -->
<div class='form-group'>
    <div class='col-md-1'></div>
    <?php echo Form::label('status','Status',['class' => 'control-label col-md-2']); ?>

    <div class='col-md-8'>
        <?php echo Form::select('status',FAQ::statuses(),null,['class'=>'form-control', 'id' => 'status']); ?>

    </div>
    <div class='col-md-1'></div>
</div>
<!-- Text Area for Status -->
<div class='form-group'>
    <div class='col-md-1'></div>
    <?php echo Form::label('list_style','List Style',['class' => 'control-label col-md-2']); ?>

    <div class='col-md-8'>
        <?php echo Form::select('list_style',FAQ::list_styles(),null,['class'=>'form-control', 'id' => 'list_style']); ?>

    </div>
    <div class='col-md-1'></div>
</div>


<ul class="nav nav-tabs" id="language-selector">
    <?php $__currentLoopData = Settings::languages(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code => $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <li class="tab-container" id="<?php echo e($code); ?>-tab"><a class="language-tab" data-language="<?php echo e($code); ?>"><?php echo e($name); ?></a></li>
    <?php echo Form::hidden($code.'-name',null,['id'=>"$code".'-name']); ?>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>

<!-- Text Area for Name -->
<div class='form-group'>
   <div class='col-md-1'></div>
   <?php echo Form::label('name',_('Name').'*',['class' => 'control-label col-md-2']); ?>

   <div class='col-md-8'>
       <?php echo Form::text('name',null,['class'=>'form-control', 'id' => 'name','required'=>'required']); ?>

   </div>
   <div class='col-md-1'></div>
</div>
