<?php $__env->startSection('content'); ?>
<!-- START PAGE CONTENT -->
<div class="content">
	<!-- START JUMBOTRON -->
	<div class="jumbotron" data-pages="parallax">
		<div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
			<div class="inner">
				<!-- START BREADCRUMB -->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Tetapan Menu</a></li>
					<li class="breadcrumb-item active">Senarai Menu</li>
				</ol>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- END JUMBOTRON -->
	<!-- START CONTAINER FLUID -->
	<div class="container-fluid container-fixed-lg">
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		<div class="panel panel-default">

			<?php $counter = 0; ?>
			<div class="panel-heading">
				<?php echo e(_("Menu Management")); ?>

			</div>
			<div class="panel-body">
				<div style="overflow:auto">
					<table class="table table-bordered" align="center" style="white-space: nowrap">
						<thead>
							<td><?php echo e(_('No')); ?>.</td>
							<td><?php echo e(_("Link")); ?></td>
							<td><?php echo e(_("Title")); ?></td>
							<td><?php echo e(_("Target")); ?></td>
							<td><?php echo e(_("Created At")); ?></td>
							<td><?php echo e(_("Updated At")); ?></td>
							<td colspan="2"><?php echo e(_("Action")); ?></td>
						</thead>
						<?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td><?php echo e(++$counter); ?></td>
							<td><?php echo e($menu->link); ?></td>
							<td><?php echo e($menu->title); ?></td>
							<td><?php echo e($menu->target); ?></td>
							<td><?php echo e($menu->created_at); ?></td>
							<td><?php echo e($menu->updated_at); ?></td>
							<td>
								<a title="<?php echo e(_('Edit Menu')); ?>" class="btn btn-success btn-block glyphicon glyphicon-edit" href="<?php echo e(action('MenuController@edit',$menu->link)); ?>"></a>
							</td>
							<td>
								<?php echo Form::model($menu,['method'=>'DELETE','action'=>['MenuController@destroy',$menu->link],'id'=>$menu->link]); ?><?php echo Form::close(); ?>

								<button title="<?php echo e(_('Delete Menu')); ?>" type="submit" class="btn btn-danger btn-block glyphicon glyphicon-minus" onclick="deleteConfirmation('<?php echo e($menu->link); ?>')"></button>
							</td>
						</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</table>
				</div>
			</div>
		</div>
		<!-- END PLACE PAGE CONTENT HERE -->
	</div>
	<!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('v2.admin.template.adminlayout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>