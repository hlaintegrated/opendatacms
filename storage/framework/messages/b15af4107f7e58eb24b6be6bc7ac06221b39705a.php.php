<!DOCTYPE html>
<html lang="en">

<head>

<?php echo $__env->make('v2.admin.template.partials.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</head>

<body class="fixed-header menu-pin">

    <!-- Sidebar -->
    <?php echo $__env->make('v2.admin.template.partials.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- End of Sidebar -->

    <!-- START PAGE-CONTAINER -->
    <div class="page-container">
    
      <!-- Topbar -->
      <?php echo $__env->make('v2.admin.template.partials.topbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      <!-- End of Topbar -->

      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper">
        <!-- Begin Page Content -->
        <?php echo $__env->yieldContent('content'); ?>
        <!-- /.container-fluid -->

        <!-- Footer -->
        <?php echo $__env->make('v2.admin.template.partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- End of Footer -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->

    </div>
    <!-- END PAGE CONTAINER -->

    <?php echo $__env->make('v2.admin.template.partials.footer-scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</body>

</html>
