<?php $__env->startSection('content'); ?>
<div class="panel panel-default" style="margin-top:20px">
	<div class="panel-heading">
		<?php echo e(_('Feedback Detail')); ?>

	</div>
	<div class="panel-body">
		<table class="table-striped table-hover" width="100%">
		<tr style="height: 30px">
			<td style="width: 20%"><?php echo e(_('Sender')); ?></td>
			<td><?php echo e($feedback->name); ?></td>
		</tr>
		
		<tr style="height: 30px">
			<td style="width: 20%"><?php echo e(_('Email')); ?></td>
			<td><?php echo e($feedback->email); ?></td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%"><?php echo e(_('Phone')); ?></td>
			<td><?php echo e($feedback->phone); ?></td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%"><?php echo e(_('Type')); ?></td>
			<td><?php echo e($feedback->type); ?></td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%"><?php echo e(_('Title')); ?></td>
			<td><?php echo e($feedback->title); ?></td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%"><?php echo e(_('Description')); ?></td>
			<td><?php echo e($feedback->description); ?></td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%"><?php echo e(_('Remarks')); ?></td>
			<td><?php echo e($feedback->notes); ?></td>
		</tr>

		<tr style="height: 30px">
			<td style="width: 20%"><?php echo e(_('Status')); ?></td>
			<td>
                <?php if($feedback->status == 'unread'): ?>
	               <span class="fa fa-exclamation-circle yellow" title="<?php echo e(_('Unread')); ?>"></span><?php echo e(_('Unread')); ?> <a class="btn btn-success" href="<?php echo e(action('FeedbackController@read',$feedback->feedback_id)); ?>"><?php echo e(_('Mark Read')); ?></a>
                <?php else: ?>
                    <span class="fa fa-bookmark green"></span> <?php echo e(_('Read')); ?>

                <?php endif; ?>
			</td>
		</tr>
		</table>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>