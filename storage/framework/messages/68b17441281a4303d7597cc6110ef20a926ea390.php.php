<?php $__env->startSection('additionalscript'); ?>
<script>
var chartgeneratorurl = "<?php echo e(url('administrator/home/chartgenerator')); ?>";
$(document).ready()
{
	var types=[
		<?php $first = true; ?>
		<?php $__currentLoopData = Home::types(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php
				if ($first)
				{
					$first = false;
				}
				else
				{
					echo ',';
				}
				?>'<?php echo e($index); ?>'<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	];
	var select=document.getElementById('types');
	
    function check_ckeditor(value)
    {
        var num = 0;
        if (value == '2columnwysiwyg')
            num = 2;
        if (value == '3columnwysiwyg')
            num = 3;
        for (var i = 0; i < num; i++)
        {
            CKEDITOR.replace('wysiwyg'+i);
        }
    }
    
	function hometypechange()
	{
		var value = select.value;
		var target = "<?php echo e(url('administrator/home/card')); ?>/"+value;
		$.get(target,function(data)
		{
			$("#ajaxform").html(data);
            check_ckeditor(value);
		});		
	}
	
	function homeedit($id)
	{
		var target = "<?php echo e(url('administrator/home/card')); ?>/"+$id+"/edit";
		$.get(target,function(data)
		{
			$("#ajaxform").html(data);
			onPageLoad();
            check_ckeditor(id);
		});
	}
}
</script>
<?php $__env->stopSection(); ?>
<!-- Text Box for Title -->
<div class="form-group">
	<div class="col-md-1"></div>
	<?php echo Form::label('title',_("Title").'*',['class' => 'control-label col-md-2']); ?>

	<div class="col-md-8">
		<?php echo Form::text('title',null,['class'=>'form-control', 'id' => 'title','required'=>'required']); ?>

		<input type="checkbox" name="show_title" <?php if(isset($home) && $home->title_show): ?>checked <?php endif; ?> > <label class="form-label">Show</label>
	</div>
	<div class="col-md-1"></div>
</div>

<!-- Text Box for Description -->
<div class="form-group">
	<div class="col-md-1"></div>
	<?php echo Form::label('description',_("Description").'*',['class' => 'control-label col-md-2']); ?>

	<div class="col-md-8">
		<?php echo Form::text('description',null,['class'=>'form-control', 'id' => 'description','required'=>'required']); ?>

	</div>
	<div class="col-md-1"></div>
</div>

<div class="form-group">
	<div class="col-md-1"></div>
	<?php echo Form::label('type',_("Type"),['class' => 'control-label col-md-2']); ?>

	<div class="col-md-8">
		<?php echo Form::select('type', Home::types(),null,['class'=>'form-control','id'=>'types','onchange'=>'hometypechange()']); ?>

	</div>
	<div class="col-md-1"></div>
</div>
<div class="form-group" id="ajaxform">
</div>

<?php echo $__env->make('administrator.layout.partials.chartprocessor', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->startSection('loader'); ?>
	<?php echo $__env->make('partials.loader', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>