<?php $__env->startSection('additionalstyle'); ?>
<link rel="stylesheet" href="<?php echo e(asset('assets/app/css/dragdropobject.css')); ?>" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('additionalscript'); ?>
<script src="<?php echo e(asset('assets/app/js/dragdropobject.js')); ?>"></script>
<script src="<?php echo e(asset('assets/jquery/jquery-ui.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <?php echo Form::open(['method'=>'POST','action'=>['MenuController@headersave']]); ?>

    <?php echo $__env->make('administrator.pages.menu.positionform', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="col-sm-4 col-sm-offset-4">
		    <button id="save-header" class="btn btn-success btn-block"><?php echo e(_("Save")); ?></button>
        </div>
    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>