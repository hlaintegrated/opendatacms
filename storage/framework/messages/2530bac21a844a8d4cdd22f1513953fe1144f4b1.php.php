<?php $__env->startSection('content'); ?>
<div class="panel panel-default panel-admin">
	<?php $counter = 0; ?>
	<div class="panel-heading">
		<?php echo e(_('Dataset Publisher Management')); ?>

	</div>
	<div class="panel-body" >
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td><?php echo e(_('No')); ?>.</td>
					<td><?php echo e(_("Name")); ?></td>
					<td><?php echo e(_("Custom")); ?></td>

					<td colspan="2"><?php echo e(_("Action")); ?></td>
				</thead>
				<?php $__currentLoopData = $sources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $source): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td><?php echo e(++$counter); ?></td>
					<td><?php echo e(T::_($source['name'])); ?></td>
					<td align="center">
						<?php if($source['custom']): ?>
						<i title="<?php echo e(_('Not a CKAN Default Dataset publisher')); ?>" class="fa fa-check-circle fa-2x green"></i>
						<?php else: ?>
						<i title="<?php echo e(_('CKAN Default Dataset publisher')); ?>" class="fa fa-times-circle fa-2x red"></i>
						<?php endif; ?>
					</td>

					<td>
						<button title="<?php echo e(_('Edit Dataset publisher')); ?>" class="btn btn-success glyphicon glyphicon-edit btn-block" onClick="window.location='<?php echo e(action('DataSourceController@edit',$source['id'])); ?>'"></button>
					</td>
					<td>
						<div>
							<?php echo Form::open(['method'=>'DELETE','action'=>['DataSourceController@destroy',$source['id']],'id'=>$source['id']]); ?>

							<?php echo Form::close(); ?>

							<button type="submit" title="<?php echo e(_('Delete Dataset publisher')); ?>" class="btn btn-danger glyphicon glyphicon-minus btn-block glyph-color" onclick="deleteConfirmation('<?php echo e($source['id']); ?>')"></button>
						</div>
					</td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>