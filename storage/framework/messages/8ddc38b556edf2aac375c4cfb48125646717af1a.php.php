<?php $__env->startSection('content'); ?>

<div class="panel panel-default">
	
	<?php $counter = 0; ?>
	<div class="panel-heading">
		<?php echo e(_("Article Management")); ?>

	</div>
	<div class="panel-body" >
		<div style="overflow:auto">
			<table class="table table-bordered" align="center" style="white-space: nowrap">
				<thead>
					<td><?php echo e(_('No')); ?>.</td>
					<td><?php echo e(_("Name")); ?></td>
					<td><?php echo e(_("Link")); ?></td>
					<td><?php echo e(_("Status")); ?></td>
					<td><?php echo e(_("Created At")); ?></td>
					<td><?php echo e(_("Updated At")); ?></td>
					<td colspan="2"><?php echo e(_("Action")); ?></td>
				</thead>
				<?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td><?php echo e(++$counter); ?></td>
					<td><?php echo e($article->name); ?></td>
					<td><?php echo e($article->link); ?></td>
					<td><?php echo e($article->status); ?></td>
					<td><?php echo e($article->created_at); ?></td>
					<td><?php echo e($article->updated_at); ?></td>
					<td>
						<button title="<?php echo e(_('Edit Page')); ?>" class="btn btn-success btn-block glyphicon glyphicon-edit" onClick="window.location ='<?php echo e(action('ArticleController@edit',$article->link)); ?>'"></button>
					</td>
					<td>
						<?php echo Form::model($article,['method'=>'DELETE','action'=>['ArticleController@destroy',$article->link],'id'=>$article->link]); ?><?php echo Form::close(); ?>

						<button title="<?php echo e(_('Delete Page')); ?>" type="submit" class="btn btn-danger btn-block glyphicon glyphicon-minus" onclick="deleteConfirmation('<?php echo e($article->link); ?>')"></button>
					</td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</table>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>