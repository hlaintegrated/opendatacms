<?php if($dataset_request->status == 'pending'): ?>
	<i class="fa fa-exclamation-circle yellow fa-2x" title="<?php echo e(_('Pending')); ?>">
<?php elseif($dataset_request->status == 'completed'): ?>
	<i class="fa fa-check-circle green fa-2x" title="<?php echo e(_('Completed')); ?>">
<?php elseif($dataset_request->status == 'in_progress'): ?>
	<i class="fa fa-hourglass-2 fa-2x green" title="<?php echo e(_('In Progress')); ?>">
<?php else: ?>
	<i class="fa fa-times-circle fa-2x red" title="<?php echo e(_('Rejected')); ?>">	
<?php endif; ?>