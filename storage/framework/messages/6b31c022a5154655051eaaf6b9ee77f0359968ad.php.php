<!-- Menu -->
<nav class="navbar navbar-light navbar-expand-lg py-3" style="background-color: #809fff;">
    <div class="container">
        <span class="navbar-brand mb-0">
            <img src="http://www.data.gov.my/images/dtsa-logo.png" style="width: 240px;" />
        </span>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link font-weight-bold" href="#">Data <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="#">Permohonan Set Data</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="#">Terma Penggunaan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="#">Muat Turun</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="#">Log Masuk</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Carousel -->
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="https://www.mampu.gov.my/images/slide-banner/banner_tahun_baru-01_1.jpg" class="d-block w-100" alt="..." style="object-fit: fill !important;">
    </div>
    <div class="carousel-item">
      <img src="https://www.mampu.gov.my/images/slide-banner/wsis_web_banner_TAHNIAH-01.jpg" class="d-block w-100" alt="..." style="object-fit: fill !important;">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<!-- Kluster -->

<div class="py-5">
    <div class="container">
        <div class="row d-flex justify-content-between my-4">
            <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                <div class="card-body">
                    <i class="material-icons">how_to_vote</i>
                    <br/>Pilihanraya
                </div>
            </div>
            <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                <div class="card-body">
                    <i class="material-icons">money</i>
                    <br/>Bajet
                </div>
            </div>
            <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                <div class="card-body">
                    <i class="material-icons">people</i>
                    <br/>Bancian
                </div>
            </div>
            <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                <div class="card-body">
                    <i class="material-icons">gavel</i>
                    <br/>Perundangan
                </div>
            </div>
            <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                <div class="card-body">
                    <i class="material-icons">description</i>
                    <br/>Kontrak Awam
                </div>
            </div>
            <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                <div class="card-body" style="word-wrap: break-word;">
                    <i class="material-icons">attach_money</i>
                    <br/>Perbelanjaan Kerajaan
                </div>
            </div>
        </div>
        <div class="collapse" id="collapseKluster">
            <div class="row d-flex justify-content-between my-4">
                <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                    <div class="card-body">
                        <i class="material-icons">terrain</i>
                        <br/>Pemilikan Tanah
                    </div>
                </div>
                <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                    <div class="card-body">
                        <i class="material-icons">bar_chart</i>
                        <br/>Statistik Kebangsaan
                    </div>
                </div>
                <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                    <div class="card-body">
                        <i class="material-icons">sports_kabaddi</i>
                        <br/>Jenayah
                    </div>
                </div>
                <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                    <div class="card-body">
                        <i class="material-icons">local_hospital</i>
                        <br/>Kesihatan
                    </div>
                </div>
                <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                    <div class="card-body">
                        <i class="material-icons">business</i>
                        <br/>Pendaftaran Syarikat
                    </div>
                </div>
                <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                    <div class="card-body">
                        <i class="material-icons">map</i>
                        <br/>Pemetaan
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-between my-4">
                <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                    <div class="card-body">
                        <i class="material-icons">school</i>
                        <br/>Pendidikan
                    </div>
                </div>
                <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                    <div class="card-body">
                        <i class="material-icons">eco</i>
                        <br/>Pertanian
                    </div>
                </div>
                <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                    <div class="card-body">
                        <i class="material-icons">emoji_nature</i>
                        <br/>Alam Sekitar
                    </div>
                </div>
                <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                    <div class="card-body">
                        <i class="material-icons">public</i>
                        <br/>Perdagangan Antarabangsa
                    </div>
                </div>
                <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                    <div class="card-body">
                        <i class="material-icons">directions_transit</i>
                        <br/>Pengankutan
                    </div>
                </div>
                <div class="card text-center hvr-bounce-to-bottom" style="width: 140px;">
                    <div class="card-body">
                        <i class="material-icons">extension</i>
                        <br/>Lain-lain
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-center hvr-icon-hang">
            <div data-toggle="collapse" href="#collapseKluster">
                <i class="material-icons hvr-icon" style="font-size: 35px; cursor: pointer;">import_export</i>
            </div>
        </div>
    </div>
</div>


<!-- Search Bar -->
<div class="pt-5 pb-4 bg-info">
    <div class="container">
        <div class="d-flex justify-content-center">
            <form>
                <div class="form-group">
                    <input type="text" class="form-control form-control-lg" placeholder="Search" aria-label="Search" style="width: 700px;">
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Tab -->

<div class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-8">
                <div class="list-group list-group-horizontal" id="list-tab" role="tablist">
                    <a class="list-group-item list-group-item-action active list-group-item-secondary" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Aktiviti</a>
                    <a class="list-group-item list-group-item-action list-group-item-secondary" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Set Data Popular</a>
                    <a class="list-group-item list-group-item-action list-group-item-secondary" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Set Data Terkini</a>
                    <a class="list-group-item list-group-item-action list-group-item-secondary" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">Set Data Kementerian</a>
                    <a class="list-group-item list-group-item-action list-group-item-secondary" id="list-api-list" data-toggle="list" href="#list-api" role="tab" aria-controls="settings">Set Data API</a>
                </div>
                <div class="tab-content" id="nav-tabContent" style="border: 1px solid rgba(0,0,0,.125);">
                    <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item py-3">
                                Mesyuarat Jawatankuasa Penyelarasan Data Terbuka Bilangan 2/2019
                                <div class="text-muted">09-Dec-2019</div>
                            </li>
                            <li class="list-group-item py-3">
                                Keputusan Hackathon Data Terbuka 2019
                                <div class="text-muted">14-Oct-2019</div>
                            </li>
                            <li class="list-group-item py-3">
                                HACKATHON DATA TERBUKA 2019
                                <div class="text-muted">27-Sep-2019</div>
                            </li>
                            <li class="list-group-item py-3">
                                PERTANDINGAN HACKATHON DATA TERBUKA 2019
                                <div class="text-muted">08-Aug-2019</div>
                            </li>
                            <li class="list-group-item py-3">
                                Sabah Data Fest 2019
                                <div class="text-muted">10-Jul-2019</div>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Taburan graduan ijazah pertama yang melanjutkan pengajian mengikut penaja pengajian, jenis institusi pendidikan tinggi (IPT) dan jantina
                            </li>
                            <li class="list-group-item">Bilangan Keahlian dan Pengguna Perpustakaan Agensi Remote Sensing Malaysia</li>
                            <li class="list-group-item">Preferential Certificate of Origin by Issuing Branch</li>
                            <li class="list-group-item">Pendaftar Baru Siswazah Mengikut Negeri, Jantina Dan Kumpulan Umur, Oktober 2017</li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Cras justo odio</li>
                            <li class="list-group-item">Dapibus ac facilisis in</li>
                            <li class="list-group-item">Morbi leo risus</li>
                            <li class="list-group-item">Porta ac consectetur ac</li>
                            <li class="list-group-item">Vestibulum at eros</li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Cras justo odio</li>
                            <li class="list-group-item">Dapibus ac facilisis in</li>
                            <li class="list-group-item">Morbi leo risus</li>
                            <li class="list-group-item">Porta ac consectetur ac</li>
                            <li class="list-group-item">Vestibulum at eros</li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="list-api" role="tabpanel" aria-labelledby="list-api-list">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Cras justo odio</li>
                            <li class="list-group-item">Dapibus ac facilisis in</li>
                            <li class="list-group-item">Morbi leo risus</li>
                            <li class="list-group-item">Porta ac consectetur ac</li>
                            <li class="list-group-item">Vestibulum at eros</li>
                        </ul>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-header font-weight-bold">
                        10 Penyumbang Set Data Tertinggi
                    </div>
                    <div class="card-body">
                        <canvas id="myChart" width="100%"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <div class="row d-flex align-items-center py-2">
                            <div class="ml-3">
                                <i class="material-icons" style="font-size: 40px;">data_usage</i>
                            </div>
                            <div class="col">
                                <div style="font-size: 15px;">Jumlah Set Data</div>
                                <div style="font-size: 25px; font-weight: bold;">13093</div>
                            </div>
                        </div>
                        <div class="row d-flex align-items-center py-2">
                            <div class="ml-3">
                                <i class="material-icons" style="font-size: 40px;">record_voice_over</i>
                            </div>
                            <div class="col">
                                <div style="font-size: 15px;">Jumlah Pembekal Set Data</div>
                                <div style="font-size: 25px; font-weight: bold;">224</div>
                            </div>
                        </div>
                        <div class="row d-flex align-items-center py-2">
                            <div class="ml-3">
                                <i class="material-icons" style="font-size: 40px;">accessibility</i>
                            </div>
                            <div class="col">
                                <div style="font-size: 15px;">Jumlah Pelawat</div>
                                <div style="font-size: 25px; font-weight: bold;">142</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-center mt-5">
                    <div class="card-header font-weight-bold">
                        PELAKSANAAN
                    </div>
                    <div class="card-body py-3" style="background-color: #9b59b6">
                        <img src="http://www.data.gov.my/images/opendata.png" width="60px;" />
                    </div>
                    <div class="card-footer text-muted">
                        Adakah Kerajaan komited dalam pelaksanaan inisiatif data terbuka...
                    </div>
                </div>
                <div class="card text-center mt-5">
                    <div class="card-header font-weight-bold">
                        KESEDIAAN
                    </div>
                    <div class="card-body py-3" style="background-color: #e74c3c">
                        <img src="http://www.data.gov.my/images/availability.png" width="60px;" />
                    </div>
                    <div class="card-footer text-muted">
                        Sejauh mana kesediaan Kerajaan dalam inisiatif data terbuka...
                    </div>
                </div>
                <div class="card text-center mt-5">
                    <div class="card-header font-weight-bold">
                        IMPAK
                    </div>
                    <div class="card-body py-3" style="background-color: #3498db">
                        <img src="http://www.data.gov.my/images/chart.png" width="60px;" />
                    </div>
                    <div class="card-footer text-muted">
                        Adakah inisiatif data terbuka memberi manfaat dan impak...
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Penyumbang Data -->

<div class="pb-5">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header font-weight-bold">
                        10 Bilangan Data Mengikut Kluster
                    </div>
                    <div class="card-body">
                        <canvas id="myChart1" width="100%"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-header font-weight-bold">
                        10 Set Data Kerap Dimuaturun
                    </div>
                    <div class="card-body">
                        <canvas id="myChart2" width="100%"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<style>
html, body
{
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    line-height: 1.428571429;
    color: #333;
    background-color: #fff;
    margin: 0;
}

.material-icons
{
    font-size: 29px;
    color: #8792a4;
}
.material-icons:hover
{
    color: #606c7f;
}

a
{
    text-decoration: none !important;
}
</style>

<?php $__env->startSection('script-after-load'); ?>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var ctx = document.getElementById('myChart1').getContext('2d');
var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
    data: data = {
    datasets: [{
        data: [25, 20, 30, 45, 58, 33],
        backgroundColor: [
                'rgba(255, 99, 132, 0.9)',
                'rgba(54, 162, 235, 0.9)',
                'rgba(255, 206, 86, 0.9)',
                'rgba(75, 192, 192, 0.9)',
                'rgba(153, 102, 255, 0.9)',
                'rgba(255, 159, 64, 0.9)'
            ],
    }],
    labels: [
        'Red',
        'Yellow',
        'Blue'
    ]
    },
    // options: options
});


var ctx = document.getElementById('myChart2').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
<?php $__env->stopSection(); ?>