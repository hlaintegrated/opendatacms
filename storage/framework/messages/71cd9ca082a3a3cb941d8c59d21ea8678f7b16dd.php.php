<?php $__env->startSection('content'); ?>
<?php echo Form::model($dataset_request,['method'=>'POST','action'=>['DatasetRequestManagementController@update',$dataset_request->request_id]]); ?>

<div class="panel panel-default" style="margin-top:20px">
	<div class="panel-heading">
		<?php echo e(_('Dataset Request Detail')); ?>

	</div>
	<div class="panel-body">
		<table class="table-striped table-hover" width="100%">
			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Sender')); ?></td>
				<td><?php echo e($dataset_request->name); ?></td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Email')); ?></td>
				<td><?php echo e($dataset_request->email); ?></td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Phone')); ?></td>
				<td><?php echo e($dataset_request->phone); ?></td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Title')); ?></td>
				<td><?php echo e($dataset_request->title); ?></td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Description')); ?></td>
				<td><?php echo e($dataset_request->description); ?></td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Remarks')); ?></td>
				<td><?php echo e($dataset_request->notes); ?></td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Sources')); ?></td>
				<td>
					<?php if($dataset_request->status == 'pending'): ?>
					<?php $__currentLoopData = $sources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $source): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="col-sm-4">
						<input type="checkbox" name="source[]" id="<?php echo e($source->data_source_id); ?>" value="<?php echo e($source->data_source_id); ?>" <?= in_array($source->data_source_id,$dataset_request->sources)?'checked':''?> >
						<label for="<?php echo e($source->data_source_id); ?>" class="form-label"><?php echo e(_("$source->data_source_name")); ?></label><br/>
					</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
					<?php else: ?>
					<ul>
						<?php $__currentLoopData = $dataset_request->sources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $source): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><?php echo e(T::_($source->data_source_name)); ?></li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</ul>
					<?php endif; ?>
				</td>
			</tr>

			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Type')); ?></td>
				<td>
					<?php if($dataset_request->status == 'pending'): ?>
					<?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="col-sm-4">
						<input type="checkbox" name="type[]" id="<?php echo e($type->data_type_id); ?>" value="<?php echo e($type->data_type_id); ?>" <?= in_array($type->data_type_id,$dataset_request->types)?'checked':''?>>
						<label for="<?php echo e($type->data_type_id); ?>" class="form-label"><?php echo e($type->data_type_name); ?></label><br/>
					</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
					<ul>
						<?php $__currentLoopData = $dataset_request->types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><?php echo e($type->data_type_name); ?></li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</ul>
					<?php endif; ?>
				</td>
			</tr>
			<tr style="height: 30px">
				<td style="width: 20%"><?php echo e(_('Status')); ?></td>
				<td>
					<?php echo $__env->make('administrator.pages.datasetrequest.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
				</td>
			</tr>
			<tr style="height: 30px">
				<td style="width: 20%"></td>
				<td>
					<?php if($dataset_request->status == 'pending'): ?>
					<input type="hidden" name="status" value="in_progress">
					<button class="btn btn-primary">Accept</button>
					<a href="<?php echo e(action('DatasetRequestManagementController@reject',$dataset_request->request_id)); ?>" class="btn btn-danger">Reject</a>
					<?php elseif($dataset_request->status == 'in_progress'): ?>
					<input type="hidden" name="status" value="completed">
					<button class="btn btn-primary">Mark Finished</button>
					<a href="<?php echo e(action('DatasetRequestManagementController@reject',$dataset_request->request_id)); ?>" class="btn btn-danger">Reject</a>
					<?php endif; ?>
				</td>
			<tr>
		</table>
		<?php echo Form::close(); ?>

	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('administrator.layout.request', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>