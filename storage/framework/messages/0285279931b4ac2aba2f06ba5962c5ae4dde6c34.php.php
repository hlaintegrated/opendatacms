<div class="row">
    <div class="panel panel-fullrow home-widget">
        <div class="panel-heading">
            <input type="text" name="-title" class="form-control" placeholder="Title" value="<?php echo e(isset($content)?$content['title']:''); ?>">
        </div>
        <div class="panel-body">
            <textarea name="-content" rows="5" class="form-control" ><?php echo isset($content)?$content['content']:''; ?></textarea>
        </div>
        <div class="panel-footer">
            <div class="row form-group">
                <div class="col-sm-6">
                    <input type='text' name='-linktitle ' class='form-control' placeholder='Link Title' value='<?php echo e(isset($content)?$content['linktitle']:''); ?>'>
                </div>
                <div class="col-sm-6">
                    <input type='text' name='-linkref' class='form-control' placeholder='Link Ref' value='<?php echo e(isset($content)?$content['linkref']:''); ?>'>
                </div>
            </div>
        </div>
    </div>
</div>