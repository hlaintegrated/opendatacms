    <div class="panel panel-default panel-admin">
        <div class="panel-body" >

                Changes Made By : <strong><?php echo e($user->name); ?></strong>
            <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th width="50%" class="text-center">Before</th>
                    <th width="50%" class="text-center">After</th>
                </tr>
                <tr>
                    <td>
                        <?php if(is_array($before)): ?>
                        <table class="table table-striped">
                            <?php $__currentLoopData = $before; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($field); ?></td>
                                    <td><?php echo e($value); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(is_array($after)): ?>
                        <table class="table table-striped">
                            <?php $__currentLoopData = $after; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($field); ?></td>
                                    <td><?php echo e($value); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
            </div>
        </div>
    </div>