<?php

namespace App\Http\Controllers;

use App\Ticket;
use DB;
use App\data_source;
use App\status;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ticket =Ticket::OrderBy('id','asc')->paginate(100000);

        $sources['data'] = data_source::getDataSource();

        $statuses['data'] = status::getStatus();

        return view('admin.ticket',compact('ticket','sources','statuses'));

    }

    public function search(Request $request)
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        return view('admin.ticket-edit', ['ticket' => $ticket]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ticket)
    {
        $ticket = array(
            'name' => $request->name,
            'subjek' => $request->subjek,
            'status' => $request->status,
            'pembekal' => $request->pembekal,
            'description' => $request->description
        );
  
        Ticket::findOrfail($request->t_id)->update($ticket);

        Alert::success('Kemaskini Tiket', 'Permohonan Telah Dikemaskini');

        return redirect('/admin/ticket');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        $ticket->delete();

        Alert::warning('', 'Maklumat Tiket Telah Dibuang');

        return redirect('/admin/ticket');
    }
    
}
