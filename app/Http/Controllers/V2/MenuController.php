<?php

namespace App\Http\Controllers\V2;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Page;
use App\Models\PageContent;
use App\Http\Requests\MenuEditRequest;
use App\Http\Requests\MenuCreateRequest;
use Carbon\Carbon;
use LaravelGettext;
use App\Http\Library\APIRequest;
use Session;
use Log;
use Input;
use Settings;
use App\Http\Library\LanguageConvert;


class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // use LanguageConvert;
    protected $language_fields = ['title'];
    
    const IMAGEDIR ='./images/menu/';
    
     
    public function index()
    {
        // $menus = Menu::all();
        // foreach ($menus as $menu)
        //     $menu->title = json_decode($menu->title,true)[LaravelGettext::getLocale()];

        $menus = array();
        
        return view('v2.admin.pages.menu.index', compact('menus'));
    }
    	

    public function sectionindex($section)
    {
        // $menus = Menu::whereNotNull($section)->orderBy($section,'asc')->get();
        // foreach ($menus as $menu)
        //     $menu->title = json_decode($menu->title,true)[LaravelGettext::getLocale()];
        // $selects = Menu::whereNull($section)->get();
        // foreach ($selects as $select)
        //     $select->title = json_decode($select->title,true)[LaravelGettext::getLocale()];
        // $page = ucfirst($section);

        $menus = array();
        $selects = array();
        $page = null;
        $section = array();

        return view('v2/admin/template/pages/menu/sectionindex',compact('menus','selects','page','section'));
    }

    public function sectionsave($section)
    {
        $inputs = Request::all();
        $reset = Menu::whereNotNull($section)->get();
        foreach ($reset as $r) {
            $r->$section = null;
            $r->unauditedsave();
        }
        if (array_key_exists('links',$inputs)) {
            $update = Request::all()['links'];
            foreach ($update as $index => $id) {
                $menu = Menu::find($id);
                $menu->$section = $index;
                $menu->unauditedsave();
            }
        }
        return redirect(action('MenuController@sectionindex',$section))
            ->with('message',_(ucfirst($section).' Menu save success'));
    }

    public function sectionadd($section)
    {
        $menu = Request::get('menu');
        if ($menu != "noselect")
        {
            $menu = Menu::find($menu);
            $last = Menu::whereNotNull($section)->count();
            $menu->$section = $last;
            $menu->save();
        }
        return redirect(action("MenuController@sectionindex",$section))->with("message",_("New Menu Added"));
    }

    public function sectiondelete($section,$link)
    {
        $menu = Menu::find($link);
        $menu->$section = null;
        $menu->save();
        return redirect(action("MenuController@sectionindex",$section))->with("message",_("Menu Successfully Deleted"));
    }

    public function footerindex()
    {
        $menus = Menu::whereNotNull('footer')->orderBy('footer','asc')->get();
        foreach ($menus as $menu)
            $menu->title = json_decode($menu->title,true)[LaravelGettext::getLocale()];
        $selects = Menu::whereNull('footer')->get();
        foreach ($selects as $select)
            $select->title = json_decode($select->title,true)[LaravelGettext::getLocale()];
        $page = 'Footer';
        return view('administrator.pages.menu.footerindex',compact('menus','selects','page'));
    }

    public function headerindex()
    {
        $menus = Menu::whereNotNull('header')->orderBy('header','asc')->get();
        foreach ($menus as $menu)
        {
            $menu->title = json_decode($menu->title,true)[LaravelGettext::getLocale()];
        }
        $selects = Menu::whereNull('header')->get();
        foreach ($selects as $select)
            $select->title = json_decode($select->title,true)[LaravelGettext::getLocale()];
        $page = 'Header';
        return view('administrator.pages.menu.headerindex',compact('menus','selects','page'));
    }
    
    public function footersave()
    {
        $inputs = Request::all();
        $reset = Menu::whereNotNull('footer')->get();

        foreach ($reset as $r) {
            $r->footer = null;
            $r->unauditedsave();
        }

        if (array_key_exists('links',$inputs)) {
            $update = Request::all()['links'];
            foreach ($update as $index => $id) {
                $menu = Menu::find($id);
                $menu->footer = $index;
                $menu->unauditedsave();
            }
        }
        return redirect(action('MenuController@footerindex'))
            ->with('message',_('Footer Menu save success'));
    }
    
    public function headersave()
    {
        $inputs = Request::all();
        $reset = Menu::whereNotNull('header')->get();

        foreach($reset as $r)
        {
            $r->header = null;
            $r->unauditedsave();
        }

        if (array_key_exists('links',$inputs)) {
            $update = Request::all()['links'];
            foreach ($update as $index => $id) {
                $menu = Menu::find($id);
                $menu->header = $index;
                $menu->unauditedsave();
            }
        }
        return redirect(action('MenuController@headerindex'))
            ->with('message',_('Header Menu save success'));
    }
    
    public function dashboard()
    {
        return redirect(env('CKAN_SERVER')
            .(LaravelGettext::getLocale()=='en_US'?'en_US':'ms_MY').'/dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $types=array(); 
        // $links = Menu::options();
        
        // return view('administrator.pages.menu.create',
        //     compact('links'));

        $links = array();

        return view('v2.admin.pages.menu.create', compact('links'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuCreateRequest $request)
    {
        $input = $this->convert_inputs($request->all());
        Menu::create($input);
        return redirect('administrator/menu')
            ->with('message',_('Menu Create Success'));
        
    }

    public function convert_inputs($input)
    {
        if ($input['target'] == 'other')
            $input['target'] = $input['other_target'];

        $input = $this->convert_input($input, Settings::language_codes());
        return $input;
    }
    
    public function convert_objects($object)
    {
        $object = $this->convert_object($object, Settings::language_codes());
        $object->other_target = $object->target;
        return $object;
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($link)
    {
        $menu = Menu::whereLink($link)->firstOrFail();
        return redirect($menu->target);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = $this->convert_objects(Menu::findOrFail($id));
        $links = Menu::options();
        
        return view('administrator.pages.menu.edit',compact('menu','links'));
    }

    public function data(){
        return redirect(env('CKAN_SERVER','http://127.0.0.1:9000/').LaravelGettext::getLocale().'/dataset');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuEditRequest $request, $id)
    {
        $menu = Menu::find($id);
        $input = $this->convert_inputs($request->all(),Settings::language_codes());
        $menu->update($input);
        return redirect('administrator/menu')->with('message',_('Edit Menu Success'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::findOrFail($id);
        $menu->delete();
        return redirect('administrator/menu')->with('message',_("Menu delete successful"));
    }
}
