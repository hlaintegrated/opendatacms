<?php

namespace App\Http\Controllers\V2;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ClusterCache;
use App\Http\Library\APIRequest;

class ClusterController extends Controller
{
    //the location of cluster images being stored at. The root folder will be public/
    const IMAGEDIR ='./images/cluster/';
    
    //showing all the cluster that currently exist
    public function index()
    {
        // $this->refresh();
        // $clusters = ClusterCache::orderBy('position','asc')->get();
        $clusters = array();
        return view("v2/admin/template/pages/cluster/index",compact('clusters'));
    }

    //If CKAN make any changes in the cluster, CMS will refresh the detail of the clusters if this method invoked. The Cluster settings for CMS will be retained if the cluster is still exists in CKAN. New Cluster from CKAN will use default settings. Removed Cluster in CKAN will be removed also in CMS.
    public function refresh()
    {
        $clusters = APIRequest::getClusters(999);
        $cached = ClusterCache::all();
        $exists = array();
        for($i = 0; $i < count($cached); $i++)
            $exists[$i] = false;
        foreach($clusters as $cluster)
        {
            $found = false;
            for($i = 0; $i < count($cached) && !$found; $i++)
            {
                if ($cached[$i]->link == $cluster['name'])
                {
                    $exists[$i] = true;
                    $cached[$i]->title = $cluster['title'];
                    $cached[$i]->unauditedsave();
                    $found = true;
                }
            }
            if (!$found)
                ClusterCache::create(['link'=>$cluster['name'],'title'=>$cluster['title'],'image'=>'default.png','hover_image'=>'default_hover.png']);
        }
        for($i = 0; $i < count($cached); $i++)
            if (!$exists[$i])
                $cached[$i]->delete();
    }
    

    //Saving the changes made for CMS side
    public function save()
    {
        $input = Request::all();
        for ($i = 0; $i < count($input['link']); $i++)
        {
            $cluster = ClusterCache::find($input['link'][$i]);
            $cluster->home_show = array_key_exists("show$cluster->link",$input);
            if ($input['image'][$i])
            {
                $file = $input['image'][$i];
                $extension = $file->getClientOriginalExtension();
                $filename = $input['link'][$i];
                $file->move(self::IMAGEDIR,$filename);
                $cluster->image = $filename;
            }
            if ($input['hover_image'][$i]){
                $file = $input['hover_image'][$i];
                $extension = $file->getClientOriginalExtension();
                $filename = 'hover-'.$input['link'][$i];
                $file->move(self::IMAGEDIR,$filename);
                $cluster->hover_image = $filename;
            }
            $cluster->unauditedsave();
        }
        return redirect(action("ClusterController@index"))->with('message',_('Save Success'));
    }

    //Reordering the cluster position for home and cluster view
    public function position(){
        // $clusters = ClusterCache::orderBy('position','asc')->get();
        $clusters = array();
        return view('v2/admin/template/pages/cluster/position', compact('clusters'));
    }

    //Saving the reordering position of the cluster
    public function savePosition()
    {
        $this->refresh();
        $link = Request::input('link');
        for($i = 0; $i < count($link); $i++)
        {
            $cluster = ClusterCache::find($link[$i]);
            $cluster->position = $i+1;
            $cluster->unauditedsave();
        }
        return redirect(action("ClusterController@position"))->with('message',_('Save Success'));
    }

}
