<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
// use Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    public function index()
    {
        return view('v2.admin.pages.home');
    }
}
