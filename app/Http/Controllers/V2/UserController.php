<?php

namespace App\Http\Controllers\V2;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use Session;
use App\Http\Library\APIRequest;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserEditRequest;
use Carbon\Carbon;
use App\Jobs\MailSender;
use Input;
use App\Jobs\RawMailSender;
use App\Models\MailTemplate;
use Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        // $this->middleware('user', ['except' => ['index','show']]);
    }
     
    public function index()
    {
        // $query = User::orderBy('created','asc');
        // if(isset($_GET['name']))
        //     $query = $query->where('name','like',"%".$_GET['name']."%");
        // $users = $query->paginate(10);
        // return view('administrator.pages.user.index',compact('users'));

        $users = array();
        return view('v2/admin/template/pages/user/index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('administrator.pages.user.create');
        return view('v2/admin/template/pages/user/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $request = $request->all();
        $username = $request['name'];
        $email = $request['email'];
        $password = 'default';
        if ($request['system_administrator'] == 'true')
            $result = APIRequest::registerSysadmin($username, $email, $password);
        else
            $result = APIRequest::createUser($username, $email, $password)['id'];

        $details= [
            'username' => $username,
            'name' => $username,
            'fullname' => $request['fullname'],
            'email' =>$request['email'],
            'apikey' => APIRequest::getUserAPIKey($username),
            'id' => $result,
            'sysadmin' => $request['system_administrator'],
            'activity_streams_email_notification' => false,
            'state' => $result,
            'password' => 'nothing',
            'resetpassword' => md5(Carbon::now()),
        ];
        if (env("TICKETING","default") == "default")
            $details['data_source']=$request['data_source'];
        $details['link'] = 'http://'.$_SERVER['SERVER_NAME'].'/session/reset/'.$details['resetpassword'];
        $user = User::create($details,true,["data_source","created","apikey"]);
        $text = MailTemplate::render("Account Confirmation",$details);

        $this->dispatch(new RawMailSender($email,$request['fullname'],_('User Confirmation'),$text));
        return redirect('administrator/user')->with('message',_('User created successfully'));
    }

    public function resend($id){
        $user = User::find($id);
        $user->resetpassword = md5($user->name.$user->password.Carbon::now());
        $user->save();
        $contents = [
            'link' =>'http://'.$_SERVER['SERVER_NAME'].'/session/reset/'.$user->resetpassword,
            'email' => $user->email,
            'username' => $user->name

        ];
        $text = $text = MailTemplate::render("Password Reset",$contents);

        $this->dispatch(new RawMailSender($user->email,$user->fullname,_('Password Reset Request'),$text));
        return redirect(action("UserController@index"))->with('message',_('Reset email has been sent successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ("show unimplemented");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('administrator.pages.user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserEditRequest $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|unique:user,name,'.$id,
        ]);

        if ($validator->fails()) {
            return redirect("administrator/user/$id/edit")->withErrors("Username is taken. Please choose another username")->withInput();
        }

        $update = $request->all();
        $user = User::find($id);
        $email = $update['email'];
        $username = $user->name;
        if (array_key_exists('password',$update))
        {
            $password = $update['password'];
            if ($username == $password)
                return redirect(action('UserContoller@edit',$id))->withErrors(_("Password not allowed to be the same as username"));
            $update['password'] = md5($password);
        }
        else
        {
            $update['password'] = $user->password;
        }
        $user->update($update,true,["data_source","created"]);

        if (env('APP_ENV') != 'testing')
        {
            $apikey = Session::get('auth')->apikey;
            APIRequest::editUser($id, $email, $update['password']);
        }
        return redirect("administrator/user")->with('message','Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function suspend($id)
    {
        $user = User::find($id);
        if ($user->sysadmin)
            return view("errors.unauthorized");
        else
        {
            $user->update(['state'=>'suspended','resetpassword'=>'default']);
            $contents = [
                'username' => $user->name
            ];
            $text = $text = MailTemplate::render("Account Suspension Notification",$contents);
            $this->dispatch(new RawMailSender($user->email,$user->fullname,_('Account Suspension'),$text));
        }
        // return redirect("administrator/user")->with('message','Suspend Success');
        return redirect()->back()->with('message','Suspend Success');
    }
    
    
    public function unsuspend($id)
    {
        $user = User::find($id);
        $user->update(['state'=>'active']);
        // return redirect("administrator/user")->with('message','Unsuspend Success');
        return redirect()->back()->with('message','Unsuspend Success');
    }

    public function activate($id)
    {
        $user = User::find($id);
        $user->update(['state'=>'active']);
        return redirect("administrator/user");
    }
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $name = $user->name;
        $user->delete();
        return redirect()->back()->with('message','Successfully delete '.$name);
    }
}
