<?php

namespace App\Http\Controllers\V2;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Settings;

class SystemSettingController extends Controller
{
    const UPLOADPATH = 'images/';
    public function banner()
    {
        return view("v2/admin/template/pages/setting/banner");
    }

    public function saveBanner(Request $request)
    {
        $inputs = $request->all();
        $images = ['home_banner','home_bottom_banner','page_banner'];

        foreach($images as $image)
        {
            if ($request->hasFile($image)){
                $inputs[$image]->move(self::UPLOADPATH,$image);
            }
        }
        return redirect(action("SystemSettingController@banner"))
            ->with("message",_("Banner Save Success"));
    }

    public function other($field){
        $setting = Settings::find($field);
        return view("administrator.pages.setting.other",compact('setting','field'));
    }

    public function saveOther($field)
    {
        $setting = Settings::find($field);
        $setting->value = Request::get('value');
        $setting->save();
        return redirect(action("SystemSettingController@index"))
            ->with("message",_("Update Success"));
    }
    
    public function index()
    {
        // $settings = Settings::orderBy('name','asc')->get();
        // $grouping = Settings::get_grouping();

        $settings = array();
        $grouping = array();

        return view('v2/admin/template/pages/setting/index',compact('settings','grouping'));
    }

    public function language(){
        $languages = json_decode(Settings::get("Languages"),true);
        $field = "Languages";
        return view('administrator.pages.setting.language',compact('languages','field'));
    }

    public function localized($field){
        $settings = json_decode(Settings::get($field),true);
        return view('administrator.pages.setting.localized',compact('settings','field'));
    }

    public function saveLanguage($field)
    {
        $input = Request::all();
        $languages = [];
        for($i = 0; $i < count($input['codes']); $i++)
            $languages[$input['codes'][$i]] = $input['values'][$i];
        Settings::set($field,json_encode($languages));
        return redirect('/administrator/settings')->with('message','Update Success');
    }
    
    public function update()
    {
        foreach(Settings::all() as $setting)
            if ($setting->type == 'default')
             Settings::set($setting->name,$_POST[str_replace(' ','_',$setting->name)]);
        return redirect('/administrator/settings')->with('message','Update Success');
    }

}
