<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Models\V2\User;
use Illuminate\Http\Request;
// use Auth;
use Illuminate\Support\Facades\Auth;
use Session;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $md5_password = md5($request->password);
        $user = User::where('name', $request->username)->where('password', $md5_password)->first();

        if ($user->state == 'active')
        {
            // manually authenticating laravel style
            Auth::loginUsingId($user->id);

            $auth_user = Auth::user();
            // dd($auth_user);
            // dd(Auth::check());

            // Session::put('auth',$user);

            // if($user->data_source) Session::put('data_sources',$user->dataSources);
            // if($user->data_source) $request->session()->put('data_sources', $user->data_source)

            // if ($user->sysadmin)
            // {
            //     // Session::put('role','sysadmin');
            //     $request->session()->put('role', $user->sysadmin);
            // }

            return redirect('administrator/dashboard');

            // $username = $request['username'];
            // return view("frontend.pages.session.loginajax",compact("redirect","username"));
        }
        else
        if ($user->state == 'suspended')
        {
            return redirect(action('SessionController@login'))->withErrors(_('Your account is currently under suspension. Please contact System Administrator'));
        }
        else
        {
            return redirect(action('SessionController@login'))->withErrors(_('Your account is not yet activated, please check your email to activate it'));
        }

        // if($user) {
        //     return redirect('administrator/dashboard');
        // } else {
        //     return back();
        // }

    }
}
