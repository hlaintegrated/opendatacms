<?php

namespace App\Http\Middleware\V2;

use Closure;

class Administrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->sysadmin == 'sysadmin')
            return $next($request);
        else
            return redirect('login');

    }
}
