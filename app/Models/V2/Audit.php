<?php

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $table = "audit_trail";

    protected $fillable = [
        "user_id",
        "message",
        "at",
        "before",
        "after",
        "model",
        "ref"
    ];

    public $timestamps = false;


    public function user(){
        return $this->belongsTo(User::class,"user_id")->withTrashed();
    }
}
