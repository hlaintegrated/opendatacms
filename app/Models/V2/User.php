<?php

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;
use App\Models\V2\AuditedModel;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable //implements
{
    use SoftDeletes;

    protected $table = 'users';
		const CREATED_AT = 'created';
		const UPDATED_AT = null;

		protected $primaryKey = 'id';
		public $incrementing = false;
		protected $keyType = 'string';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'data_source',
        'name',
        'fullname',
        'email',
        'password',
        'apikey',
        'id',
        'created',
        'sysadmin',
        'activity_streams_email_notification',
        'state',
        'data_source',
        'resetpassword'
    ];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

    public static $ref_field = 'fullname';

	// public function events()
	// {
	// 	return $this->hasMany('App\Model\Event');
	// }

	// public function dataSources()
	// {
	// 	return $this->hasMany('App\Model\DataSource','id');
	// }

	// public function images()
	// {
	// 	return $this->hasMany('App\Model\Image');
    // }

}
