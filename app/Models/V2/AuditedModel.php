<?php

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class AuditedModel extends Model
{
    public static $ref_field = '';

    public static function audit($message)
    {
        if (Session::has('auth') && env('APP_ENV') != 'testing' ) {
            $values = [
                'user_id' => Session::get('auth')->id,
                'at' => Carbon::now(),
                'message' => $message
            ];
            Audit::create($values);
        }
    }

    public static function audit_create($table, $values, $ref = "NA")
    {
        $values = [
            'user_id' => self::get_user_id(),
            'at' => Carbon::now(),
            'message' => "Created new row at $table with values " . json_encode($values),
            'before' => json_encode([]),
            'after' => json_encode($values),
            'model' => $table,
            'ref' => $ref
        ];

        Audit::create($values);

    }

    public static function audit_delete($table, $id, $values = null)
    {

        $primary_key_id = App::make(get_called_class())->getKeyName();
        try {
            $before = DB::table( $table )->where( $primary_key_id, $id )->first();

            $after = [];
            $ref = self::get_ref($before);

            $values = [
                'user_id' => self::get_user_id(),
                'at' => Carbon::now(),
                'message' => "Deleted a row at $table with id $id",
                'before' => json_encode($before),
                'after' => json_encode($after),
                'model' => $table,
                'ref' => $ref
            ];

            Audit::create($values);
        } catch (Exception $e) {
            self::audit("Deleted a row at $table with id $id");
        } catch (Error $error) {
            self::audit("Deleted a row at $table with id $id");
        }


    }

    public function audit_update($table, $id, $after = null, $before = null, $ref = "NA")
    {
        $message = "Updated a row at $table with id $id";
        if ($before != null) {
            $message = $message." from ".json_encode($before);
        }
        if ($after != null) {
            $message = $message." to ".json_encode($after);
        }
        //$this->audit($message);

        $values = [
            'user_id' => self::get_user_id(),
            'at' => Carbon::now(),
            'message' => $message,
            'before' => json_encode($before),
            'after' => json_encode($after),
            'model' => $table,
            'ref' => $ref
        ];

        Audit::create($values);

    }

    public static function create(array $attributes = [],$audit=true,$ignore = null)
    {
        $result =  parent::create($attributes);
        if($audit){
            $model = self::mapAttributes($result);
            unset($model["created_at"]);
            unset($model["updated_at"]);
            unset($model["password"]);
            unset($model["apikey"]);
            if ($ignore != null)
            {
                foreach($ignore as $configuration)
                    unset($model[$configuration]);
            }
            $ref = self::get_ref($model);
            self::audit_create($result->table,$model, $ref);

        }
        return $result;
    }

    private static function mapAttributes ($model){
        $result = [];
        foreach($model->fillable as $attribute)
        {
            $result[$attribute] = $model->$attribute;
        }
        return $result;
    }

    public function update(array $attributes = [], $audit=true,$ignore = null)
    {
        $before = self::mapAttributes($this);
        $result = parent::update($attributes);
        $primaryField = $this->primaryKey;
        $after = self::mapAttributes($this);
        $primaryKey = $this->$primaryField;

        if ($audit){
            self::unsetAttribute("created_at", $after, $before);
            self::unsetAttribute("updated_at", $after, $before);
            foreach($this->fillable as $fill)
            {

                if ($fill != "data_source" && $condition = $after[$fill] == $before[$fill])
                {
                    self::unsetAttribute($fill, $after, $before);
                }
            }
            if ($ignore != null)
            {
                foreach($ignore as $configuration)
                    self::unsetAttribute($configuration, $after, $before);
            }

            if ($after != $before) {
                try {
                    //get ref for update
                    $current_data = self::find($this->$primaryField)->toArray();
                    $ref = self::get_ref($current_data);
                } catch (Exception $e) {
                    $ref = "N/A";
                }
                $this->audit_update($this->table,$primaryKey,$after,$before, $ref);
            }
        }
        return $result;
    }

    private static function unsetAttribute($attribute, &$m1, &$m2){
        unset($m1[$attribute]);
        unset($m2[$attribute]);
    }

    public function delete($audit=true)
    {
        $primaryKey = $this->primaryKey;
        if ($audit)
            $this->audit_delete($this->table,$this->$primaryKey,$this);
        return parent::delete();
    }

    public function unauditedsave(array $options =[])
    {
        return $this->save($options);
    }

    public function save(array $options = [],$audit = true, $ignore = null)
    {
        return parent::save($options); // TODO: Change the autogenerated stub
    }

    public static function get_user_id() {
        if (Session::has('auth') && env('APP_ENV') != 'testing' ) {
            $user_id = Session::get('auth')->id;
        } else {
            $user_id = "NA";
        }

        return $user_id;
    }

    public static function get_ref($model) {
        $theclass = get_called_class();
        $model = (array) $model;
        if(!empty($theclass::$ref_field )) {
            if(!empty($model[$theclass::$ref_field ])) {
                return $model[$theclass::$ref_field ];
            }
        }

        return 'NA';
    }
}
