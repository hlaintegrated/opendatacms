<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\status;

class status extends Model
{
    Protected $fillable = ['status_id', 'status_name', 'id'];

    public static function getStatus()
    {
      $value = DB::table('statuses')->orderBy('status_name', 'asc')->get(); 
   
      return $value;
    }
}
