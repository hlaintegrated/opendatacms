<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\data_source;

class data_source extends Model
{
    Protected $table = 'data_source';
    Protected $fillable = ['data_source_id','data_source_name','description', 'id', 'data_source_email'];

    public static function getDataSource()
    {
      $value=DB::table('data_source')->orderBy('data_source_name', 'asc')->get(); 
   
      return $value;
    }

}
