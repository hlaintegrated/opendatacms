<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ticket;

class Ticket extends Model
{
    Protected $primaryKey = 'id';
    Protected $fillable = ['tick','subjek','name','status','pembekal','emel', 'phone', 'description', 'notes', 'file'];

}
